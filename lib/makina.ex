# TODO: check that options are valid
# TODO: parse options
defmodule Makina do
  @moduledoc File.read!("priv/docs/makina.md")

  @doc false
  @spec __using__(Keyword.t()) :: Macro.t()
  defmacro __using__(options) do
    quote do
      use Makina.Exports, unquote(options)
    end
  end
end
