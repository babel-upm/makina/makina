defmodule Makina.Command.Extends do
  import Makina.Helpers
  import Makina.Command, only: [commands_info: 1, command_info: 2]

  alias Makina.Error
  alias Makina.Command.Base

  @moduledoc false

  @typedoc """

  This type represents the command options processed by this module:
  - `:extends` contains the model to be extended.

  """

  @type option() :: {:extends, Macro.t()}

  @type command_info() :: Makina.Command.command_info()

  @options [:extends]

  ##################################################################################################
  # Macros
  ##################################################################################################

  @doc """

  This macro configures the module to allow extended command definitions. Accepts global command
  options, this means that options passed to `use` are passed to every command declared in the
  module. Supported options are defined by the `#{inspect(__MODULE__)}.option()` type.

  This macro registers the following module attributes:

  | attribute           | accumulate | initial value | description                               |
  |---------------------|------------|---------------|-------------------------------------------|
  | `:commands`         | `true`     | `[]`          | names of the commands                     |
  | `:commands_options` | `true`     | `options`     | global options for commands               |
  | `:commands_info`    | `true`     | `[]`          | information about the command enviroments |
  | `:commands_processed_options` | `true` | `[]`     | information about processed options to avoid infinite recursion. |

  ## Examples

      iex> defmodule Example do
      ...>   use Makina.Command.Base
      ...>   use Makina.Command.Extends
      ...> end
      iex> defmodule ExampleExtended do
      ...>   use Makina.Command.Extends, extends: Example
      ...> end

      iex> defmodule Example do
      ...>   use Makina.Command.Extends, extends: "error"
      ...> end
      ** (Makina.Error) `"error"` is not a valid module name

      iex> defmodule Example do
      ...>   use Makina.Command.Extends, extends: Enum
      ...> end
      ** (Makina.Error) module `Enum` does not contain commands

      iex> defmodule Example do
      ...>   use Makina.Command.Extends, extends: A
      ...> end
      ** (Makina.Error) could not load `A`

  """
  @spec __using__([option()]) :: Macro.t()
  defmacro __using__(options) do
    context = __CALLER__.module
    options = Enum.map(options, fn {k, v} -> {k, Macro.expand(v, __CALLER__)} end)

    with {:option, {:ok, extends}} when not is_nil(extends) <-
           {:option, Keyword.fetch(options, :extends)},
         {:ok, ^extends} <- validate_extends(extends, context),
         {:commands_info, %{commands: _commands}} <- commands_info(extends) do
      :ok
    else
      {:option, _} -> :ok
      {:error, error} -> {:error, error}
      error -> {:error, "Unknown error: #{error}"}
    end
    |> then(fn
      :ok -> :ok
      {:error, message} -> Error.throw_error(message, __CALLER__)
    end)

    # Registers an attribute that stores the names of the commands defined in the module.
    Module.register_attribute(__CALLER__.module, :commands, accumulate: true)
    Module.register_attribute(__CALLER__.module, :commands_info, accumulate: true)
    Module.register_attribute(__CALLER__.module, :commands_options, accumulate: true)
    Module.register_attribute(__CALLER__.module, :commands_processed_options, accumulate: true)

    checker = get_checker()
    # Extracts and registers the global options in `use`.
    register_options(__CALLER__, :commands_options, @options, options)

    quote do
      use unquote(checker)
      import unquote(__MODULE__), only: :macros
      require Makina.Command
      require Makina.Command.Base
      @before_compile Makina.Command.Extends
      # @before_compile Makina.Command.Base
    end
  end

  @doc """

  This macro processes the `:extends` option in commands. The resulting declaration contains the
  union of the arguments of the "extending" and the "extended" commands.

  This macro reads the module attribute `:commands_options`.

  ## Examples

      iex> defmodule Example do
      ...>   use Makina.Command, defaults: true
      ...>   command f() do
      ...>     call :ok
      ...>   end
      ...> end
      iex> defmodule Extended do
      ...>   use Makina.Command.Extends, extends: Example
      ...>   command f(arg1 :: integer()), [], do: []
      ...>   command g(), [], do: []
      ...> end

  """
  @spec command(Macro.t(), [option()], Macro.t()) :: Macro.t()
  defmacro command(decl, opts, [{:do, block}]) do
    context = __CALLER__.module
    options = opts ++ Module.get_attribute(context, :commands_options, [])

    with {:ok, info = %{name: name}} <- Base.parse_command(decl, opts, block, context, __CALLER__),
         {:extends, {:ok, extends}} when not is_nil(extends) <-
           {:extends, Keyword.fetch(options, :extends)},
         {:commands_info, %{commands: commands}} <- commands_info(extends),
         {:extends, true} <- {:extends, name in commands},
         {:command_info, b_info} <- command_info(extends, name) do
      # extending definition "e_info" contains the information of the "extending" model
      e_info = info
      _ = check_extends_types(b_info, e_info) |> Enum.map(&Error.throw_warn(&1, __CALLER__))

      extended_decl = extends_decl(b_info, e_info)
      extended_pre = extends_pre(b_info)
      extended_post = extends_post(b_info, e_info)
      options = Keyword.delete(opts, :extends)

      quote do
        Makina.Command.command unquote(extended_decl), unquote(options) do
          unquote(extended_pre)
          unquote(block)
          unquote(extended_post)
        end
      end
    else
      {:extends, _} ->
        quote do: Makina.Command.command(unquote(decl), unquote(options), do: unquote(block))

      {:error, error} ->
        {:error, error}
    end
    |> then(fn
      {:error, message} -> Error.throw_error(message, __CALLER__)
      result -> result
    end)
  end

  @doc """

  This macro injects extended commands that are missing in the extending model.

  This macro reads the module attributes: `:commands` and `:commands_options`.

  """
  @spec __before_compile__(Macro.Env.t()) :: Macro.t()
  defmacro __before_compile__(env) do
    commands = Module.get_attribute(env.module, :commands)
    options = Module.get_attribute(env.module, :commands_options)

    with {:extends, {:ok, extends}} <- {:extends, Keyword.fetch(options, :extends)},
         {:commands_info, %{commands: extending_commands}} <- commands_info(extends) do
      commands =
        for command <- extending_commands, command not in commands do
          case command_info(extends, command) do
            {:command_info, %{result: result}} ->
              quote do: Makina.Command.command(unquote(command)() :: unquote(result))

            error ->
              error
          end
        end

      errors = Enum.filter(commands, &match?({:error, _error}, &1))

      if errors == [],
        do: commands,
        else: {:error, Enum.reduce(errors, fn {_, v}, acc -> acc <> v end)}
    else
      {:extends, _} -> {:ok, []}
      {:error, reason} -> {:error, "Error while loading extended module, reason: #{reason}"}
    end
    |> then(fn
      {:error, message} -> Error.throw_error(message, env)
      result -> result
    end)
  end

  ##################################################################################################
  # Helpers
  ##################################################################################################

  @doc """

  This function generates a command extended declaration combining the information in the base and
  the extending declarations.

  ## Examples

      iex> alias Makina.Command.Base
      iex> b_decl = quote do: f(arg1 :: integer()) :: :ok                # base declaration
      iex> {:ok, b_info} = Base.parse_command(b_decl, [], [], Example, __ENV__)
      iex> e_decl = quote do: f(arg2 :: boolean())                       # extending declaration
      iex> {:ok, e_info} = Base.parse_command(e_decl, [], [], Example, __ENV__)
      iex> extends_decl(b_info, e_info) |> Macro.to_string()
      quote do
        f(arg1 :: integer(), arg2 :: boolean()) :: :ok
      end |> Macro.to_string()

  """
  @spec extends_decl(command_info(), command_info()) :: Macro.t()
  def extends_decl(b_model, e_model) do
    %{arguments: arguments, result: result} = b_model
    %{name: name, arguments: decl_args} = e_model

    arguments =
      (arguments ++ decl_args)
      |> Enum.uniq_by(&elem(&1, 0))
      |> Enum.map(fn {k, v} -> quote do: unquote(Macro.var(k, nil)) :: unquote(v) end)

    quote do
      unquote(name)(unquote_splicing(arguments)) :: unquote(result)
    end
  end

  @doc """

  This function checks the coherence between the extends and the extending parameters in a
  command. Returns a list of error messages.

  ## Examples

      iex> alias Makina.Command.Base
      iex> b_decl = quote do: f(arg1 :: integer()) :: :ok
      iex> {:ok, b_info} = Base.parse_command(b_decl, [], [], Example, __ENV__)
      iex> e_decl = quote do: f(arg1 :: boolean())
      iex> {:ok, e_info} = Base.parse_command(e_decl, [], [], Example, __ENV__)
      iex> [] = check_extends_types(b_info, b_info)
      iex> check_extends_types(b_info, e_info)
      ["incorrect type `integer()` for argument `arg1`, type `boolean()` was expected"]

  """
  @spec check_extends_types(command_info(), command_info()) :: [String.t()]
  def check_extends_types(b_model, e_model) do
    %{name: _name, arguments: decl_args, result: decl_res} = b_model
    %{arguments: arguments, result: result, module: module} = e_model
    grouped = (arguments ++ decl_args) |> Enum.group_by(fn {k, _} -> k end, fn {_k, v} -> v end)

    result =
      if equivalent_type?(result, decl_res, module) or equivalent_type?(result, unknown_type()) do
        []
      else
        decl_res = Macro.to_string(decl_res)
        result = Macro.to_string(result)
        ["incorrect type `#{result}` in result type, type `#{decl_res}` was expected"]
      end

    arguments =
      for {arg, [type1, type2]} <- grouped,
          not (equivalent_type?(type1, type2, module) or
                 equivalent_type?(result, unknown_type())) do
        type1 = Macro.to_string(type1)
        type2 = Macro.to_string(type2)
        "incorrect type `#{type2}` for argument `#{arg}`, type `#{type1}` was expected"
      end

    arguments ++ result
  end

  @doc """

  This function generates the callbacks that call the base command implementation.

  ## Examples

      iex> alias Makina.Command.Base
      iex> decl = quote do: command f(x :: integer())
      iex> block = quote do
      ...>           pre true
      ...>           args []
      ...>           valid_args true
      ...>           valid true
      ...>           call :ok
      ...>           next []
      ...>           post true
      ...>         end
      iex> {:ok, info} = Base.parse_command(decl, [], block, Example, __ENV__)
      iex> extends_pre(info) |> Macro.to_string()
      quote do
        [
         pre(Example.pre(state)),
         args(Example.args(state)),
         valid_args(Example.valid_args(state, arguments)),
         valid(Example.valid(state, arguments)),
         call(Example.call(arguments)),
         next(Example.next(state, arguments, result, valid)),
         post(Example.post(state, arguments, result, valid))
        ]
      end |> Macro.to_string()


  """
  @spec extends_pre(command_info()) :: Macro.t()
  def extends_pre(%{callbacks: callbacks, module: module}) do
    for callback <- callbacks do
      args =
        cond do
          callback in [:pre, :args, :weight] -> [:state]
          callback in [:valid_args, :valid] -> [:state, :arguments]
          callback in [:call] -> [:arguments]
          callback in [:features] -> [:state, :arguments, :result]
          callback in [:post, :next] -> [:state, :arguments, :result, :valid]
          true -> []
        end
        |> Enum.map(&Macro.var(&1, nil))

      quote do
        unquote(callback)(unquote(module).unquote(callback)(unquote_splicing(args)))
      end
    end
  end

  @doc """

  This function generates the callbacks that override user definitions

  ## Examples

      iex> alias Makina.Command.Base
      iex> decl = quote do: command f(x :: integer())
      iex> block = quote do
      ...>           pre true
      ...>           args []
      ...>           valid_args true
      ...>           valid true
      ...>           call :ok
      ...>           next []
      ...>           post true
      ...>         end
      iex> {:ok, info} = Base.parse_command(decl, [], block, Example, __ENV__)
      iex> extends_post(info, info)

  """
  @spec extends_post(command_info(), command_info()) :: [Macro.t()]
  def extends_post(b_model, e_model) do
    [
      pre(b_model, e_model),
      args(b_model, e_model),
      valid_args(b_model, e_model),
      valid(b_model, e_model),
      next(b_model, e_model),
      post(b_model, e_model)
    ]
  end

  @doc """

  This function returns the precondition for the extended model, which is the conjunction of the
  precondition in the base and extending models.

  ## Examples

      iex> alias Makina.Command.Base
      iex> b_decl = quote do: command f()
      iex> b_block = [quote do: pre true]
      iex> {:ok, b_info} = Base.parse_command(b_decl, [], b_block, Example, __ENV__)
      iex> e_decl = quote do: command f()
      iex> e_block = [quote do: pre true]
      iex> {:ok, e_info} = Base.parse_command(e_decl, [], e_block, Example1, __ENV__)
      iex> pre(b_info, e_info) |> Macro.to_string()
      quote do
        pre(((pre = Example.pre(state)) || is_nil(pre)) && ((pre = super()) || is_nil(pre)))
      end |> Macro.to_string()

  """
  @spec pre(command_info(), command_info()) :: Macro.t()
  def pre(b_model, e_model) do
    %{callbacks: callbacks, module: module} = b_model
    %{callbacks: extending_callbacks} = e_model
    args = [:state] |> Enum.map(&Macro.var(&1, nil))
    base = quote do: unquote(module).pre(unquote_splicing(args))

    if :pre in callbacks and :pre in extending_callbacks do
      quote generated: true do
        pre(((pre = unquote(base)) || is_nil(pre)) && ((pre = super()) || is_nil(pre)))
      end
    else
      quote generated: true do
        pre((pre = unquote(base)) || is_nil(pre))
      end
    end
  end

  @doc """

  This function returns the `valid_args` for the extended model, which is the conjunction of the
  `valid_args` in the base and extending models.

  ## Examples


      iex> alias Makina.Command.Base
      iex> b_decl = quote do: command f()
      iex> b_block = [quote do: valid_args true]
      iex> {:ok, b_info} = Base.parse_command(b_decl, [], b_block, Example, __ENV__)
      iex> e_decl = quote do: command f()
      iex> e_block = [quote do: valid_args true]
      iex> {:ok, e_info} = Base.parse_command(e_decl, [], e_block, Example1, __ENV__)
      iex> valid_args(b_info, e_info) |> Macro.to_string()
      quote do
        valid_args(((valid_args = Example.valid_args(state, arguments)) || is_nil(valid_args)) && ((valid_args = super()) || is_nil(valid_args)))
      end |> Macro.to_string()

  """
  @spec valid_args(command_info(), command_info()) :: Macro.t()
  def valid_args(b_model, e_model) do
    %{callbacks: callbacks, module: module} = b_model
    %{callbacks: extending_callbacks} = e_model
    args = [:state, :arguments] |> Enum.map(&Macro.var(&1, nil))
    base = quote do: unquote(module).valid_args(unquote_splicing(args))

    if :valid_args in callbacks and :valid_args in extending_callbacks do
      quote generated: true do
        valid_args(
          ((valid_args = unquote(base)) || is_nil(valid_args)) &&
            ((valid_args = super()) || is_nil(valid_args))
        )
      end
    else
      quote generated: true do
        valid_args((valid_args = unquote(base)) || is_nil(valid_args))
      end
    end
  end

  @doc """

  This function returns the arguments generator for the extended model, which is the merge of the
  generators in the base model and the extending model. Arguments in the extending model are
  prioritized.

  ## Examples

      iex> alias Makina.Command.Base
      iex> b_decl = quote do: command f()
      iex> b_block = [quote do: args true]
      iex> {:ok, b_info} = Base.parse_command(b_decl, [], b_block, Example, __ENV__)
      iex> e_decl = quote do: command f()
      iex> e_block = [quote do: args true]
      iex> {:ok, e_info} = Base.parse_command(e_decl, [], e_block, Example1, __ENV__)
      iex> args(b_info, e_info)

  """
  @spec args(command_info(), command_info()) :: Macro.t()
  def args(b_model, e_model) do
    %{callbacks: callbacks, module: module, env: b_env} = b_model
    %{callbacks: extending_callbacks, env: e_env} = e_model
    args = [:state] |> Enum.map(&Macro.var(&1, nil))
    base = quote do: unquote(module).args(unquote_splicing(args))

    if :args in callbacks and :args in extending_callbacks do
      quote do
        args do
          let [
            extended_args <- unquote(module).args(unquote_splicing(args)),
            super_args <- super(unquote_splicing(args))
          ] do
            unless Keyword.keyword?(extended_args) do
              "Error base `args` does not return a keyword list"
              |> Error.throw_error(unquote(Macro.escape(b_env)))
            end

            unless Keyword.keyword?(super_args) do
              "Error extending `args` does not return a keyword list"
              |> Error.throw_error(unquote(Macro.escape(e_env)))
            end

            case unquote(module).validate_args(Enum.into(extended_args, %{})) do
              {:error, error} -> Error.throw_error(error)
              {:ok, _} -> Keyword.merge(extended_args, super_args)
            end
          end
        end
      end
    else
      quote do: args(unquote(base))
    end
  end

  @doc """

  This function returns the `valid` for the extended model, which is the conjunction of the
  `valid` in the base and extending models.

  ## Examples

      iex> alias Makina.Command.Base
      iex> b_decl = quote do: command f()
      iex> b_block = [quote do: valid true]
      iex> {:ok, b_info} = Base.parse_command(b_decl, [], b_block, Example, __ENV__)
      iex> e_decl = quote do: command f()
      iex> e_block = [quote do: valid true]
      iex> {:ok, e_info} = Base.parse_command(e_decl, [], e_block, Example1, __ENV__)
      iex> valid(b_info, e_info) |> Macro.to_string()
      quote do
        valid ((valid = Example.valid(state, arguments, result)) || is_nil(valid)) && ((valid = super()) || is_nil(valid))
      end |> Macro.to_string()

  """
  @spec valid(command_info(), command_info()) :: Macro.t()
  def valid(b_model, e_model) do
    %{callbacks: callbacks, module: module} = b_model
    %{callbacks: extending_callbacks} = e_model
    args = [:state, :arguments, :result] |> Enum.map(&Macro.var(&1, nil))
    base = quote do: unquote(module).valid(unquote_splicing(args))

    if :valid in callbacks and :valid in extending_callbacks do
      quote do
        valid(((valid = unquote(base)) || is_nil(valid)) && ((valid = super()) || is_nil(valid)))
      end
    else
      quote generated: true do
        valid((valid = unquote(base)) || is_nil(valid))
      end
    end
  end

  @doc """

  This function returns the function that computes the updates in the extended model, which is the
  merge of the updates in the base model and the extending model. Updates in the extending model are
  prioritized.

  ## Examples

      iex> alias Makina.Command.Base
      iex> b_decl = quote do: command f()
      iex> b_block = [quote do: next true]
      iex> {:ok, b_info} = Base.parse_command(b_decl, [], b_block, Example, __ENV__)
      iex> e_decl = quote do: command f()
      iex> e_block = [quote do: next []]
      iex> {:ok, e_info} = Base.parse_command(e_decl, [], e_block, Example1, __ENV__)
      iex> next(b_info, e_info) |> Macro.to_string()
      quote do
        next((Example.next(state, arguments, result, valid) || []) ++ (super() || []))
      end |> Macro.to_string()

  """
  @spec next(command_info(), command_info()) :: Macro.t()
  def next(b_model, e_model) do
    %{callbacks: callbacks, module: module} = b_model
    %{callbacks: extending_callbacks} = e_model
    args = [:state, :arguments, :result, :valid] |> Enum.map(&Macro.var(&1, nil))
    base = quote do: unquote(module).next(unquote_splicing(args))

    if :next in callbacks and :next in extending_callbacks do
      quote do: next((unquote(base) || []) ++ (super() || []))
    else
      quote do: next(unquote(base) || [])
    end
  end

  @doc """

  This function returns the postcondition for the extended model, which is the conjunction of the
  postcondition in the base and extending models.

  ## Examples

      iex> alias Makina.Command.Base
      iex> b_decl = quote do: command f()
      iex> b_block = [quote do: post true]
      iex> {:ok, b_info} = Base.parse_command(b_decl, [], b_block, Example, __ENV__)
      iex> e_decl = quote do: command f()
      iex> e_block = [quote do: post true]
      iex> {:ok, e_info} = Base.parse_command(e_decl, [], e_block, Example1, __ENV__)
      iex> post(b_info, e_info) |> Macro.to_string()
      quote do
        post(((post = Example.post(state, arguments, result, valid)) || is_nil(post)) && ((post = super()) || is_nil(post)))
      end |> Macro.to_string()

  """
  @spec post(command_info(), command_info()) :: Macro.t()
  def post(b_model, e_model) do
    %{callbacks: callbacks, module: module} = b_model
    %{callbacks: extending_callbacks} = e_model
    args = [:state, :arguments, :result, :valid] |> Enum.map(&Macro.var(&1, nil))
    base = quote do: unquote(module).post(unquote_splicing(args))

    if :post in callbacks and :post in extending_callbacks do
      quote generated: true do
        post(((post = unquote(base)) || is_nil(post)) && ((post = super()) || is_nil(post)))
      end
    else
      quote generated: true do
        post((post = unquote(base)) || is_nil(post))
      end
    end
  end
end
