defmodule Makina.Command.Defaults do
  import Makina.Helpers

  @moduledoc false

  @typedoc """

  This type contains the options processed by the macros in this module:
  - `:defaults` whether or not to introduce the default callbacks in a command definition.

  """

  @type option() :: {:defaults, boolean()}

  @options [:defaults]

  ##################################################################################################
  # Macros
  ##################################################################################################

  @doc """

  This macro configures the module to allow the option that introduces default callbacks on command
  definitions.

  This macro registersthe following module attributes:

  | attribute        | accumulate | initial value | description                                |
  |------------------|------------|---------------|--------------------------------------------|
  | `:commands`      | `true`     | `[]`          | names of the commands                      |
  | `:commands_info` | `true`     | `[]`          | information about the command environments |

  ## Examples

      iex> defmodule Example do
      ...>   use Makina.Command.Defaults, defaults: true
      ...> end

  """
  @spec __using__([option()]) :: Macro.t()
  defmacro __using__(options) do
    # Registers an attribute that stores the names of the commands defined in the module.
    Module.register_attribute(__CALLER__.module, :commands, accumulate: true)
    Module.register_attribute(__CALLER__.module, :commands_info, accumulate: true)
    # Extracts and registers the global options passed to `use`.
    register_options(__CALLER__, :commands_options, @options, options)

    quote do
      import unquote(__MODULE__), only: :macros
      require Makina.Command
      require Makina.Command.Base
      # @before_compile Makina.Command.Base
    end
  end

  @doc """

  This macro processes the `:defaults` option and introduces the default callback implementation in
  a command.

  ## Examples

      iex> defmodule Example do
      ...>   use Makina.Command.Defaults
      ...>   command f(), [defaults: true] do
      ...>   end
      ...> end

  """
  @spec command(Macro.t(), [option()], Macro.t()) :: Macro.t()
  defmacro command(decl, opts, [{:do, block}]) do
    context = __CALLER__.module

    defaults =
      if opts[:defaults] do
        quote context: context do
          pre true
          valid_args true
          args []
          next []
          valid true
          features []
          post true
          weight 1
        end
      else
        []
      end

    opts = Keyword.delete(opts, :defaults)

    quote context: context do
      Makina.Command.command unquote(decl), unquote(opts) do
        unquote(defaults)
        unquote(block)
      end
    end
  end
end
