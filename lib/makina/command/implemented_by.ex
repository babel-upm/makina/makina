defmodule Makina.Command.ImplementedBy do
  import Makina.Helpers
  import Makina.Command, only: [commands_info: 1, command_info: 2]

  alias Makina.Error

  @moduledoc false

  @typedoc """

  Type that represents the options processed by `#{inspect(__MODULE__)}`:
  - `:implemented_by` contains the module that contains the implementation under test.

  """

  @type option() :: {:implemented_by, module()}

  @options [:implemented_by]

  ##################################################################################################
  # Macros
  ##################################################################################################

  @doc """

  This macro configures the module to allow the option that redirects the `call` callback to some
  implementation.

  This macro registersthe following module attributes:

  | attribute        | accumulate | initial value | description                                |
  |------------------|------------|---------------|--------------------------------------------|
  | `:commands`      | `true`     | `[]`          | names of the commands                      |
  | `:commands_info` | `true`     | `[]`          | information about the command environments |

  ## Examples

      iex> defmodule Example do
      ...>   use Makina.Command.ImplementedBy, implemented_by: Enum
      ...> end

      iex> defmodule Example do
      ...>   use Makina.Command.ImplementedBy, implemented_by: "error"
      ...> end
      ** (Makina.Error) `:implemented_by` option `"error"` is not a valid module name

  """
  @spec __using__([option()]) :: Macro.t()
  defmacro __using__(options) do
    context = __CALLER__.module
    options = Enum.map(options, fn {k, v} -> {k, Macro.expand(v, __CALLER__)} end)

    with {:option, {:ok, implemented_by}} when not is_nil(implemented_by) <-
           {:option, Keyword.fetch(options, :implemented_by)},
         {:ok, implemented_by} <- check_implemented_by(implemented_by, context),
         {:module, ^implemented_by} <- ensure_compiled(implemented_by) do
      :ok
    else
      {:option, _} -> :ok
      {:error, error} -> {:error, error}
      error -> {:error, "Unknown error: #{error}"}
    end
    |> then(fn
      :ok -> :ok
      {:error, message} -> Error.throw_error(message, __CALLER__)
    end)

    # Registers an attribute that stores the names of the commands defined in the module.
    Module.register_attribute(context, :commands, accumulate: true)
    Module.register_attribute(context, :commands_info, accumulate: true)
    # Extracts and registers the global options passed to `use`.
    register_options(__CALLER__, :commands_options, @options, options)

    quote do
      import unquote(__MODULE__)
      require Makina.Command
      require Makina.Command.Base
      # @before_compile Makina.Command.Base
    end
  end

  @doc """

  This macro processes the `:implemented_by` option and introduces a `call` callback in the command
  that points to the module passed in the `:implemented_by` option.

  ## Examples

      iex> defmodule A do
      ...>   def f(), do: :ok
      ...> end
      iex> defmodule Example do
      ...>   use Makina.Command.ImplementedBy
      ...>   command f(), [implemented_by: A] do
      ...>   end
      ...> end

      iex> defmodule Example do
      ...>   use Makina.Command.ImplementedBy
      ...>   command f(), [implemented_by: "error"] do
      ...>   end
      ...> end
      ** (Makina.Error) `:implemented_by` should receive a module name

  """
  @spec command(Macro.t(), [option()], Macro.t()) :: Macro.t()
  defmacro command(decl, opts, [{:do, block}]) do
    context = __CALLER__.module
    extends = Keyword.get(opts, :extends)

    call =
      with {:ok, %{name: name, arguments: arguments}} <-
             Makina.Command.Base.parse_command(decl, opts, block, context, __CALLER__),
           {:ok, extended_args} <- extended_args(extends, name),
           args <- (extended_args ++ arguments) |> Enum.uniq_by(fn {k, _} -> k end),
           {:ok, implemented_by} <- Keyword.fetch(opts, :implemented_by),
           implemented_by <- Macro.prewalk(implemented_by, &Macro.expand(&1, __CALLER__)),
           {:ok, implemented_by} <- implemented_by(implemented_by, name, args) do
        args = Enum.map(args, &to_call_arg/1)
        quote do: call(unquote(implemented_by).unquote(name)(unquote_splicing(args)))
      else
        :error -> []
        {:warn, _} -> []
        {:error, message} -> Error.throw_error(message, __CALLER__)
      end

    opts = Keyword.delete(opts, :implemented_by)

    quote context: context do
      Makina.Command.command unquote(decl), unquote(opts) do
        unquote(call)
        unquote(block)
      end
    end
  end

  ##################################################################################################
  # Helpers
  ##################################################################################################

  @doc """

  This function extracts the arguments from the base command

  ## Examples

      iex> defmodule Example do
      ...>   use Makina.Command, defaults: true
      ...>   command f(arg1 :: integer()) :: :ok do
      ...>     call :ok
      ...>   end
      ...> end
      iex> {:ok, args} = extended_args(Example, :f)
      iex> args |> Macro.to_string()
      quote do
        [arg1: integer()]
      end |> Macro.to_string()

      iex> import Makina.Command.ImplementedBy
      iex> {:ok, []} = extended_args(nil, :f)

      iex> import Makina.Command.ImplementedBy
      iex> extended_args(A, :f)
      {:error, "Extends module Elixir.A not found, reason: could not load `A`"}

  """
  @spec extended_args(module() | nil, atom()) :: {:ok, Macro.t()} | {:error, String.t()}
  def extended_args(nil, _name), do: {:ok, []}

  def extended_args(extends, name) do
    with {:module, extends} <- ensure_compiled(extends),
         {:commands_info, %{commands: commands}} <- commands_info(extends),
         {:extending?, true} <- {:extending?, name in commands},
         {:command_info, %{arguments: args}} <- command_info(extends, name) do
      {:ok, args}
    else
      {:extending?, false} -> {:ok, []}
      {:error, reason} -> {:error, "Extends module #{extends} not found, reason: #{reason}"}
    end
  end

  @doc """

  This function checks the `implemented_by` option.

  ## Examples

      iex> check_implemented_by(A, Example)

      iex> check_implemented_by(1, Example)
      {:error, "`:implemented_by` option `1` is not a valid module name"}

      iex> check_implemented_by(Example, 1)
      {:error, "`1` is not a valid module name"}

  """
  @spec check_implemented_by(module(), module()) :: {:ok, module()} | {:error, String.t()}
  def check_implemented_by(module, context) do
    cond do
      not is_atom(module) ->
        {:error, "`:implemented_by` option `#{inspect(module)}` is not a valid module name"}

      not is_atom(context) ->
        {:error, "`#{inspect(context)}` is not a valid module name"}

      true ->
        {:ok, module}
    end
  end

  @doc """

  This function checks that the implementation module contains the function in the command.

  ## Examples

      iex> defmodule A do
      ...>   def f(), do: :ok
      ...> end
      iex> {:ok, A} = implemented_by(A, :f, [])
      iex> implemented_by(A, :f, [:x])
      {:warn, "function `f/1` not found in module `MakinaDoctestTest.A`"}

      iex> implemented_by(A, :f, [:x])
      {:error, "could not load `A`"}

  """
  @spec implemented_by(module(), atom(), [atom()]) ::
          {:ok, module()} | {:error, String.t()} | {:warn, String.t()}
  def implemented_by(implemented_by, _name, _args) when not is_atom(implemented_by) do
    {:error, "`:implemented_by` should receive a module name"}
  end

  def implemented_by(module, name, args) do
    with {:module, ^module} <- ensure_compiled(module),
         {:defines?, true} <- {:defines?, {name, length(args)} in module.__info__(:functions)} do
      {:ok, module}
    else
      {:defines?, false} ->
        {:warn, "function `#{name}/#{length(args)}` not found in module `#{inspect(module)}`"}

      {:error, error} ->
        {:error, error}
    end
  end

  defp to_call_arg({arg, _}), do: Macro.var(arg, nil)
  defp to_call_arg(arg), do: Macro.var(arg, nil)
end
