defmodule Makina.Command.Callback do
  import Makina.Helpers

  alias Makina.Error

  @moduledoc false

  ##################################################################################################
  # Macros
  ##################################################################################################

  @doc """

  This macro configures the module to allow callback definitions.

  This macro registers the following module attributes:

  | attribute    | accumulate | initial value      | description             |
  |--------------|------------|--------------------|-------------------------|
  | `:command`   | `false`    | `opts[:command]`   | name of the command     |
  | `:state`     | `false`    | `opts[:state]`     | state attributes        |
  | `:arguments` | `false`    | `opts[:arguments]` | command arguments       |
  | `:callbacks` | `true`     | `[]`               | processed callbacks     |
  | `:info`      | `false`    | `opts[:info]`      | command declaration env |

  ## Examples

      iex> defmodule Example do
      ...>   use Makina.Command.Callback, state: []
      ...>   pre true
      ...>   args []
      ...>   valid_args true
      ...>   valid true
      ...>   call :ok
      ...>   next []
      ...>   post true
      ...>   weight 1
      ...>   features []
      ...> end

      iex> module = quote do
      ...>   defmodule Example do
      ...>     use Makina.Command.Callback
      ...>   end
      ...> end
      iex> message = capture_io(:stderr, fn -> Code.eval_quoted(module) end)
      iex> assert message =~ "callback `pre` undefined"
      iex> assert message =~ "callback `args` undefined"
      iex> assert message =~ "callback `valid_args` undefined"
      iex> assert message =~ "callback `valid` undefined"
      iex> assert message =~ "callback `call` undefined"
      iex> assert message =~ "callback `next` undefined"
      iex> assert message =~ "callback `post` undefined"
      iex> assert message =~ "callback `weight` undefined"
      iex> assert message =~ "callback `features` undefined"

  """
  @spec __using__(Keyword.t()) :: Macro.t()
  defmacro __using__(opts) do
    context = __CALLER__.module
    Module.register_attribute(context, :command, accumulate: false)
    Module.register_attribute(context, :state, accumulate: false)
    Module.register_attribute(context, :arguments, accumulate: false)
    Module.register_attribute(context, :callbacks, accumulate: true)
    Module.register_attribute(context, :info, accumulate: false)
    Module.register_attribute(context, :abstract, accumulate: false)

    command = Keyword.get(opts, :command)
    state = Keyword.get(opts, :state)
    arguments = Keyword.get(opts, :arguments, [])
    result = Keyword.get(opts, :result, quote(do: any()))
    info = Keyword.get(opts, :info, __CALLER__)
    abstract = Keyword.get(opts, :abstract, false)

    symbolic_state = Keyword.get(opts, :symbolic_state, quote(do: any()))
    dynamic_state = Keyword.get(opts, :dynamic_state, quote(do: any()))
    symbolic_updates = Keyword.get(opts, :symbolic_updates, quote(do: any()))
    dynamic_updates = Keyword.get(opts, :dynamic_updates, quote(do: any()))

    Module.put_attribute(context, :command, command)
    Module.put_attribute(context, :state, state)
    Module.put_attribute(context, :arguments, arguments)
    Module.put_attribute(context, :info, info)
    Module.put_attribute(context, :abstract, abstract)

    quote do
      require Makina.Exports
      import Makina.Exports, only: [symbolic: 1]
      import unquote(__MODULE__), only: :macros
      @before_compile unquote(__MODULE__)
      unquote(command_types(command, arguments, result))
      unquote(validate_args(Keyword.keys(arguments)))

      @type symbolic_state() :: unquote(symbolic_state)
      @type dynamic_state() :: unquote(dynamic_state)
      @type symbolic_updates() :: unquote(symbolic_updates)
      @type dynamic_updates() :: unquote(dynamic_updates)
      @type symbolic_expr() :: Makina.Types.symbolic_expr()
    end
  end

  @doc """

  This macro is used to write command preconditions. A precondition is the predicate that enables or
  disables the generation of a command.

  This macro reads the module attributes: `:state`, `:callbacks` and `:command`.

  This macro modifies the module attributes:

  | attribute    | value             |
  |--------------|-------------------|
  | `:callbacks `| [:pre]            |

  ## Examples

      iex> defmodule Example do
      ...>   require Makina.Command.Callback
      ...>   import Makina.Command.Callback, only: [pre: 1]
      ...>   Module.register_attribute(__MODULE__, :command, accumulate: false)
      ...>   @command :f
      ...>   @type symbolic_state() :: %{}
      ...>   pre true
      ...> end

  """

  @doc_template ~S"""

  This predicate enables or disables the generation of the command `<%= name %>`.

  ## Available variables
  ### State
  - `state` contains the complete symbolic state of the model.
  <%= for attr <- attrs, do: "- `#{attr}` attribute defined in the state declaration.\n"%>

  """
  @spec pre(Macro.t()) :: Macro.t()
  defmacro pre(do: block) do
    module = __CALLER__.module

    attrs = Module.get_attribute(module, :state, []) |> Enum.uniq()

    state = map_pattern(:state, attrs)
    args = [state]
    callbacks = Module.get_attribute(module, :callbacks, [])

    extra =
      if :pre in callbacks do
        quote do
          defoverridable pre: 1
        end
      else
        name = Module.get_attribute(module, :command)
        docs = EEx.eval_string(@doc_template, name: name, attrs: attrs)

        quote do
          @doc unquote(docs)
          @spec pre(symbolic_state()) :: boolean() | nil
        end
      end

    Module.put_attribute(module, :callbacks, :pre)
    block = insert_super_args(block, [:state], :pre, __CALLER__)

    quote do
      unquote(extra)
      def pre(), do: unquote(block)
    end
    |> Macro.postwalk([], &insert_args(&1, &2, args))
    |> elem(0)
  end

  defmacro pre(block), do: quote(do: pre(unquote(do: block)))

  @doc """

  This macro is used to write generators for the command parameters.

  This macro reads the module attributes: `:state`, `:callbacks`, `:command` and `:arguments`.

  This macro modifies the module attributes:

  | attribute     | value             |
  |---------------|-------------------|
  | `:callbacks`  | `[:args]`         |

  ## Examples

      iex> defmodule Example do
      ...>   require Makina.Command.Callback
      ...>   import Makina.Command.Callback, only: [args: 1]
      ...>   Module.register_attribute(__MODULE__, :command, accumulate: false)
      ...>   @command :f
      ...>   @type symbolic_arguments() :: %{}
      ...>   @type symbolic_state() :: %{}
      ...>   args true
      ...> end

  """
  @doc_template ~S"""

  This function returns the generator of arguments for the command `<%= name %>`.

  ## Available variables
  ### State
  - `state` contains all the attributes of the model.
  <%= for attr <- attrs, do: "- `#{attr}` defined in the model state.\n" %>

  """
  @spec args(Macro.t()) :: Macro.t()
  defmacro args(do: block) do
    module = __CALLER__.module
    attrs = Module.get_attribute(module, :state, []) |> Enum.uniq()
    state = map_pattern(:state, attrs)
    args = [state]

    callbacks = Module.get_attribute(module, :callbacks, [])

    extra =
      if :args in callbacks do
        quote do
          defoverridable args: 1
        end
      else
        name = Module.get_attribute(module, :command)
        docs = EEx.eval_string(@doc_template, name: name, attrs: attrs)

        quote do
          @doc unquote(docs)
          @spec args(symbolic_state()) :: Makina.Types.generator(symbolic_arguments())
        end
      end

    Module.put_attribute(module, :callbacks, :args)
    block = insert_super_args(block, [:state], :args, __CALLER__)

    quote do
      unquote(extra)
      def args(), do: unquote(block)
    end
    |> Macro.postwalk([], &insert_args(&1, &2, args))
    |> elem(0)
  end

  defmacro args(block), do: quote(do: args(unquote(do: block)))

  @doc """

  This macro is used to write validators of command parameters.

  This macro reads the module attributes: `:state`, `:callbacks` `:command` and `:arguments`.

  This macro modifies the module attributes:

  | attribute     | value             |
  |---------------|-------------------|
  | `:callbacks ` | `[:valid_args]`   |

  ## Examples

      iex> defmodule Example do
      ...>   require Makina.Command.Callback
      ...>   import Makina.Command.Callback, only: [valid_args: 1]
      ...>   Module.register_attribute(__MODULE__, :command, accumulate: false)
      ...>   @command :f
      ...>   @type symbolic_state() :: %{}
      ...>   @type symbolic_arguments() :: %{}
      ...>   valid_args true
      ...> end

  """

  @doc_template ~S"""

  This function checks the correctness of the arguments generated for the command `<%= name %>`.

  ## Available variables
  ### State
  - `state` contains the complete symbolic state of the model.
  <%= for attr <- attrs, do: "- `#{attr}` attribute defined in the state declaration.\n" %>
  ### Arguments
  - `arguments` contains all the generated arguments of the command.
  <%= for arg <- args, do: "- `#{arg}` argument defined in the command declaration.\n" %>

  """
  @spec valid_args(Macro.t()) :: Macro.t()
  defmacro valid_args(do: block) do
    module = __CALLER__.module
    attrs = Module.get_attribute(module, :state, []) |> Enum.uniq()
    args_names = Module.get_attribute(module, :arguments, []) |> Enum.uniq() |> Keyword.keys()
    state = map_pattern(:state, attrs)
    arguments = map_pattern(:arguments, args_names)
    args = [state, arguments]
    callbacks = Module.get_attribute(module, :callbacks, [])

    extra =
      if :valid_args in callbacks do
        quote do
          defoverridable valid_args: 2
        end
      else
        name = Module.get_attribute(module, :command)
        docs = EEx.eval_string(@doc_template, name: name, attrs: attrs, args: args_names)

        quote do
          @doc unquote(docs)
          @spec valid_args(symbolic_state(), symbolic_arguments()) :: boolean() | nil
          # TODO: check what this function does
          def call_pre() do
            valid_args(unquote_splicing([:state, :arguments] |> Enum.map(&Macro.var(&1, nil))))
          end
        end
      end

    Module.put_attribute(module, :callbacks, :valid_args)
    block = insert_super_args(block, [:state, :arguments], :valid_args, __CALLER__)

    quote do
      unquote(extra)
      def valid_args(), do: unquote(block)
    end
    |> Macro.postwalk([], &insert_args(&1, &2, args))
    |> elem(0)
  end

  defmacro valid_args(block), do: quote(do: valid_args(unquote(do: block)))

  @doc """

  This macro is used to write function that interact with the system-under test.

  This macro reads the module attributes: `:callbacks` `:command` and `:arguments`.

  This macro modifies the module attributes:

  | attribute     | value             |
  |---------------|-------------------|
  | `:callbacks ` | `[:call]`         |

  ## Examples

      iex> defmodule Example do
      ...>   require Makina.Command.Callback
      ...>   import Makina.Command.Callback, only: [call: 1]
      ...>   Module.register_attribute(__MODULE__, :command, accumulate: false)
      ...>   @command :f
      ...>   @type dynamic_arguments() :: %{}
      ...>   @type dynamic_result :: any()
      ...>   call true
      ...> end

  """

  @doc_template ~S"""

  This function contains the call to the real implementation of the command under test.

  ## Available variables
  ### Arguments
  - `arguments` contains all the generated arguments of the command.
  <%= for arg <- args, do: "- `#{arg}` argument defined in the command declaration.\n" %>

  """
  @spec call(Macro.t()) :: Macro.t()
  defmacro call(do: block) do
    module = __CALLER__.module
    args_names = Module.get_attribute(module, :arguments, []) |> Enum.uniq() |> Keyword.keys()
    arguments = map_pattern(:arguments, args_names)
    args = [arguments]

    callbacks = Module.get_attribute(module, :callbacks, [])

    extra =
      if :call in callbacks do
        quote do
          defoverridable call: 1
        end
      else
        docs = EEx.eval_string(@doc_template, args: args_names)

        quote do
          @doc unquote(docs)
          @spec call(dynamic_arguments()) :: dynamic_result()
        end
      end

    Module.put_attribute(module, :callbacks, :call)
    block = insert_super_args(block, [:arguments], :call, __CALLER__)

    quote do
      unquote(extra)
      def call(), do: unquote(block)
    end
    |> Macro.postwalk([], &insert_args(&1, &2, args))
    |> elem(0)
  end

  defmacro call(block), do: quote(do: call(unquote(do: block)))

  @doc """

  This macro is used to write the updates to the state after the execution of a command.

  This macro reads the module attributes: `:state`, `:callbacks` `:command` and `:arguments`.

  This macro modifies the module attributes:

  | attribute     | value             |
  |---------------|-------------------|
  | `:callbacks ` | `[:next]`         |

  ## Examples

      iex> defmodule Example do
      ...>   require Makina.Command.Callback
      ...>   import Makina.Command.Callback, only: [next: 1]
      ...>   Module.register_attribute(__MODULE__, :command, accumulate: false)
      ...>   @command :f
      ...>   @type symbolic_state() :: %{}
      ...>   @type symbolic_arguments() :: %{}
      ...>   @type symbolic_result() :: Makina.Types.symbolic_var()
      ...>   @type symbolic_updates() :: %{}
      ...>   @type dynamic_state() :: %{}
      ...>   @type dynamic_arguments() :: %{}
      ...>   @type dynamic_result() :: any()
      ...>   @type dynamic_updates() :: %{}
      ...>   next true
      ...> end

  """
  @doc_template ~S"""
  This function computes the next state of the model after the execution of the command `<%= name %>`.

  ## Available variables
  ### State
  - `state` contains the complete symbolic state of the model.
  <%= for attr <- attrs, do: "- `#{attr}` attribute defined in the state declaration.\n" %>
  ### Arguments
  - `arguments` contains all the generated arguments of the command.
  <%= for arg <- args, do: "- `#{arg}` argument defined in the command declaration.\n" %>
  ### Others
  - `result` variable that contains the symbolic result of the command execution.
  - `valid` variable that contains the result of the evaluation of the `valid` predicate.

  """
  @spec next(Macro.t()) :: Macro.t()
  defmacro next(do: block) do
    module = __CALLER__.module
    attrs = Module.get_attribute(module, :state, []) |> Enum.uniq()
    args_names = Module.get_attribute(module, :arguments, []) |> Enum.uniq() |> Keyword.keys()
    state = map_pattern(:state, attrs)
    arguments = map_pattern(:arguments, args_names)
    result = Macro.var(:result, nil)
    valid = Macro.var(:valid, nil)
    args = [state, arguments, result, valid]
    callbacks = Module.get_attribute(module, :callbacks, [])

    extra =
      if :next in callbacks do
        quote do
          defoverridable next: 4
        end
      else
        name = Module.get_attribute(module, :command)
        docs = EEx.eval_string(@doc_template, attrs: attrs, args: args_names, name: name)

        quote do
          @doc unquote(docs)
          @spec next(symbolic_state(), symbolic_arguments(), symbolic_result(), boolean()) ::
                  [symbolic_updates()] | nil
          @spec next(dynamic_state(), dynamic_arguments(), dynamic_result(), boolean()) ::
                  [dynamic_updates()] | nil
        end
      end

    Module.put_attribute(module, :callbacks, :next)

    block =
      quote do
        unquote(block)
      end
      |> insert_super_args([:state, :arguments, :result, :valid], :next, __CALLER__)
      |> Macro.postwalk(&Macro.expand(&1, __CALLER__))

    quote do
      unquote(extra)

      def next() do
        unquote(block)
      end
    end
    |> Macro.postwalk([], &insert_args(&1, &2, args))
    |> elem(0)
  end

  defmacro next(block), do: quote(do: next(unquote(do: block)))

  @doc """

  This macro is used to write the postcondition of a command.

  This macro reads the module attributes: `:state`, `:callbacks`, `:command`and `:arguments`.

  This macro modifies the module attributes:

  | attribute     | value             |
  |---------------|-------------------|
  | `:callbacks ` | `[:post]`         |

  ## Examples

      iex> defmodule Example do
      ...>   require Makina.Command.Callback
      ...>   import Makina.Command.Callback, only: [post: 1]
      ...>   Module.register_attribute(__MODULE__, :command, accumulate: false)
      ...>   @command :f
      ...>   @type dynamic_state() :: %{}
      ...>   @type dynamic_arguments() :: %{}
      ...>   @type dynamic_result :: any()
      ...>   post true
      ...> end

  """

  @doc_template ~S"""

  This predicate checks the correctness of the execution of the command `<%= name %>`.

  ## Available variables
  ### State
  - `state` contains the complete dynamic state of the model.
  <%= for attr <- attrs, do: "- `#{attr}` attribute defined in the state declaration.\n" %>
  ### Arguments
  - `arguments` contains all the generated arguments of the command.
  <%= for arg <- args, do: "- `#{arg}` argument defined in the command declaration.\n" %>
  ### Result
  - `result` variable that contains the result of the command execution.
  - `valid` variable that contains the result of the evaluation of the `valid` predicate.

  """
  @spec post(Macro.t()) :: Macro.t()
  defmacro post(do: block) do
    module = __CALLER__.module
    attrs = Module.get_attribute(module, :state, []) |> Enum.uniq()
    args_names = Module.get_attribute(module, :arguments, []) |> Enum.uniq() |> Keyword.keys()
    state = map_pattern(:state, attrs)
    arguments = map_pattern(:arguments, args_names)
    result = Macro.var(:result, nil)
    valid = Macro.var(:valid, nil)
    args = [state, arguments, result, valid]
    callbacks = Module.get_attribute(module, :callbacks, [])

    extra =
      if :post in callbacks do
        quote do
          defoverridable post: 4
        end
      else
        name = Module.get_attribute(module, :command)
        docs = EEx.eval_string(@doc_template, attrs: attrs, args: args_names, name: name)

        quote do
          @doc unquote(docs)
          @spec post(dynamic_state(), dynamic_arguments(), dynamic_result(), boolean()) ::
                  boolean() | nil
        end
      end

    Module.put_attribute(module, :callbacks, :post)
    block = insert_super_args(block, [:state, :arguments, :result, :valid], :post, __CALLER__)

    quote do
      unquote(extra)
      def post(), do: unquote(block)
    end
    |> Macro.postwalk([], &insert_args(&1, &2, args))
    |> elem(0)
  end

  defmacro post(block), do: quote(do: post(unquote(do: block)))

  @doc """

  This macro is used to write `features` of a command. This callback works only when using EQC.

  This macro reads the module attributes: `:state`, `:callbacks`, `:command` and `:arguments`.

  This macro modifies the module attributes:

  | attribute     | value             |
  |---------------|-------------------|
  | `:callbacks ` | `[:features]`     |

  ## Examples

      iex> defmodule Example do
      ...>   require Makina.Command.Callback
      ...>   import Makina.Command.Callback, only: [features: 1]
      ...>   Module.register_attribute(__MODULE__, :command, accumulate: false)
      ...>   @command :f
      ...>   @type dynamic_state() :: %{}
      ...>   @type dynamic_arguments() :: %{}
      ...>   @type dynamic_result :: any()
      ...>   features true
      ...> end

  """

  @doc_template ~S"""

  This function returns a list containing the features tested by`<%= name %>`

  ## Available variables
  ### State
  - `state` contains the complete dynamic state of the model.
  <%= for attr <- attrs, do: "- `#{attr}` attribute defined in the state declaration.\n" %>
  ### Arguments
  - `arguments` contains all the generated arguments of the command.
  <%= for arg <- args, do: "- `#{arg}` argument defined in the command declaration.\n" %>
  ### Result
  - `result` variable that contains the result of the command execution.

  """
  @spec features(Macro.t()) :: Macro.t()
  defmacro features(do: block) do
    module = __CALLER__.module
    attrs = Module.get_attribute(module, :state, []) |> Enum.uniq()
    args_names = Module.get_attribute(module, :arguments, []) |> Enum.uniq() |> Keyword.keys()
    state = map_pattern(:state, attrs)
    arguments = map_pattern(:arguments, args_names)
    result = Macro.var(:result, nil)
    args = [state, arguments, result]
    callbacks = Module.get_attribute(module, :callbacks, [])

    extra =
      if :features in callbacks do
        quote do
          defoverridable features: 3
        end
      else
        name = Module.get_attribute(module, :command)
        docs = EEx.eval_string(@doc_template, name: name, args: args_names, attrs: attrs)

        quote do
          @doc unquote(docs)
          @spec features(dynamic_state(), dynamic_arguments(), dynamic_result()) :: [any()] | nil
        end
      end

    Module.put_attribute(module, :callbacks, :features)
    block = insert_super_args(block, [:state, :arguments, :result], :features, __CALLER__)

    quote do
      unquote(extra)
      def features(), do: unquote(block)
    end
    |> Macro.postwalk([], &insert_args(&1, &2, args))
    |> elem(0)
  end

  defmacro features(block), do: quote(do: features(unquote(do: block)))

  @doc """

  This macro is used to write the weight of a command.

  This macro reads the module attributes: `:state`, `:command` and `:callbacks`.

  This macro modifies the module attributes:

  | attribute     | value             |
  |---------------|-------------------|
  | `:callbacks ` | `[:weight]`       |

  ## Examples

      iex> defmodule Example do
      ...>   require Makina.Command.Callback
      ...>   import Makina.Command.Callback, only: [weight: 1]
      ...>   Module.register_attribute(__MODULE__, :command, accumulate: false)
      ...>   @command :f
      ...>   @type symbolic_state() :: %{}
      ...>   weight true
      ...> end

  """

  @doc_template ~S"""

  This function returns the weight of `<%= name %>` during test generation.

  ## Available variables
  ### State
  - `state` contains the complete symbolic state of the model.
  <%= for attr <- attrs, do: "- `#{attr}` attribute defined in the state declaration.\n" %>

  """
  @spec weight(Macro.t()) :: Macro.t()
  defmacro weight(do: block) do
    module = __CALLER__.module
    attrs = Module.get_attribute(module, :state, []) |> Enum.uniq()
    state = map_pattern(:state, attrs)
    args = [state]
    callbacks = Module.get_attribute(module, :callbacks, [])

    extra =
      if :weight in callbacks do
        quote do
          defoverridable weight: 1
        end
      else
        name = Module.get_attribute(module, :command)
        docs = EEx.eval_string(@doc_template, name: name, attrs: attrs)

        quote do
          @doc unquote(docs)
          @spec weight(symbolic_state()) :: integer()
        end
      end

    Module.put_attribute(module, :callbacks, :weight)
    block = insert_super_args(block, [:state], :weight, __CALLER__)

    quote do
      unquote(extra)
      def weight(), do: unquote(block)
    end
    |> Macro.postwalk([], &insert_args(&1, &2, args))
    |> elem(0)
  end

  defmacro weight(block), do: quote(do: weight(unquote(do: block)))

  @doc """

  This macro is used to write predicates that check the validity of a generated command. The result
  of valid can be accessed in `post` and `next`.

  This macro reads the module attributes: `:state`, `:arguments`, `:command` and `:callbacks`.

  This macro modifies the module attributes:

  | attribute     | value             |
  |---------------|-------------------|
  | `:callbacks ` | `[:valid]`        |

  ## Examples

      iex> defmodule Example do
      ...>   require Makina.Command.Callback
      ...>   import Makina.Command.Callback, only: [valid: 1]
      ...>   Module.register_attribute(__MODULE__, :command, accumulate: false)
      ...>   @command :f
      ...>   @type symbolic_state() :: %{}
      ...>   @type symbolic_arguments() :: %{}
      ...>   @type symbolic_result() :: Makina.Types.symbolic_var()
      ...>   @type dynamic_state() :: %{}
      ...>   @type dynamic_arguments() :: %{}
      ...>   @type dynamic_result() :: :ok
      ...>   valid true
      ...> end

  """

  @doc_template ~S"""

  This predicate can be used to indicate that a produce call to a command is correct.

  ## Available variables
  ### State
  - `state` contains the complete symbolic state of the model.
  <%= for attr <- attrs, do: "- `#{attr}` attribute defined in the state declaration.\n" %>
  ### Arguments
  - `arguments` contains all the generated arguments of the command.
  <%= for arg <- args, do: "- `#{arg}` argument defined in the command declaration.\n" %>

  """
  @spec valid(Macro.t()) :: Macro.t()
  defmacro valid(do: block) do
    module = __CALLER__.module
    attrs = Module.get_attribute(module, :state, []) |> Enum.uniq()
    args_names = Module.get_attribute(module, :arguments, []) |> Enum.uniq() |> Keyword.keys()
    state = map_pattern(:state, attrs)
    arguments = map_pattern(:arguments, args_names)
    result = Macro.var(:result, nil)
    args = [state, arguments, result]
    callbacks = Module.get_attribute(module, :callbacks, [])

    extra =
      if :valid in callbacks do
        quote do
          defoverridable valid: 3
        end
      else
        name = Module.get_attribute(module, :command)
        docs = EEx.eval_string(@doc_template, name: name, args: args_names, attrs: attrs)

        quote do
          @docs unquote(docs)
          @spec valid(symbolic_state(), symbolic_arguments(), symbolic_result()) :: boolean() | nil
          @spec valid(dynamic_state(), dynamic_arguments(), dynamic_result()) :: boolean() | nil
        end
      end

    Module.put_attribute(module, :callbacks, :valid)
    block = insert_super_args(block, [:state, :arguments, :result], :valid, __CALLER__)

    quote do
      unquote(extra)
      def valid(), do: unquote(block)
    end
    |> Macro.postwalk([], &insert_args(&1, &2, args))
    |> elem(0)
  end

  defmacro valid(block), do: quote(do: valid(unquote(do: block)))

  @doc """

  This macro throws a warning during compilation if the command does not contain some callback.

  This macro reads the attributes `:callbacks`, `:info`, `:command` and `:moduledoc`.

  ## Examples

      iex> defmodule Example do
      ...>   Module.register_attribute(__MODULE__, :info, accumulate: false)
      ...>   Module.register_attribute(__MODULE__, :callbacks, accumulate: false)
      ...>   @callbacks [:pre, :args, :valid_args, :call, :next, :weight, :post, :features, :valid]
      ...>   @info __ENV__
      ...>   @before_compile Makina.Command.Callback
      ...> end

      iex> module = quote do
      ...>   defmodule Example do
      ...>     Module.register_attribute(__MODULE__, :info, accumulate: false)
      ...>     Module.register_attribute(__MODULE__, :callbacks, accumulate: false)
      ...>     @callbacks []
      ...>     @info __ENV__
      ...>     @before_compile Makina.Command.Callback
      ...>   end
      ...> end
      iex> message = capture_io(:stderr, fn -> Code.eval_quoted(module) end)
      iex> assert message =~ "callback `pre` undefined"
      iex> assert message =~ "callback `args` undefined"
      iex> assert message =~ "callback `valid_args` undefined"
      iex> assert message =~ "callback `valid` undefined"
      iex> assert message =~ "callback `call` undefined"
      iex> assert message =~ "callback `next` undefined"
      iex> assert message =~ "callback `post` undefined"
      iex> assert message =~ "callback `weight` undefined"
      iex> assert message =~ "callback `features` undefined"

      iex> module = quote do
      ...>   defmodule Example do
      ...>     Module.register_attribute(__MODULE__, :info, accumulate: false)
      ...>     Module.register_attribute(__MODULE__, :callbacks, accumulate: false)
      ...>     Module.register_attribute(__MODULE__, :abstract, accumulate: false)
      ...>     @callbacks []
      ...>     @info __ENV__
      ...>     @abstract true
      ...>     @before_compile Makina.Command.Callback
      ...>   end
      ...> end
      iex> message = capture_io(:stderr, fn -> Code.eval_quoted(module) end)
      iex> assert message =~ "callback `pre` undefined"
      iex> assert message =~ "callback `args` undefined"
      iex> assert message =~ "callback `valid_args` undefined"
      iex> assert message =~ "callback `valid` undefined"
      iex> assert message =~ "callback `next` undefined"
      iex> assert message =~ "callback `post` undefined"
      iex> assert message =~ "callback `weight` undefined"
      iex> assert message =~ "callback `features` undefined"


  """

  @doc_template ~S"""

  This module contains the functions necessary to generate and execute the command `<%= name %>`.
  <%= if userdocs, do: elem(userdocs, 1) %>
  ## Defined callbacks
  <%= for c <- callbacks, do: "- `#{c}`\n" %>
  Detailed information about each callback can be accessed inside the interpreter:

      iex> h <%= inspect(module) %>.post

  """
  @spec __before_compile__(Macro.Env.t()) :: Macro.t()
  defmacro __before_compile__(_env) do
    context = __CALLER__.module
    callbacks = Module.get_attribute(context, :callbacks, []) |> Enum.uniq()
    info = Module.get_attribute(context, :info)

    abstract_callbacks =
      if Module.get_attribute(context, :abstract) do
        []
      else
        [:call]
      end

    ([:pre, :args, :valid_args, :next, :weight, :post, :features, :valid] ++ abstract_callbacks)
    |> Enum.filter(&(&1 not in callbacks))
    |> then(fn
      [] -> :none
      [cb] -> Error.throw_warn("callback `#{cb}` undefined.", info)
      cbs -> Enum.map(cbs, fn cb -> Error.throw_warn("callback `#{cb}` undefined.", info) end)
    end)

    userdocs = Module.get_attribute(context, :moduledoc)
    name = Module.get_attribute(context, :command)

    docs =
      EEx.eval_string(@doc_template,
        name: name,
        callbacks: callbacks,
        module: context,
        userdocs: userdocs
      )

    quote do
      @moduledoc unquote(docs)
    end
  end

  ##################################################################################################
  # Helpers
  ##################################################################################################

  @doc """

  This function generates the type definitions of a command.

  ## Examples

      iex> type = quote do: integer()
      iex> [_args, _result] = command_types(:f, [arg: type], type)

  """
  @spec command_types(atom(), [{atom(), Macro.t()}], Macro.t()) :: [Macro.t()]
  def command_types(name, arguments, result) do
    args_t = arguments
    result_t = result

    [args_type(args_t, name), result_type(result_t, name)]
  end

  @doc """

  This function generates the type that represents the arguments of the command.

  ## Examples

      TODO test with symbolic and dynamic types

      iex> type = quote do: integer()
      iex> args_type([arg1: type], :f)

  """

  @doc_template ~S"""

  This type represent the arguments for command `<%= name %>`.
  <%= if args != [] do %>
  Contains the named arguments:
  <%  args = for {arg, _} <- args, do: " - `#{arg}`." %>
  <%= Enum.join(args, "\n") %>
  <% else %>
  Contains no named arguments.
  <% end %>

  """
  @spec args_type(Keyword.t(Macro.t()), atom()) :: Macro.t()
  def args_type(args_types, name) do
    args_types =
      for {name, type} <- args_types do
        if equivalent_type?(unknown_type(), type) do
          {name, any_type()}
        else
          {name, type}
        end
      end

    docs = EEx.eval_string(@doc_template, args: args_types, name: name)

    symbolic = Enum.map(args_types, fn t -> Macro.postwalk(t, &symbolic_type/1) end)
    dynamic = Enum.map(args_types, fn t -> Macro.postwalk(t, &dynamic_type/1) end)

    quote do
      @typedoc unquote(docs)
      @type symbolic_arguments() :: %{unquote_splicing(symbolic)}
      @type dynamic_arguments() :: %{unquote_splicing(dynamic)}
    end
  end

  @doc """

  This function generates the type that represents result of the command.

  ## Examples

      iex> type = quote do: integer()
      iex> result_type(type, :f)

  """

  @doc_template ~S"""

  This type represents the result of the command `<%= name %>`.

  """
  @spec result_type(Macro.t(), atom()) :: Macro.t()
  def result_type(result_t, name) do
    docs = EEx.eval_string(@doc_template, name: name)
    result_t = if equivalent_type?(unknown_type(), result_t), do: any_type(), else: result_t

    quote do
      @doc unquote(docs)
      @type dynamic_result() :: unquote(result_t)
      @type symbolic_result() :: Makina.Types.symbolic_var()
    end
  end

  @doc """

  This function generates a function to validate the generated arguments for a command.

  ## Examples

      iex> validate_args([:arg1, :arg2])

  """

  @doc_template ~S"""

  This function checks the correctness of the given arguments.

  """
  @spec validate_args([atom()]) :: Macro.t()
  def validate_args(arguments) do
    quote do
      @doc unquote(EEx.eval_string(@doc_template))
      @spec validate_args(symbolic_arguments()) ::
              {:ok, symbolic_arguments()} | {:error, String.t()}
      @spec validate_args(dynamic_arguments()) :: {:ok, dynamic_arguments()} | {:error, String.t()}
      def validate_args(arguments) when is_map(arguments) do
        args = unquote(arguments)
        missing = Enum.filter(args, &(&1 not in Map.keys(arguments)))
        extra = Enum.filter(Map.keys(arguments), &(&1 not in args))

        if missing == [] and extra == [] do
          {:ok, arguments}
        else
          missing =
            if missing != [] do
              Makina.Helpers.atoms_to_string(missing)
              |> then(&"missing arguments: #{&1}")
            else
              ""
            end

          extra =
            if extra != [] do
              Makina.Helpers.atoms_to_string(extra)
              |> then(&"extra arguments: #{&1}")
            else
              ""
            end

          sep = if missing != "" and extra != "", do: ", ", else: ""

          {:error, missing <> sep <> extra}
        end
      end

      def validate_args(_arguments), do: {:error, "generated arguments should be inside a map"}
    end
  end
end
