defmodule Makina.Invariants do
  import Makina.Helpers

  alias Makina.Error

  @moduledoc false

  @typedoc """

  This type contains the options to configure the state of a model:
  - `:extends` contains the model to be extended.

  """
  @type option() :: {:docs, boolean()} | {:specs, boolean()} | {:extends, module()} | {:debug, Macro.t()}

  @options [:extends, :debug]

  ##################################################################################################
  # Macros
  ##################################################################################################

  @doc """

  This macro configures the module to allow invariants definitions. Accepts global options, this
  means that options passed to this macro are the default options for the invariants declarations in
  the module. Supported options are defined by the `#{inspect(__MODULE__)}.option()` type.

  This macro registers the following module attributes:

  | attribute              | accumulate | initial value | description                               |
  |------------------------|------------|---------------|-------------------------------------------|
  | `:invariants`          | `true`     | `[]`          | names of the invariants                   |
  | `:invariants_options`  | `true`     | `options`     | global options for invariants             |
  | `:invariants_declared` | `false`    | `false`       | `true` if invariants are already declared |
  | `:invariants_info`     | `false`    | `nil`         | information about the environment         |
  | `:invariants_code`     | `true`     | `[]`          | code containing invariant information     |
  | `:invariant_info`      | `true`     | `[]`          | information about every invariant         |

  ## Examples

      iex> defmodule Example do
      ...>   use Makina.Invariants
      ...> end
      iex> defmodule Example2 do
      ...>   use Makina.Invariants, extends: Example
      ...> end

      iex> defmodule Example do
      ...>   use Makina.Invariants, extends: "name"
      ...> end
      ** (Makina.Error) `"name"` is not a valid module name

      iex> defmodule Example do
      ...>   use Makina.Invariants, extends: A
      ...> end
      ** (Makina.Error) could not load `A`

      iex> defmodule Example do
      ...>   use Makina.Invariants, extends: Enum
      ...> end
      ** (Makina.Error) module `Enum` does not contain invariants

      iex> defmodule Example do
      ...>   use Makina.Invariants, extends: Example
      ...> end
      ** (Makina.Error) `MakinaDoctestTest.Example` cannot extend itself

  """
  @spec __using__([option()]) :: Macro.t()
  defmacro __using__(options) do
    context = __CALLER__.module
    options = Enum.map(options, fn {k, v} -> {k, Macro.expand(v, __CALLER__)} end)

    with {:option, {:ok, extends}} <- {:option, Keyword.fetch(options, :extends)},
         {:ok, extends} <- validate_extends(extends, context),
         {:invariants_info, _info} <- invariants_info(extends) do
      :ok
    else
      {:option, :error} -> :ok
      {:error, error} -> {:error, error}
    end
    |> then(fn
      :ok -> :ok
      {:error, message} -> Error.throw_error(message, __CALLER__)
    end)

    Module.register_attribute(context, :invariants, accumulate: true)
    Module.register_attribute(context, :invariants_options, accumulate: true)
    Module.register_attribute(context, :invariants_declared, accumulate: false)
    Module.register_attribute(context, :invariants_info, accumulate: false)
    Module.register_attribute(context, :invariants_code, accumulate: true)
    Module.register_attribute(context, :invariant_info, accumulate: true)
    # Extracts and registers the global options passed to `use`.
    register_options(__CALLER__, :invariants_options, @options, options)
    Module.put_attribute(context, :invariants_declared, false)

    quote do
      import unquote(__MODULE__), only: :macros
      @before_compile unquote(__MODULE__)
    end
  end

  @doc """

  This macro is used to declare invariants in a model. Each invariant is stored in its own
  module. Receives a keyword list that contains the name of the invariant and the predicate that
  checks. If the module contains a state declaration, expressions inside invariants declaration can
  access state attributes.

  This macro reads the following module attributes: `:invariants_declared`, `:invariants_options`
  and `:invariants`.

  This macro modifies the following module attributes:

  | attribute              | value        |
  |------------------------|--------------|
  | `:invariants_declared` | `true`       |
  | `:invariants_info`     | `__CALLER__` |
  | `:invariants`          | `[atom()]`   |

  ## Examples

      iex> defmodule Example do
      ...>   use Makina.Invariants
      ...>   invariants invariant1: is_map(state)
      ...> end
      iex> defmodule ExampleExtended do
      ...>   use Makina.Invariants
      ...>   invariants invariant2: true
      ...> end

      iex> defmodule Example do
      ...>   use Makina.Invariants
      ...>   invariants :no_keyword_list
      ...> end
      ** (Makina.Error) `invariants` receives a keyword list `invariant_name: ` `expression`

      iex> defmodule Example do
      ...>   use Makina.Invariants
      ...>   invariants attr1: 1 :: integer()
      ...>   invariants attr2: 0
      ...> end
      ** (Makina.Error) multiple `invariants` declarations

      iex> defmodule Example do
      ...>   use Makina.Invariants
      ...>   invariants attr1: 1 :: integer(), attr1: 1
      ...> end
      ** (Makina.Error) multiple declarations of invariants: `:attr1`

  """
  @spec invariants(Keyword.t()) :: Macro.t()
  defmacro invariants(invariants \\ []) do
    context = __CALLER__.module
    options = Module.get_attribute(context, :invariants_options)

    with {:declared, false} <- {:declared, Module.get_attribute(context, :invariants_declared)},
         {:keyword, true} <- {:keyword, Keyword.keyword?(invariants)},
         extends = Keyword.get(options, :extends),
         {:extends, {:ok, extending_invs}} <- {:extends, extends(extends, context)},
         freqs <-
           (invariants ++ Enum.map(extending_invs, fn {n, e, _} -> {n, e} end))
           |> Keyword.keys()
           |> Enum.frequencies(),
         repeated <- Enum.filter(freqs, fn {_, v} -> v > 1 end),
         {:repeated, []} <- {:repeated, Keyword.keys(repeated)} do
      for {name, expr} <- invariants do
        Module.put_attribute(context, :invariants, name)
        Module.put_attribute(context, :invariants_code, {name, expr})
        Module.put_attribute(context, :invariant_info, {name, context})
      end

      for {name, expr, defined_in} <- extending_invs do
        Module.put_attribute(context, :invariants, name)
        Module.put_attribute(context, :invariants_code, {name, expr})
        Module.put_attribute(context, :invariant_info, {name, defined_in})
      end

      :ok
    else
      {:declared, true} ->
        {:error, "multiple `invariants` declarations"}

      {:keyword, false} ->
        {:error, "`invariants` receives a keyword list `invariant_name: ` `expression`"}

      {:repeated, repeated} ->
        {:error, "multiple declarations of invariants: #{atoms_to_string(repeated)}"}

      {:extends, error} ->
        error

      error ->
        {:error, "unknown error #{error}"}
    end
    |> then(fn
      :ok -> :ok
      {:error, message} -> Error.throw_error(message, __CALLER__)
    end)

    Module.put_attribute(context, :invariants_declared, true)
    Module.put_attribute(context, :invariants_info, __CALLER__)

    []
  end

  @doc """

  This macro injects the types and teh functions derived from the invariants declaration.

  This macro reads the following module attributes: `:invariants_declared`, `:invariants`,
  `:invariants_info`, `:invariants_code` and `:attributes`

  """

  @doc_template ~S"""

  This module contains the invariants of the model `<%= model %>`.

  ## Invariants
  <%= for inv <- invs, do: "- `#{inv}`\n" %>
  Detailed information about each invariant can be accessed inside the interpreter:

      iex> h <%= module %>.Invariants.invariant

  """
  @spec __before_compile__(atom | %{:module => atom, optional(any) => any}) :: [] | Macro.t()
  defmacro __before_compile__(env) do
    context = env.module
    state_module = state_module_name(context)
    invariants_module = invariants_module_name(context)
    options = Module.get_attribute(context, :invariants_options, [])
    debug = Keyword.get(options, :debug, Application.get_env(:makina, :debug))
    info = Module.get_attribute(context, :invariant_info)

    {invariants, invariants_code, invariants_info, env} =
      cond do
        Module.get_attribute(env.module, :invariants_declared) ->
          invariants = Module.get_attribute(context, :invariants)
          env = Module.get_attribute(context, :invariants_info)
          {invariants, Module.get_attribute(context, :invariants_code), info, env}

        extends = Keyword.get(options, :extends) ->
          case extends(extends, context) do
            {:ok, extending_invs} ->
              Enum.reduce(extending_invs, {[], [], [], env}, fn {name, expr, inv_info},
                                                                {invs, code, info, env} ->
                {[name | invs], [{name, expr} | code], [{name, inv_info} | info], env}
              end)

            {:error, message} ->
              Error.throw_error(message, __CALLER__)
          end

        true ->
          {[], [], [], env}
      end

    docs = EEx.eval_string(@doc_template, model: context, invs: invariants, module: context)

    quote do
      unquote(debug)
      @moduledoc unquote(docs)
      @type state() :: unquote(state_module).dynamic_state()

      def __makina_info__() do
        %{invariants: unquote(invariants), module: unquote(invariants_module)}
      end

      unquote(check(invariants, context))
      unquote(import_definitions(context))
    end
    |> then(&Makina.Module.create(invariants_module, &1, env))

    for {name, expr} <- invariants_code do
      invariant_module = invariant_module_name(name, context)
      attrs = Module.get_attribute(context, :attributes, []) |> Enum.uniq()
      state = map_pattern(:state, attrs)

      {_, invariant_info} =
        Enum.find(invariants_info, fn
          {name, _} -> name == name
          x -> IO.inspect(x)
        end)

      args = [state]

      doc_template = ~S"""

      This function contains the definition of the invariant `<%= name %>`.

      ## Available variables
      ### State
      - `state` contains the complete symbolic state of the model.
      <%= for attr <- attrs, do: "- `#{attr}` attribute defined in the state declaration.\n" %>

      """

      docs = EEx.eval_string(doc_template, name: name, attrs: attrs)

      {check, _} =
        quote do
          @spec check(state()) :: boolean() | nil
          @doc unquote(docs)
          def check(), do: unquote(expr)
        end
        |> Macro.postwalk([], &insert_args(&1, &2, args))

      quote do
        unquote(debug)
        @type state() :: unquote(invariants_module).state()
        def __makina_info__() do
          %{
            name: unquote(name),
            module: unquote(invariant_module),
            defined_in: unquote(invariant_info)
          }
        end

        unquote(check)
        unquote(import_definitions(context))
        unquote(import_types(context))
      end
      |> then(&Makina.Module.create(invariant_module, &1, __CALLER__))
    end

    []
  end

  ##################################################################################################
  # Options Helpers
  ##################################################################################################

  @doc """

  This function extracts the invariants defined in the extending model.

  ## Examples

      iex> defmodule Example do
      ...>   use Makina.Invariants
      ...>   invariants invariant1: true
      ...> end
      iex> defmodule ExampleExtended do
      ...>   use Makina.Invariants, extends: Example
      ...>   invariants invariant2: true
      ...> end
      iex> {:ok, [{:invariant1, invariant1, _}]} = extends(Example, ExampleExtended)
      iex> invariant1 |> Macro.to_string()
      quote do
        MakinaDoctestTest.Example.Invariant.Invariant1.check(state)
      end |> Macro.to_string()

  """
  @spec extends(module() | nil, module()) ::
          {:ok, [{atom(), Macro.t(), module()}]} | {:error, String.t()}
  def extends(nil, _module), do: {:ok, []}

  def extends(extends, module) do
    with {:ok, extends} <- validate_extends(extends, module),
         {:invariants_info, %{invariants: invariants}} <- invariants_info(extends) do
      for name <- invariants do
        {:invariant_info, inv_info} = invariant_info(extends, name)
        %{module: invariant_module} = inv_info
        expr = quote do: unquote(invariant_module).check(unquote(Macro.var(:state, nil)))
        {name, expr, inv_info.defined_in}
      end
      |> then(&{:ok, &1})
    end
  end

  ##################################################################################################
  # Helpers
  ##################################################################################################

  @doc """

  This function returns an AST that contains the definition of the function that checks the
  invariants defined in a model.

  ## Examples

      iex> invariants = [:invariant1, :invariant2]
      iex> check(invariants, Example)

  """
  @spec check([atom()], module()) :: Macro.t()
  def check(invariants, context) do
    failed = Macro.var(:failed, nil)

    checks =
      for name <- invariants, module = invariant_module_name(name, context) do
        quote generated: true do
          unquote(failed) =
            unquote(failed) ++
              if (inv = unquote(module).check(state)) || is_nil(inv), do: [], else: [unquote(name)]
        end
      end

    quote do
      @spec check(state()) :: {:ok, state} | {:error, String.t()}
      def check(state) do
        unquote(failed) = []
        unquote_splicing(checks)

        if unquote(failed) == [] do
          {:ok, state}
        else
          unquote(failed) = Makina.Helpers.atoms_to_string(unquote(failed))
          {:error, "failed to check the invariants: " <> unquote(failed)}
        end
      end
    end
  end

  ##################################################################################################
  # Information functions
  ##################################################################################################

  @type invariants_info() :: %{invariants: [atom()]}

  @doc """

  This function extracts the information about invariants from a model.

  ## Examples

      iex> defmodule Example do
      ...>   use Makina.Invariants
      ...>   invariants invariant: true
      ...> end
      iex> invariants_info(Example)
      {:invariants_info, %{invariants: [:invariant], module: MakinaDoctestTest.Example.Invariants}}

  """
  @spec invariants_info(module()) :: {:invariants_info, invariants_info} | {:error, String.t()}
  def invariants_info(module) do
    with {:module, ^module} <- ensure_compiled(module),
         invariants_module = invariants_module_name(module),
         {:invariants_module, {:module, ^invariants_module}} <-
           {:invariants_module, ensure_compiled(invariants_module)},
         true <- {:__makina_info__, 0} in invariants_module.__info__(:functions) do
      {:invariants_info, invariants_module.__makina_info__()}
    else
      {:invariants_module, _} ->
        {:error, "module `#{inspect(module)}` does not contain invariants"}

      {:error, message} when is_bitstring(message) ->
        {:error, message}

      false ->
        {:error, "could not load information about the invariants module of model `#{module}`"}

      unknown ->
        {:error, "unknown error: `#{inspect(unknown)}`"}
    end
  end

  @type invariant_info() :: %{name: atom(), module: module(), defined_in: module()}

  @doc """

  This function extracts the information about an invariant from a model.

  ## Examples

      iex> defmodule Example do
      ...>   use Makina.Invariants
      ...>   invariants invariant: true
      ...> end
      iex> {:invariant_info, info} = invariant_info(Example, :invariant)
      iex> Map.keys(info)
      [:module, :name, :defined_in]

      iex> {:error, "module `Enum` does not contain invariants"} = invariant_info(Enum, :attribute)
      iex> {:error, "could not load `A`"} = invariant_info(A, :attribute)

  """
  @spec invariant_info(module(), atom()) ::
          {:invariant_info, invariant_info()} | {:error, String.t()}
  def invariant_info(module, invariant) do
    with {:invariants_info, invariants} <- invariants_info(module),
         {:contained?, true} <- {:contained?, invariant in invariants.invariants},
         invariant_module = invariant_module_name(invariant, module),
         {:module, ^invariant_module} <- ensure_compiled(invariant_module),
         true <- {:__makina_info__, 0} in invariant_module.__info__(:functions) do
      {:invariant_info, invariant_module.__makina_info__()}
    else
      {:contained?, false} ->
        {:error,
         "model `#{inspect(module)}` does not contain the invariant `#{inspect(invariant)}`"}

      {:error, message} when is_bitstring(message) ->
        {:error, message}

      unknown ->
        {:error, "unknown error: `#{inspect(unknown)}`"}
    end
  end

  @doc """

  This function extracts the information about all the invariants in a model.

  ## Examples

      iex> defmodule Example do
      ...>   use Makina.Invariants
      ...>   invariants invariant: true, invariant2: true
      ...> end
      iex> {:invariant_info, info} = invariant_info(Example)
      iex> 2 = length(info)

      iex> {:error, "module `Enum` does not contain invariants"} = invariant_info(Enum)
      iex> {:error, "could not load `A`"} = invariant_info(A)

  """
  @spec invariant_info(module()) :: {:invariant_info, [invariant_info()]} | {:error, String.t()}
  def invariant_info(module) do
    case invariants_info(module) do
      {:invariants_info, %{invariants: invariants}} ->
        Enum.map(invariants, &invariant_info(module, &1))
        |> Enum.split_with(&match?({:error, _}, &1))
        |> then(fn
          {[], invariants} -> {:invariant_info, invariants |> Keyword.values()}
          {errors, _} -> concat_error_messages(errors)
        end)

      error ->
        error
    end
  end
end
