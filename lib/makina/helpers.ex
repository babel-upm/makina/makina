defmodule Makina.Helpers do
  alias Makina.Error

  @moduledoc false

  @doc """

  This function returns the callbacks supperted by Makina models.

  ## Examples

      iex> callbacks()

  """
  def callbacks() do
    [
      pre: 1,
      args: 2,
      valid_args: 2,
      valid: 2,
      call: 1,
      next: 4,
      post: 4,
      weight: 1,
      features: 1
    ]
  end

  @doc """

  This function transforms a list of atoms into a readable string.

  ## Examples

      iex> atoms_to_string([])
      ""

      iex> atoms_to_string([:a, :b])
      "`:a` and `:b`"

      iex> atoms_to_string([:a, :b, :c])
      "`:a`, `:b` and `:c`"

  """
  @spec atoms_to_string([atom()]) :: String.t()
  def atoms_to_string([]), do: ""
  def atoms_to_string([attribute]), do: "`#{inspect(attribute)}`"

  def atoms_to_string([attribute1, attribute2]),
    do: "`#{inspect(attribute1)}` and `#{inspect(attribute2)}`"

  def atoms_to_string([attribute | attributes]),
    do: "`#{inspect(attribute)}`, " <> atoms_to_string(attributes)

  @doc """

  This function returns the commands module name.

  ## Examples

      iex> Example.Commands = commands_module_name(Example)
      Example.Commands

  """
  @spec commands_module_name(module()) :: atom()
  def commands_module_name(model) do
    :"#{model}.Commands"
  end

  @doc """

  This function returns a command module name given a command name and a model name.

  ## Examples

      iex> command_module_name(:f, Example)
      Example.Command.F

  """
  @spec command_module_name(any(), module()) :: atom()
  def command_module_name(command, model) do
    name = command |> to_string() |> Macro.camelize()
    :"#{model}.Command.#{name}"
  end

  @doc """

  This function returns a state module name.

  ## Examples

      iex> state_module_name(Example)
      Example.State

  """
  @spec state_module_name(module()) :: atom()
  def state_module_name(model), do: :"#{model}.State"

  @doc """

  This function returns an attribute module name.

  ## Examples

      iex> attribute_module_name(:v, Example)
      Example.State.Attribute.V

  """
  @spec attribute_module_name(any(), module()) :: atom()
  def attribute_module_name(attribute, model) do
    name = attribute |> to_string() |> Macro.camelize()
    state_module = state_module_name(model)
    :"#{state_module}.Attribute.#{name}"
  end

  @doc """

  ## Examples

      iex> invariants_module_name(Example)
      Example.Invariants

  """
  @spec invariants_module_name(module()) :: atom()
  def invariants_module_name(model), do: :"#{model}.Invariants"

  @doc """

  This function returns the invariants module name.

  ## Examples

      iex> invariant_module_name(:invariant_1, Example)
      Example.Invariant.Invariant1

  """
  @spec invariant_module_name(any(), module()) :: atom()
  def invariant_module_name(invariant, model) do
    name = invariant |> to_string() |> Macro.camelize()
    :"#{model}.Invariant.#{name}"
  end

  @doc """

  This function transforms a module that contains a command into its original name.

  ## Examples

      iex> module_to_command_name(Example.Command.F)
      :f

  """
  @spec module_to_command_name(module()) :: atom()
  def module_to_command_name(module) do
    module
    |> Atom.to_string()
    |> String.split(".")
    |> List.last()
    |> Macro.underscore()
    |> String.to_atom()
  end

  @doc """

  This function retrieves the checker configuration from the application environment.

  ## Examples

      iex> get_checker()
      Checker

  """
  @spec get_checker() :: module()
  def get_checker() do
    if checker = Application.get_env(:makina, :checker) do
      checker
    else
      Error.throw_warn("checker not found, please add it to `config.exs`")
      {:error, :checker_not_found}
    end
  end

  @doc """

  This function thecks wether two types are equivalent.

  WARNING: not sure what `Module.Types.Unify.subtype?/3` does

  ## Examples

      iex> import Makina.Helpers
      iex> t1 = quote do: integer()
      iex> t2 = quote do: boolean()
      iex> equivalent_type?(t1, t2, __ENV__.module)
      false
      iex> equivalent_type?(t1, t1, __ENV__.module)
      true

      iex> import Makina.Helpers
      iex> t1 = quote do: integer()
      iex> t2 = quote do: boolean()
      iex> equivalent_type?(t1, t2)
      false
      iex> equivalent_type?(t1, t1)
      true

  """
  @spec equivalent_type?(Macro.t(), Macro.t()) :: boolean()
  def equivalent_type?(t1, t2) do
    t1 = t1 |> Macro.to_string()
    t2 = t2 |> Macro.to_string()
    t1 == t2
  end

  @spec equivalent_type?(Macro.t(), Macro.t(), module()) :: boolean()
  def equivalent_type?(t1, t2, _context) do
    t1 = t1 |> Macro.prewalk(&clean_meta/1)
    t2 = t2 |> Macro.prewalk(&clean_meta/1)
    # Module.Types.Unify.subtype?(t1, t2, context) # TODO: this function has been removed
    t1 == t2
  end

  defp clean_meta({v1, _, v2}), do: {v1, [], v2}
  defp clean_meta(ast), do: ast

  @doc """

  This function generates an expression that contains private types definition for all the types
  exported in the given module.

  WARNING: a call to this function will succeed only if the argument module is not compiled yet.

  """
  @spec import_types(module()) :: [Macro.t()]
  def import_types(module) do
    Module.get_attribute(module, :type)
    |> Enum.map(fn {_, type_def, _} ->
      quote do
        @type unquote(type_def)
      end
    end)
  end

  @doc """

  This function generates an expression that contains private functions that reference to all the
  functions exported by the given module.

  """
  @spec import_definitions(module()) :: Macro.t()
  def import_definitions(module) do
    for {fun, arity} <- Module.definitions_in(module) do
      args = Macro.generate_arguments(arity, nil)

      quote do
        defp unquote(fun)(unquote_splicing(args)) do
          unquote(module).unquote(fun)(unquote_splicing(args))
        end
      end
    end
  end

  @doc """

  This function ensures that a moule is compiled and returns an error. This function locks the
  compiler until the module is compiled.

  ## Examples

      iex> ensure_compiled(Makina.Helpers)
      {:module, Makina.Helpers}
      iex> ensure_compiled(A)
      {:error, "could not load `A`"}

  """
  @spec ensure_compiled(module()) :: {:module, module} | {:error, String.t()}
  def ensure_compiled(module) when is_atom(module) do
    try do
      {:module, Makina.Module.ensure_compiled!(module)}
    catch
      _error, _exception -> {:error, "could not load `#{inspect(module)}`"}
    end
  end

  def ensure_compiled(module), do: {:error, "`#{inspect(module)}` is not a valid module name"}

  @doc """

  This function validates the `:extends` option in makina models. It only checks that both arguments
  are valid Elixir modules and that they are not the same module. It does not check whether the
  `:extends` module contains a valid model.

  ## Examples

      iex> validate_extends(1, Example)
      {:error, "`1` is not a valid module name"}
      iex> validate_extends(Example, Example)
      {:error, "`Example` cannot extend itself"}
      iex> validate_extends(Example, Extended)
      {:ok, Example}

  """
  @spec validate_extends(module(), module()) :: {:ok, module()} | {:error, String.t()}
  def validate_extends(extends, module) do
    cond do
      not is_atom(extends) -> {:error, "`#{inspect(extends)}` is not a valid module name"}
      not is_atom(module) -> {:error, "`#{inspect(module)}` is not a valid module name"}
      extends == module -> {:error, "`#{inspect(extends)}` cannot extend itself"}
      true -> {:ok, extends}
    end
  end

  @doc """

  This function merges a list of error messages into a single error message.

  ## Examples

      iex> error1 = {:error, "error message 1"}
      iex> error2 = {:error, "error message 2"}
      iex> concat_error_messages([error1, error2])
      {:error, "error message 1\\nerror message 2"}

  """
  @spec concat_error_messages([{:error, String.t()}]) :: {:error, String.t()}
  def concat_error_messages(errors) do
    {:error, Enum.map_join(errors, "\n", &elem(&1, 1))}
  end

  @doc """

  This function registers all the given options in some module.

  """
  @spec register_options(Macro.Env.t(), atom(), [atom()], [{atom(), any()}]) :: [:ok]
  def register_options(env, attribute, allowed_attributes, options) do
    for {option, value} <- options,
        value = Macro.prewalk(value, &Macro.expand(&1, env)),
        Module.put_attribute(env.module, attribute, {option, value}),
        option not in allowed_attributes do
      Error.throw_error("Option `#{option}` not supported.", env)
    end
  end

  @doc """

  This function can be used to inspect some AST.

  """
  @spec macro_inspect(Macro.t(), Keyword.t()) :: Macro.t()
  def macro_inspect(macro, _options \\ []) do
    Macro.to_string(macro) |> IO.puts()
    macro
  end

  @doc """

  This function returns the optional type.

  """
  @spec optional_type() :: Macro.t()
  def optional_type(), do: quote(do: [{optional(any), any}])

  @doc """

  This function returns the unknown type.

  """
  @spec unknown_type() :: Macro.t()
  def unknown_type(), do: quote(do: Makina.Types.unknown())

  @doc """

  This function returns the any type.

  """

  @spec any_type() :: Macro.t()
  def any_type(), do: quote(do: any())

  @doc """

  This function returns an union type.

  ## Examples

      iex> type1 = quote do: integer()
      iex> type2 = quote do: boolean()
      iex> result = union_type([type1, type2])
      iex> expected = quote do: [boolean() | integer()]
      iex> assert Macro.to_string(expected) =~ Macro.to_string(result)

  """
  @spec union_type([Macro.t()]) :: [Macro.t()]
  def union_type([]), do: []

  def union_type(types) do
    for type <- types, reduce: [] do
      [] -> type
      acc -> quote do: unquote(type) | unquote(acc)
    end
    |> then(&[&1])
  end

  @doc """

  This function inserts parameters in a `super()` expression.

  ## Examples

      iex> block = quote do: super()
      iex> insert_super_args(block, [:arguments, :result], :next, __ENV__)

  """
  @spec insert_super_args(Macro.t(), [atom()], atom(), Macro.Env.t()) :: Macro.t()
  def insert_super_args(block, args, callback, env) do
    args = Enum.map(args, &Macro.var(&1, nil))

    insert_args_in_super = fn
      {:super, meta, []} ->
        {:super, meta, args}

      ast = {:super, _meta, super_args} ->
        if length(super_args) != length(args) do
          "super in `#{callback}` has #{length(super_args)} but expects #{length(args)}"
          |> Error.throw_error(env)
        end

        ast

      ast ->
        ast
    end

    Macro.postwalk(block, insert_args_in_super)
  end

  @doc """

  This function generates a pattern matching on a map.

  ## Examples

      iex> code = quote do: map = %{x: x, y: y}
      iex> result = map_pattern(:map, [:x, :y])
      iex> assert Macro.to_string(code) =~ Macro.to_string(result)

  """
  @spec map_pattern(atom(), [atom()]) :: Macro.t()
  def map_pattern(map_name, map_values) do
    map_name = Macro.var(map_name, nil)
    map_values = Enum.map(map_values, fn value -> {value, Macro.var(value, nil)} end)
    quote do: unquote(map_name) = %{unquote_splicing(map_values)}
  end

  @doc """

  This function introduces some args in a function definition.

  ## Examples

      iex> original = quote do def f(), do: :ok end
      iex> expected = quote do def f(_x), do: :ok end
      iex> x = Macro.var(:x, nil)
      iex> {result, _}  = insert_args(original, [], [x])
      iex> assert Macro.to_string(result) =~ Macro.to_string(expected)

  """
  @spec insert_args(Macro.t(), [atom()], Macro.t()) :: {Macro.t(), [atom()]}
  def insert_args({:def, info0, [{name, info1, old_args}, block]}, acc, args) do
    old_args = if is_nil(old_args), do: [], else: old_args
    args = Macro.prewalk(args, &ignore_unused(&1, acc))
    expr = {:def, info0, [{name, info1, args ++ old_args}, block]}
    {expr, []}
  end

  def insert_args({:defdelegate, info0, [{name, info1, old_args}, body]}, _acc, args) do
    old_args = if is_nil(old_args), do: [], else: old_args
    args = Macro.generate_arguments(length(args), nil)
    expr = {:defdelegate, info0, [{name, info1, args ++ old_args}, body]}
    {expr, []}
  end

  def insert_args(expr = {value, _, nil}, acc, _), do: {expr, [value | acc]}
  def insert_args(expr = {value, _, inf}, acc, _) when is_atom(inf), do: {expr, [value | acc]}
  def insert_args(expr, acc, _args), do: {expr, acc}

  @doc """

  This function can be used to ignore unused arguments in a function definition.

  ## Examples

      iex> original = quote do def f(x), do: :ok end
      iex> expected = quote do def f(_x), do: :ok end
      iex> result = Macro.prewalk(original, &ignore_unused(&1, []))
      iex> assert Macro.to_string(result) =~ Macro.to_string(expected)

  """
  @spec ignore_unused(Macro.t(), [atom()]) :: Macro.t()
  def ignore_unused(expr = {value, info, nil}, used) do
    if value in used, do: expr, else: {:"_#{value}", info, nil}
  end

  def ignore_unused({value, info, context}, used) when is_atom(context) do
    if value in used, do: {value, info, nil}, else: {:"_#{value}", info, context}
  end

  def ignore_unused(expr, _used), do: expr

  @doc """

  This function converts a module name to a String.

      iex> inspect_module_name(A)
      "A"
      iex> inspect_module_name(:"A")
      "A"

  """
  @spec inspect_module_name(module()) :: String.t()
  def inspect_module_name(module) do
    Atom.to_string(module) |> String.trim("Elixir.")
  end

  @doc """

  This function removes top-level `symbolic` annotations from a type definition. It must be used
  with function that traverses the AST, like `Macro.postwalk` to remove all the `symbolic`
  annotations from the given type.

  ## Examples

     iex> type = quote do: [symbolic(integer())]
     iex> Macro.postwalk(type, &dynamic_type/1)
     quote do: [integer()]

  """
  @spec dynamic_type(Macro.t()) :: Macro.t()
  def dynamic_type({:symbolic, _, [type]}), do: type
  def dynamic_type(ast), do: ast

  @doc """

  This function replaces top-level `symbolic` annotations by the `symbolic_expr` type. It must be
  used with a function that traverses the AST, like `Macro.prewalk` to remove all the `symbolic`
  annotations from the given type.

  ## Examples

      iex> type = quote do: [symbolic(integer())]
      iex> Macro.postwalk(type, &symbolic_type/1)
      quote do: [symbolic_expr()]

  """
  @spec symbolic_type(Macro.t()) :: Macro.t()
  def symbolic_type({:symbolic, _, [_type]}), do: quote(do: symbolic_expr())
  def symbolic_type(ast), do: ast
end
