defmodule Makina.Types do
  @moduledoc "Provides definitions for types common to all Makina models."

  @typedoc "Represents a symbolic var."
  @type symbolic_var() :: {:var, pos_integer()}

  @typedoc "Represents a symbolic call."
  @type symbolic_call() ::
          {:call, atom(), atom(), list()}
          | {:call, atom(), atom(), list(), any()}

  @typedoc "Represents a symbolic expression."
  @type symbolic_expr() :: symbolic_var() | symbolic_call()

  @typedoc "Represents a PBT generator."
  @type generator(a) :: a | any()

  @typedoc "Represents an unknown type."
  @type unknown() :: any()
end
