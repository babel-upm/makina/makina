defmodule Makina.State.Attribute do
  import Makina.Helpers

  @moduledoc false

  ##################################################################################################
  # Macros
  ##################################################################################################

  @doc """

  This macro imports the macros defined in this module.

  ## Examples

      iex> defmodule Example do
      ...>   use Makina.State.Attribute
      ...> end

  """
  @spec __using__(Keyword.t()) :: Macro.t()
  defmacro __using__(_options) do
    Module.register_attribute(__CALLER__.module, :attribute, accumulate: false)

    quote do
      import unquote(__MODULE__), only: :macros
    end
  end

  @doc """

  This macro populates the module with attribute information.

  ## Examples

      iex> defmodule Example do
      ...>   use Makina.State.Attribute
      ...>   attribute :attr1, 0, integer()
      ...> end

  """

  @doc_template ~S"""

  Defines the initial value of the attribute `<%= name %>`.

  """
  @spec attribute(atom(), Macro.t(), Macro.t()) :: Macro.t()
  defmacro attribute(name, init, type) do
    docs = EEx.eval_string(@doc_template, name: name)
    symbolic = Macro.postwalk(type, &symbolic_type/1)
    dynamic = Macro.postwalk(type, &dynamic_type/1)

    if Module.get_attribute(__CALLER__.module, :attribute) do
      quote do
        defoverridable init: 0
        def init(), do: unquote(init)
      end
    else
      Module.put_attribute(__CALLER__.module, :attribute, true)

      quote do
        @type symbolic_attribute() :: unquote(symbolic)
        @type dynamic_attribute() :: unquote(dynamic)
        @type symbolic_expr() :: Makina.Types.symbolic_expr()

        @doc unquote(docs)
        @spec init() :: symbolic_attribute()
        def init(), do: unquote(init)

        @spec __makina_info__() :: Makina.State.attribute_info()
        def __makina_info__() do
          %{
            name: unquote(name),
            type: unquote(Macro.escape(type)),
            symbolic: unquote(Macro.escape(symbolic)),
            dynamic: unquote(Macro.escape(dynamic)),
            module: unquote(__CALLER__.module)
          }
        end
      end
    end
  end
end
