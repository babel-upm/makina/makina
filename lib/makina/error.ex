defmodule Makina.Error do
  @moduledoc false

  defexception message: "makina error"

  @doc """

  This function provides throws errors in the current environment.

  ## Examples

      iex> throw_error("error")
      ** (Makina.Error) error

  """
  @spec throw_error(String.t()) :: :none
  def throw_error(message) do
    error = %__MODULE__{message: message}
    raise error
  end

  @doc """

  This function throws errors in some environment.

  ## Examples

      iex> throw_error("error", __ENV__)
      ** (Makina.Error) error

  """
  @spec throw_error(String.t(), Macro.Env.t()) :: :none
  def throw_error(message, env) do
    error = %__MODULE__{message: message}
    stacktrace = Macro.Env.stacktrace(env)
    reraise error, stacktrace
  end

  @doc """

  This function throws warnings in the current environment.

  ## Examples

      iex> capture_io(:stderr, fn -> throw_warn("warning") end)

  """
  @spec throw_warn(String.t()) :: :ok
  def throw_warn(message) do
    IO.warn(message, [])
  end

  @doc """

  This function throws warnings in some environment.

  ## Examples

       iex> capture_io(:stderr, fn -> throw_warn("warning", __ENV__) end)

  """
  @spec throw_warn(String.t(), Macro.Env.t()) :: :ok
  def throw_warn(message, env) do
    stacktrace = Macro.Env.stacktrace(env)
    IO.warn(message, stacktrace)
  end
end
