defmodule Makina.Exports do
  alias Makina.Error

  import Makina.Helpers

  @moduledoc false

  @typedoc """

  This type contains the options to configure a model:
  - `:extends` contains the extended model or a list of models to compose.
  - `:where` contains a keyword list with the commands to rename.
  - `:hiding` includes a list of commands to conceal in the model.
  - `:implemented_by` includes the module that contains the implementation being tested.
  - `abstract` can be used to indicate that the model in this module contains an abstract model
    (which means it is not executable).

  """
  @type option() ::
          {:extends, module()}
          | {:extends, [module()]}
          | {:implemented_by, module()}
          | {:hiding, [atom()]}
          | {:where, [{atom(), atom()}]}
          | {:abstract, boolean()}

  ##################################################################################################
  # Macros
  ##################################################################################################

  @doc """

  This macro configures a module that will contain a Makina model.

  ## Examples

      iex> defmodule Example do
      ...>   use Makina.Exports
      ...> end

  """
  @spec __using__(Keyword.t()) :: Macro.t()
  defmacro __using__(options) do
    options =
      options
      |> Enum.map(fn opt -> Macro.prewalk(opt, &Macro.expand(&1, __CALLER__)) end)
      |> validate_options(__CALLER__)
      |> process_options(__CALLER__)

    state_options = Enum.filter(options, fn {k, _v} -> k in [:extends] end)
    invariant_options = Enum.filter(options, fn {k, _v} -> k in [:extends] end)

    command_options =
      Enum.filter(options, fn {k, _v} -> k in [:extends, :implemented_by, :abstract] end) ++
        [defaults: true, abstract: false]

    debug = Keyword.get(options, :debug, Application.get_env(:makina, :debug))

    quote do
      use Makina.State, unquote([{:debug, debug} | state_options])
      use Makina.Invariants, unquote([{:debug, debug} | invariant_options])
      use Makina.Command, unquote([{:debug, debug} | command_options])
      use Makina.Behaviour
      import unquote(__MODULE__), except: [symbolic: 1]
      unquote(debug)
      @before_compile unquote(__MODULE__)
    end
  end

  @doc """

  This macro introduces auto generated documentation in the module that contains the model..

  """

  @doc_template ~S"""

  Contains a Makina model called `<%= inspect(name) %>`.

  <%= if userdocs, do: elem(userdocs, 1) %>
  <%= if extends do %>
  ## TODO
  <% end %>
  <%= if composed do %>
  ## TODO
  <% end %>
  <%= if imports do %>
  ## TODO
  <% end %>
  ## Commands
  <%= for c <- cmds, do: "- `#{c}` stored at `#{Makina.Helpers.command_module_name(c, name) |> inspect()}`\n" %>
  Detailed information about each command can be accessed inside the interpreter:

      iex> h <%= inspect(name) %>.Command.NAME

  ## State attributes
  <%= for attr <- attrs, do: "- `#{attr}`\n" %>
  Detailed information about the state can be accessed inside the interpreter:

      iex> h <%= inspect(name) %>.Command.State

  ## Invariants
  <%= for inv <- invs, do: "- `#{inv}`\n" %>
  Detailed information about the invariants can be accessed inside the interpreter:

      iex> h <%= inspect(name) %>.Invariants

  """
  @spec __before_compile__(Macro.Env.t()) :: Macro.t()
  defmacro __before_compile__(_env) do
    module = __CALLER__.module
    userdocs = Module.get_attribute(module, :moduledoc)
    # options = Module.get_attribute(module, :options)

    cmds = Module.get_attribute(module, :commands)
    attrs = Module.get_attribute(module, :attributes)
    invs = Module.get_attribute(module, :invariants)

    docs =
      EEx.eval_string(@doc_template,
        name: module,
        userdocs: userdocs,
        cmds: cmds,
        attrs: attrs,
        invs: invs,
        # TODO
        extends: nil,
        composed: nil,
        imports: nil
      )

    quote do
      @moduledoc unquote(docs)
    end
  end

  @doc """

  This macro rewrites the given expression into a symbolic call.

  This macro is only intended to be used in `next`, notice that `result` is a reserved word in next
  and therefore should not be overriden or inspected!

  ## Examples

      iex> f = fn result -> symbolic(Kernel.elem({0, 1}, 1)) end
      iex> f.({:var, 1})
      {:call, Kernel, :elem, [{0, 1}, 1]}
      iex> f.(1)
      1

      iex> code = quote do: symbolic(elem({0, 1}))
      iex> Code.eval_quoted(code)
      ** (Makina.Error) error cannot convert `elem({0, 1})` into a symbolic expression.

  """
  @spec symbolic(Macro.t()) :: Macro.t()
  defmacro symbolic(expr = {{:., _, [_, _]}, _, _}), do: to_symbolic_call(expr)

  defmacro symbolic(expr) do
    "error cannot convert `#{Macro.to_string(expr)}` into a symbolic expression."
    |> Error.throw_error(__CALLER__)
  end

  @spec to_symbolic_call(Macro.t()) :: Macro.t()
  defp to_symbolic_call({{:., _, [module, function]}, _, args}) do
    args =
      Enum.map(args, fn
        arg = {{:., _, [_, _]}, _, _} -> to_symbolic_call(arg)
        arg -> arg
      end)

    # TODO: add static check in next that prevents renaming "result"
    quote generated: true do
      if match?({:var, _}, unquote(Macro.var(:result, nil))) do
        {:call, unquote(module), unquote(function), unquote(args)}
      else
        unquote(module).unquote(function)(unquote_splicing(args))
      end
    end
  end

  ##################################################################################################
  # Helpers
  ##################################################################################################

  @doc """

  TODO

  Errors can occur:
  - TODO: check forbidden parallel composition patterns.

  """
  @spec validate_options([option()], Macro.Env.t()) :: [option()]
  def validate_options(options, env) do
    !Keyword.get(options, :extends) && Keyword.get(options, :hiding) &&
      Error.throw_error("`:extends` is missing, but is required for `:hiding`", env)

    !Keyword.get(options, :extends) && Keyword.get(options, :where) &&
      Error.throw_error("`:extends` is missing, but is required for `:where`", env)

    length(Keyword.get_values(options, :extends)) > 1 &&
      Error.throw_error("only one `:extends` per model is permitted", env)

    options
  end

  @doc """

  """
  @spec process_options([option()], Macro.Env.t()) :: [option()]
  def process_options([], _env), do: []

  def process_options(options, env) do
    {extends, options} = Keyword.pop(options, :extends)

    cond do
      !extends ->
        options

      is_list(extends) ->
        extends = parallel_composition(extends, env)
        process_options([{:extends, extends} | options], env)

      length(options) > 0 ->
        [{option, value} | options] = options

        cond do
          option == :hiding ->
            extends = hiding(extends, value, env)
            process_options([{:extends, extends} | options], env)

          option == :where ->
            extends = aliasing(extends, value, env)
            process_options([{:extends, extends} | options], env)

          true ->
            [{option, value} | process_options([{:extends, extends} | options], env)]
        end

      true ->
        [{:extends, extends} | options]
    end
  end

  @doc """

  This function generates a composed model.

  ## Examples

      iex> defmodule A do
      ...>   use Makina
      ...>   state attr_1: 0
      ...>   command f() do
      ...>     call :ok
      ...>   end
      ...> end
      iex> defmodule B do
      ...>   use Makina
      ...>   state attr_2: 0
      ...>   command g() do
      ...>     call :ok
      ...>   end
      ...> end
      iex> parallel_composition([A, B], __ENV__)
      :"MakinaDoctestTest.A | MakinaDoctestTest.B"

      iex> defmodule A do
      ...>   use Makina
      ...>   state attr_1: 0
      ...>   command f() do
      ...>     call :ok
      ...>   end
      ...> end
      iex> defmodule B do
      ...>   use Makina
      ...>   state attr_2: 0
      ...>   command g() do
      ...>     call :ok
      ...>   end
      ...> end
      iex> parallel_composition([A, {B, hiding: :g}], __ENV__)
      :"A | B hiding g"
      :"MakinaDoctestTest.A | MakinaDoctestTest.B hiding [g]"

      iex> defmodule A do
      ...>   use Makina
      ...>   state attr_1: 0
      ...>   command f() do
      ...>     call :ok
      ...>   end
      ...> end
      iex> defmodule B do
      ...>   use Makina
      ...>   state attr_2: 0
      ...>   command g() do
      ...>     call :ok
      ...>   end
      ...> end
      iex> parallel_composition([A, {B, extends: A, hiding: :g}], __ENV__)
      ** (Makina.Error) `:extends` is not allowed inside parallel composition

  """
  @spec parallel_composition([module()], Macro.Env.t()) :: module()
  def parallel_composition(models, env) do
    models =
      models
      |> Enum.map(fn
        module when is_atom(module) ->
          module

        {module, options} ->
          Keyword.get(options, :extends) &&
            Error.throw_error("`:extends` is not allowed inside parallel composition")

          options = process_options([{:extends, module} | options], env)
          Keyword.get(options, :extends)
      end)

    composed = models |> Enum.map_join(" | ", &inspect_module_name/1) |> then(&:"#{&1}")

    contents = quote do: use(Makina.Composition, extends: unquote(models))
    ^composed = Makina.Module.create(composed, contents, env)
    composed
  end

  @doc """

  This function creates an intermediate model by utilizing a hiding operator.

  ## Examples

      iex> defmodule A do
      ...>   use Makina
      ...>   state attr_1: 0
      ...>   command f() do
      ...>     call :ok
      ...>   end
      ...> end
      iex> hiding(A, [:f], __ENV__)
      :"MakinaDoctestTest.A hiding [f]"

      iex> defmodule A do
      ...>   use Makina
      ...>   state attr_1: 0
      ...>   command f() do
      ...>     call :ok
      ...>   end
      ...>   command g() do
      ...>     call :ok
      ...>   end
      ...> end
      iex> hiding(A, [:f, :g], __ENV__)
      :"MakinaDoctestTest.A hiding [f, g]"


  """
  @spec hiding(module(), atom() | [atom()], Macro.Env.t()) :: module()
  def hiding(module, command, env) when is_atom(command) do
    hiding(module, [command], env)
  end

  def hiding(module, commands, env) when is_list(commands) do
    hidden =
      Enum.map_join(commands, ", ", &Atom.to_string/1)
      |> then(&:"#{inspect(module)} hiding [#{&1}]")

    contents = quote do: use(Makina.Import, model: unquote(module), hiding: unquote(commands))
    ^hidden = Makina.Module.create(hidden, contents, env)
    hidden
  end

  def hiding(_module, _commands, env) do
    Error.throw_error("`:hiding` receives a list of command names", env)
  end

  @doc """

  This function creates an intermediate model with the renaming operator applied.

  ## Examples

      iex> defmodule A do
      ...>   use Makina
      ...>   state attr_1: 0
      ...>   command f() do
      ...>     call :ok
      ...>   end
      ...>   command g() do
      ...>     call :ok
      ...>   end
      ...> end
      iex> aliasing(A, [f: :h, g: :i], __ENV__)
      :"MakinaDoctestTest.A where [f as h, g as i]"

  """
  @spec aliasing(module, [{atom(), atom()}], Macro.Env.t()) :: module()
  def aliasing(module, aliases, env) do
    aliased =
      Enum.map_join(aliases, ", ", fn {x, y} -> "#{Atom.to_string(x)} as #{Atom.to_string(y)}" end)
      |> then(&:"#{inspect(module)} where [#{&1}]")

    contents = quote do: use(Makina.Import, model: unquote(module), where: unquote(aliases))
    ^aliased = Makina.Module.create(aliased, contents, env)
    aliased
  end
end
