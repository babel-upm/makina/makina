defmodule Makina.Module do
  use Agent

  @spec start() :: {:ok, pid()}
  defp start() do
    case Process.whereis(__MODULE__) do
      # the state of this agent is a list of Makina modules that are being loaded and a list of
      # tuples of pids waiting for some module
      nil -> Agent.start(fn -> %{loading: [], waiting: []} end, name: __MODULE__)
      pid -> {:ok, pid}
    end
  end

  @spec create(module(), Macro.t(), Macro.Env.t() | keyword()) :: module()
  def create(module, contents, env) do
    start()
    loaded = :code.all_loaded()

    unlock_and_update = fn state ->
      state.waiting
      |> Enum.map(fn {waiting, pid} ->
        (waiting == module || waiting not in state.loading) && send(pid, {__MODULE__, module})
      end)

      Map.update!(state, :loading, fn loading -> List.delete(loading, module) end)
    end

    load? =
      Agent.get_and_update(__MODULE__, fn state ->
        # if the module is being loaded or has already been loaded, there is nothing to load
        ((module in loaded || module in state.loading) && {false, state}) ||
          {true, Map.update!(state, :loading, fn loading -> [module | loading] end)}
      end)

    load? &&
      try do
        clean_import = fn
          {op, meta, args} -> {op, Keyword.delete(meta, :imports), args}
          x -> x
        end

        contents |> Macro.prewalk(clean_import) |> then(&Module.create(module, &1, env))
      rescue
        exception ->
          Agent.update(__MODULE__, unlock_and_update)
          reraise exception, __STACKTRACE__
      end && Agent.update(__MODULE__, unlock_and_update)

    module
  end

  @spec ensure_compiled!(module()) :: module()
  def ensure_compiled!(module) do
    start()

    try do
      Code.ensure_compiled!(module)
    rescue
      exception ->
        if module in :code.all_loaded() do
          module
        else
          (Agent.get_and_update(__MODULE__, fn state ->
             if module in state.loading do
               {true, Map.update!(state, :waiting, fn waiting -> [{module, self()} | waiting] end)}
             else
               {false, state}
             end
           end) &&
             receive do
               {__MODULE__, ^module} -> module
             after
               5000 -> module
             end) || reraise exception, __STACKTRACE__
        end
    end
  end
end
