defmodule Makina.Import do
  import Makina.Helpers
  import Makina.State, only: [attribute_info: 1]
  import Makina.Invariants, only: [invariant_info: 1]
  import Makina.Command, only: [commands_info: 1, command_info: 1]

  alias Makina.Error

  @moduledoc false

  @typedoc """

  This type represents the command options processed by this module:
  - `:model` contains the model with the commands to export.
  - `:where` contains a keyword list with the commands to rename.
  - `:hiding` contains a list of commands to hide in the model.

  """
  @type option() :: {:model, module()} | {:where, Keyword.t(atom())} | {:hiding, [atom()] | atom()}

  ##################################################################################################
  # Macros
  ##################################################################################################

  @doc """

  This macro generates a Makina model that imports the state, invariants and commands. Only one
  hiding or aliasing operation can be applied in each call.

  ## Examples

      iex> defmodule Example do
      ...>   use Makina
      ...>   state attr1: 0 :: integer(), attr2: true
      ...>   invariants invariant: true
      ...>   command f() :: :ok do
      ...>     call :ok
      ...>   end
      ...>   command g() :: :ok do
      ...>     call :ok
      ...>   end
      ...> end
      iex> defmodule ImportExample do
      ...>   use Makina.Import, model: Example, hiding: [:g]
      ...> end
      iex> {:commands_info, %{commands: [:f]}} = Makina.Command.commands_info(ImportExample)

      iex> defmodule Example do
      ...>   use Makina
      ...>   state attr1: 0 :: integer(), attr2: true
      ...>   invariants invariant: true
      ...>   command f() :: :ok do
      ...>     call :ok
      ...>   end
      ...>   command g() :: :ok do
      ...>     call :ok
      ...>   end
      ...> end
      iex> defmodule ImportExample do
      ...>   use Makina.Import, model: Example, where: [f: :h]
      ...> end
      iex> {:commands_info, %{commands: cmds}} = Makina.Command.commands_info(ImportExample)
      iex> :f in cmds
      true
      iex> :g in cmds
      true
      iex> :h in cmds
      true

      iex> defmodule Example do
      ...>   use Makina
      ...>   state attr1: 0 :: integer(), attr2: true
      ...>   invariants invariant: true
      ...>   command f() :: :ok do
      ...>     call :ok
      ...>   end
      ...>   command g() :: :ok do
      ...>     call :ok
      ...>   end
      ...> end
      iex> defmodule ImportExample do
      ...>   use Makina.Import, model: Example, where: [f: :g]
      ...> end
      ** (Makina.Error) `:where` error, cannot rename `f` to `g`, `g` already exists in `MakinaDoctestTest.Example`

      iex> defmodule ImportExample do
      ...>   use Makina.Import, model: Example, where: [f: :h]
      ...> end
      ** (Makina.Error) could not load `Example`

      iex> defmodule ImportExample do
      ...>   use Makina.Import, where: [f: :h]
      ...> end
      ** (Makina.Error) missing model to import

      iex> defmodule Example do
      ...>   use Makina
      ...>   state attr1: 0 :: integer(), attr2: true
      ...>   invariants invariant: true
      ...>   command f() :: :ok do
      ...>     call :ok
      ...>   end
      ...>   command g() :: :ok do
      ...>     call :ok
      ...>   end
      ...> end
      iex> defmodule ImportExample do
      ...>   use Makina.Import, model: Example, hiding: {:f, :g}
      ...> end
      ** (Makina.Error) `:hiding` receives a list of command names

      iex> defmodule Example do
      ...>   use Makina
      ...>   state attr1: 0 :: integer(), attr2: true
      ...>   invariants invariant: true
      ...>   command f() :: :ok do
      ...>     call :ok
      ...>   end
      ...>   command g() :: :ok do
      ...>     call :ok
      ...>   end
      ...> end
      iex> defmodule ImportExample do
      ...>   use Makina.Import, model: Example, where: [a: :h]
      ...> end
      ** (Makina.Error) `:where` error, missing `a` in `MakinaDoctestTest.Example`

  """
  @spec __using__([option()]) :: Macro.t()
  defmacro __using__(options) do
    options = Enum.map(options, fn opt -> Macro.prewalk(opt, &Macro.expand(&1, __CALLER__)) end)
    hiding = Keyword.get(options, :hiding)
    rename = Keyword.get(options, :where)

    hiding && rename &&
      Error.throw_error("cannot apply `:hiding` and `renaming` at the same time", __CALLER__)

    with {:ok, module} <- Keyword.fetch(options, :model),
         :ok <- (module && :ok) || :error,
         {:ok, commands} <-
           (hiding && import_commands_hiding(module, hiding)) ||
             (rename && import_commands_renaming(module, rename)),
         {:ok, state} <- import_state(module),
         {:ok, invariants} <- import_invariants(module) do
      quote do
        use Makina
        unquote(state)
        unquote(invariants)
        unquote_splicing(commands)
      end
    else
      :error -> {:error, "missing model to import"}
      {:error, error} -> {:error, error}
    end
    |> then(fn
      {:error, message} -> Error.throw_error(message, __CALLER__)
      result -> result
    end)
  end

  ##################################################################################################
  # Helpers
  ##################################################################################################

  @doc """

  This function generates an expression that imports the attributes from a given model.

  ## Examples

      iex> defmodule Example do
      ...>   use Makina.State
      ...>   state attr1: 0 :: integer, attr2: true
      ...> end
      iex> {:ok, state} = import_state(Example)
      iex> result = state |> Macro.to_string()
      iex> code = quote do
      ...>   state attr2: MakinaDoctestTest.Example.State.Attribute.Attr2.init() :: any(),
      ...>         attr1: MakinaDoctestTest.Example.State.Attribute.Attr1.init() :: integer
      ...> end |> Macro.to_string()
      iex> assert result =~ code

      iex> import Makina.Import
      iex> {:error, _} = import_state("error")

  """
  @spec import_state(module) :: {:ok, Macro.t()} | {:error, String.t()}
  def import_state(module) do
    case attribute_info(module) do
      {:attribute_info, attributes} ->
        for %{type: type, module: module, name: name} <- attributes do
          expr = quote do: unquote(module).init() :: unquote(type)
          {name, expr}
        end
        |> then(&{:ok, quote(do: state(unquote(&1)))})

      {:error, error} ->
        {:error, error}
    end
  end

  @doc """

  This function generates an expression that imports the invariants from a makina model.

  ## Examples

      iex> defmodule Example do
      ...>   use Makina.Invariants
      ...>   invariants inv1: true
      ...> end
      iex> {:ok, invariants} = import_invariants(Example)
      iex> result = Macro.to_string(invariants)
      iex> code = quote do
      ...>   invariants inv1: MakinaDoctestTest.Example.Invariant.Inv1.check(state)
      ...> end |> Macro.to_string()
      iex> assert result =~ code

  """
  @spec import_invariants(module()) :: {:ok, Macro.t()} | {:error, String.t()}
  def import_invariants(module) do
    case invariant_info(module) do
      {:invariant_info, invariants} ->
        for %{name: name, module: module} <- invariants do
          expr = quote do: unquote(module).check(unquote(Macro.var(:state, nil)))
          {name, expr}
        end
        |> then(&{:ok, quote(do: invariants(unquote(&1)))})

      error ->
        error
    end
  end

  @doc """

  This function generates an expression that imports all the commands in a model except the ones
  appearing in the hiding argument.

  ## Examples

      iex> defmodule Example do
      ...>   use Makina.Command, defaults: true
      ...>   command f(), do: call(:ok)
      ...> end
      iex> {:ok, []} = import_commands_hiding(Example, [:f])

      iex> defmodule Example do
      ...>   use Makina.Command, defaults: true
      ...>   command f(), do: call(:ok)
      ...>   command g(arg1 :: integer(), arg2) :: :ok, do: call(:ok)
      ...> end
      iex> {:ok, commands} = import_commands_hiding(Example, [:f])
      iex> result = commands |> Macro.to_string()
      iex> code = quote do
      ...>   [
      ...>     command g(arg1 :: integer(), arg2) :: :ok do
      ...>       pre MakinaDoctestTest.Example.Command.G.pre(state)
      ...>       args MakinaDoctestTest.Example.Command.G.args(state)
      ...>       valid_args MakinaDoctestTest.Example.Command.G.valid_args(state, arguments)
      ...>       call MakinaDoctestTest.Example.Command.G.call(arguments)
      ...>       valid MakinaDoctestTest.Example.Command.G.valid(state, arguments, result)
      ...>       next MakinaDoctestTest.Example.Command.G.next(state, arguments, result, valid)
      ...>       post MakinaDoctestTest.Example.Command.G.post(state, arguments, result, valid)
      ...>       weight MakinaDoctestTest.Example.Command.G.weight(state)
      ...>     end
      ...>   ]
      ...> end |> Macro.to_string()
      iex> assert result =~ code

  """
  @spec import_commands_hiding(module(), [atom()]) :: {:ok, Macro.t()} | {:error, String.t()}
  def import_commands_hiding(module, hiding) do
    with {:commands_info, %{commands: cmds}} <- commands_info(module),
         {:ok, hiding} <- validate_hiding(module, hiding, cmds),
         imports <- MapSet.difference(MapSet.new(cmds), MapSet.new(hiding)),
         {results, []} <-
           Enum.map(imports, fn name -> import_command(module, name, name) end)
           |> Enum.split_with(fn cmd -> match?({:ok, _}, cmd) end) do
      results |> Enum.map(&elem(&1, 1)) |> then(&{:ok, &1})
    else
      {results, errors} when is_list(results) and is_list(errors) ->
        {:error, errors |> Enum.map(&elem(&1, 1)) |> Enum.join()}

      error ->
        error
    end
  end

  @doc """

  This function generates an expression that imports all the commands in a model. It also creates a
  renamed copy of every command in the "rename" argument.

  ## Examples

      iex> defmodule Example do
      ...>   use Makina.Command, defaults: true
      ...>   command f(arg1 :: integer(), arg2) :: :ok, do: call(:ok)
      ...> end
      iex> {:ok, commands} = import_commands_renaming(Example, [f: :g])
      iex> result = commands |> Macro.to_string()
      iex> code = quote do
      ...>   [
      ...>     command f(arg1 :: integer(), arg2) :: :ok do
      ...>       pre MakinaDoctestTest.Example.Command.F.pre(state)
      ...>       args MakinaDoctestTest.Example.Command.F.args(state)
      ...>       valid_args MakinaDoctestTest.Example.Command.F.valid_args(state, arguments)
      ...>       call MakinaDoctestTest.Example.Command.F.call(arguments)
      ...>       valid MakinaDoctestTest.Example.Command.F.valid(state, arguments, result)
      ...>       next MakinaDoctestTest.Example.Command.F.next(state, arguments, result, valid)
      ...>       post MakinaDoctestTest.Example.Command.F.post(state, arguments, result, valid)
      ...>       weight MakinaDoctestTest.Example.Command.F.weight(state)
      ...>     end,
      ...>     command g(arg1 :: integer(), arg2) :: :ok do
      ...>       pre MakinaDoctestTest.Example.Command.F.pre(state)
      ...>       args MakinaDoctestTest.Example.Command.F.args(state)
      ...>       valid_args MakinaDoctestTest.Example.Command.F.valid_args(state, arguments)
      ...>       call MakinaDoctestTest.Example.Command.F.call(arguments)
      ...>       valid MakinaDoctestTest.Example.Command.F.valid(state, arguments, result)
      ...>       next MakinaDoctestTest.Example.Command.F.next(state, arguments, result, valid)
      ...>       post MakinaDoctestTest.Example.Command.F.post(state, arguments, result, valid)
      ...>       weight MakinaDoctestTest.Example.Command.F.weight(state)
      ...>     end
      ...>   ]
      ...> end |> Macro.to_string()
      iex> assert result =~ code

  """
  @spec import_commands_renaming(module(), [{atom(), atom()}]) ::
          {:ok, Macro.t()} | {:error, String.t()}
  def import_commands_renaming(module, rename) do
    with {:commands_info, %{commands: cmds}} <- commands_info(module),
         {:ok, rename} <- validate_where(module, rename, cmds),
         imports <- Enum.map(cmds, &{&1, &1}) ++ rename,
         {results, []} <-
           Enum.map(imports, fn {old_name, new_name} ->
             import_command(module, old_name, new_name)
           end)
           |> Enum.split_with(fn cmd -> match?({:ok, _}, cmd) end) do
      results |> Enum.map(&elem(&1, 1)) |> then(&{:ok, &1})
    else
      {results, errors} when is_list(results) and is_list(errors) ->
        {:error, errors |> Enum.map(&elem(&1, 1)) |> Enum.join()}

      error ->
        error
    end
  end

  @spec import_command(module(), atom(), atom()) :: {:ok, Macro.t()} | {:error, String.t()}
  def import_command(module, old_name, new_name) do
    with {:command_info, commands} <- command_info(module) do
      command = Enum.find(commands, fn info -> info.name == old_name end)
      %{result: result_type, arguments: arguments, module: module} = command

      args =
        Enum.map(arguments, fn {arg, type} ->
          if equivalent_type?(type, unknown_type()) do
            quote do: unquote(Macro.var(arg, nil))
          else
            quote do: unquote(Macro.var(arg, nil)) :: unquote(type)
          end
        end)

      state = Macro.var(:state, nil)
      arguments = Macro.var(:arguments, nil)
      result = Macro.var(:result, nil)
      valid = Macro.var(:valid, nil)

      quote do
        command unquote(new_name)(unquote_splicing(args)) :: unquote(result_type) do
          pre unquote(module).pre(unquote_splicing([state]))
          args unquote(module).args(unquote_splicing([state]))
          valid_args unquote(module).valid_args(unquote_splicing([state, arguments]))
          call unquote(module).call(unquote_splicing([arguments]))
          valid unquote(module).valid(unquote_splicing([state, arguments, result]))
          next unquote(module).next(unquote_splicing([state, arguments, result, valid]))
          post unquote(module).post(unquote_splicing([state, arguments, result, valid]))
          weight unquote(module).weight(unquote_splicing([state]))
        end
      end
      |> then(&{:ok, &1})
    else
      error -> error
    end
  end

  @doc """

  This function validates the option `:where`.

  ## Examples

      iex> validate_where(A, [f: :g], [:f])
      {:ok, [f: :g]}

      iex> validate_where(A, [f: :g], [:g])
      {:error, "`:where` error, missing `f` in `A`"}

      iex> validate_where(A, :f, [:f])
      {:error, "`:where` receives a keyword list"}

      iex> validate_where(A, [f: 1], [:f])
      {:error, "`1` in `:where` are not valid command names"}

  """
  @spec validate_where(module(), Keyword.t(atom()), [atom()]) ::
          {:ok, Keyword.t(atom())} | {:error, String.t()}
  def validate_where(module, where, cmds) do
    with true <- Keyword.keyword?(where),
         [] <- Keyword.values(where) |> Enum.filter(&(not is_atom(&1))),
         {:available, []} <- {:available, Enum.filter(where, fn {x, _} -> x not in cmds end)},
         {:overrides, []} <-
           {:overrides, Enum.filter(where, fn {_, x} -> x in cmds end)} do
      {:ok, where}
    else
      false ->
        {:error, "`:where` receives a keyword list"}

      {:available, values} ->
        values = Enum.map(values, &elem(&1, 0)) |> Enum.map_join(", ", &"`#{Atom.to_string(&1)}`")
        {:error, "`:where` error, missing #{values} in `#{inspect(module)}`"}

      {:overrides, values} ->
        values =
          Enum.map(values, fn {x, y} ->
            "`:where` error, cannot rename `#{Atom.to_string(x)}` to `#{Atom.to_string(y)}`, `#{Atom.to_string(y)}` already exists in `#{inspect(module)}`"
          end)

        {:error, Enum.join(values, "\n")}

      values when is_list(values) ->
        {:error, "#{atoms_to_string(values)} in `:where` are not valid command names"}
    end
  end

  @doc """

  This function validates the option `:hiding`

  ## Examples

      iex> validate_hiding(A, :f, [:f, :g])
      {:ok, [:f]}

      iex> validate_hiding(A, [:f, :g], [:f, :g])
      {:ok, [:f, :g]}

      iex> validate_hiding(A, [:h], [:f, :g])
      {:error, "`:hiding` error, missing `h` in `A`"}

  """
  @spec validate_hiding(module(), [atom()] | atom(), [atom()]) ::
          {:ok, [atom()]} | {:error, String.t()}
  def validate_hiding(module, hiding, cmds) when is_list(hiding) do
    with {:names, []} <- {:names, Enum.filter(hiding, &(not is_atom(&1)))},
         {:available, []} <- {:available, Enum.filter(hiding, &(&1 not in cmds))} do
      {:ok, hiding}
    else
      {:names, _} ->
        {:error, "`:hiding` receives a list of command names"}

      {:available, values} ->
        values = Enum.map_join(values, ", ", &"`#{Atom.to_string(&1)}`")
        {:error, "`:hiding` error, missing #{values} in `#{inspect(module)}`"}
    end
  end

  def validate_hiding(module, hiding, cmds) when is_atom(hiding) do
    validate_hiding(module, [hiding], cmds)
  end

  def validate_hiding(_, _, _) do
    {:error, "`:hiding` receives a list of command names"}
  end
end
