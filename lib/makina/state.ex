defmodule Makina.State do
  import Makina.Helpers
  alias Makina.Error

  @moduledoc false

  @typedoc """

  This type contains the options to configure the state of a model:
  - `:extends` contains the model to be extended.

  """
  @type option() :: {:docs, boolean()} | {:specs, boolean()} | {:extends, module()} | {:debug, Macro.t()}

  @options [:extends, :debug]

  ##################################################################################################
  # Macros
  ##################################################################################################

  @doc """

  This macro configures the module to allow state definitions. Accepts global options, this means
  that options passed to this macro are the default options for state declarations in the
  module. Supported options are defined by the `#{inspect(__MODULE__)}.option()` type.

  This macro registers the following module attributes:

  | attribute           | accumulate | initial value | description                             |
  |---------------------|------------|---------------|-----------------------------------------|
  | `:attributes`       | `true`     | `[]`          | names and code of the state attributes  |
  | `:state_options`    | `true`     | `options`     | global options for state                |
  | `:state_declared`   | `false`    | `false`       | `true` if the state is already declared |
  | `:state_info`       | `false`    | `nil`         | information about the environment       |
  | `:state_code`       | `true`     | `[]`          | code containing attribute information   |

  ## Examples

      iex> defmodule Example do
      ...>   use Makina.State
      ...> end
      iex> defmodule ExampleExtended do
      ...>   use Makina.State, extends: Example
      ...> end

      iex> defmodule Example do
      ...>   use Makina.State, extends: "name"
      ...> end
      ** (Makina.Error) `"name"` is not a valid module name

      iex> defmodule Example do
      ...>   use Makina.State, extends: A
      ...> end
      ** (Makina.Error) could not load `A`

      iex> defmodule Example do
      ...>   use Makina.State, extends: Enum
      ...> end
      ** (Makina.Error) module `Enum` does not contain a state

      iex> defmodule Example do
      ...>   use Makina.State, extends: Example
      ...> end
      ** (Makina.Error) `MakinaDoctestTest.Example` cannot extend itself

  """
  @spec __using__([option()]) :: Macro.t()
  defmacro __using__(options) do
    context = __CALLER__.module
    options = Enum.map(options, fn {k, v} -> {k, Macro.expand(v, __CALLER__)} end)

    with {:option, {:ok, extends}} <- {:option, Keyword.fetch(options, :extends)},
         {:ok, ^extends} <- validate_extends(extends, context),
         {:state_info, %{attributes: _attrs}} <- state_info(extends) do
      :ok
    else
      {:option, :error} -> :ok
      {:error, error} -> {:error, error}
      error -> {:error, "Unknown error: #{error}"}
    end
    |> then(fn
      :ok -> :ok
      {:error, message} -> Error.throw_error(message, __CALLER__)
    end)

    Module.register_attribute(context, :attributes, accumulate: true)
    Module.register_attribute(context, :state_options, accumulate: true)
    Module.register_attribute(context, :state_declared, accumulate: false)
    Module.register_attribute(context, :state_info, accumulate: false)
    Module.register_attribute(context, :state_code, accumulate: true)
    Module.register_attribute(context, :type, accumulate: false)
    # Extracts and registers the global options passed to `use`.
    register_options(__CALLER__, :state_options, @options, options)
    Module.put_attribute(context, :state_declared, false)

    quote do
      import unquote(__MODULE__), only: :macros
      @before_compile unquote(__MODULE__)
    end
  end

  @doc """

  This macro is used to declare the state of a model, each state attribute is stored in its own
  module. Receives a keyword list which contains the name of the attribute, the initial value and,
  optionally, its type. If no type is provided, its type is defaulted to `any()`. Global options
  state options can be overriden by using `state/2`.

  This macro reads the module attribute `:state_declared`.

  This macro modifies the following module attributes:

  | attribute           | value                   |
  |---------------------|-------------------------|
  | `:state_info`       | `__CALLER__`            |
  | `:state_code`       | `[{atom(), Macro.t()}]` |
  | `:state_declared`   | `true`                  |
  | `:attributes`       | `[{atom(), Macro.t()}]` |

  ## Examples

      iex> defmodule Example do
      ...>   use Makina.State
      ...>   state attr1: 1 :: integer()
      ...> end
      iex> defmodule ExampleExtended do
      ...>   use Makina.State, extends: Example
      ...> end

      iex> defmodule Example do
      ...>   use Makina.State
      ...>   state :no_keyword_list
      ...> end
      ** (Makina.Error) `state` receives a keyword list `attribute_name: ` `expression`

      iex> defmodule Example do
      ...>   use Makina.State
      ...>   state attr1: 1 :: integer()
      ...>   state attr2: 0
      ...> end
      ** (Makina.Error) multiple `state` declarations

      iex> defmodule Example do
      ...>   use Makina.State
      ...>   state attr1: 1 :: integer(), attr1: 1
      ...> end
      ** (Makina.Error) multiple declarations of attributes: `:attr1`

  """
  @spec state(Keyword.t(), [option()]) :: Macro.t()
  defmacro state(attributes \\ [], opts \\ []) do
    context = __CALLER__.module
    # State level options override global options.
    options = opts ++ Module.get_attribute(context, :state_options, [])

    with {:declared, false} <- {:declared, Module.get_attribute(context, :state_declared, false)},
         {:keyword, true} <- {:keyword, Keyword.keyword?(attributes)},
         freqs = Enum.frequencies(Keyword.keys(attributes)),
         repeated = Enum.filter(freqs, fn {_, v} -> v > 1 end),
         {:repeated, []} <- {:repeated, Keyword.keys(repeated)},
         extends = Keyword.get(options, :extends),
         {:extends, {:ok, extending_attrs}} <- {:extends, extends(extends, context)} do
      for {name, expr} <- attributes ++ extending_attrs do
        type = get_type(expr)
        expr = remove_types(expr)
        code = quote do: attribute(unquote(name), unquote(expr), unquote(type))
        Module.put_attribute(context, :attributes, name)
        Module.put_attribute(context, :state_code, {name, code})
      end

      :ok
    else
      {:declared, true} ->
        {:error, "multiple `state` declarations"}

      {:keyword, false} ->
        {:error, "`state` receives a keyword list `attribute_name: ` `expression`"}

      {:repeated, repeated} ->
        {:error, "multiple declarations of attributes: #{atoms_to_string(repeated)}"}

      {:extends, error} ->
        error

      error ->
        {:error, "unknown error #{error}"}
    end
    |> then(fn
      :ok -> :ok
      {:error, message} -> Error.throw_error(message, __CALLER__)
    end)

    Module.put_attribute(context, :state_info, __CALLER__)
    Module.put_attribute(context, :state_declared, true)

    []
  end

  @doc """

  This macro injects the types and the functions derived from the state declaration.

  This macro reads the module attributes: `:state_info`, `:state_code` and `:attributes`.

  """
  @spec __before_compile__(atom | %{:module => atom, optional(any) => any}) :: [] | Macro.t()
  defmacro __before_compile__(env) do
    context = env.module
    state_module = state_module_name(context)
    # If debug is enabled inserts the debug code in the module.
    options = Module.get_attribute(context, :state_options, [])
    debug = Keyword.get(options, :debug, Application.get_env(:makina, :debug))

    {attrs, state_code, env} =
      cond do
        Module.get_attribute(context, :state_declared) ->
          env = Module.get_attribute(context, :state_info, __CALLER__)
          attrs = Module.get_attribute(context, :attributes, [])
          state_code = Module.get_attribute(context, :state_code)
          {attrs, state_code, env}

        extends = Keyword.get(options, :extends) ->
          case extends(extends, context) do
            {:ok, extending_attrs} ->
              Enum.reduce(extending_attrs, {[], [], env}, fn {name, expr}, {attrs, code, env} ->
                type = get_type(expr)
                expr = remove_types(expr)
                expr = quote do: attribute(unquote(name), unquote(expr), unquote(type))
                Module.put_attribute(context, :attributes, name)
                {[name | attrs], [{name, expr} | code], env}
              end)

            {:error, message} ->
              Error.throw_error(message, __CALLER__)
          end

        true ->
          {[], [], env}
      end

    state_code = Enum.group_by(state_code, fn {name, _expr} -> name end)

    for {name, code} <- state_code do
      attribute_module = attribute_module_name(name, context)
      expr = code |> Enum.map(&elem(&1, 1))

      quote do
        unquote(debug)
        use Makina.State.Attribute
        unquote(expr)
        # extras
        unquote(import_definitions(context))
        unquote(import_types(context))
      end
      |> then(&Makina.Module.create(attribute_module, &1, env))
    end

    quote do
      unquote(debug)
      # types
      @type t() :: symbolic_state() | dynamic_state()
      unquote(symbolic(attrs, context))
      unquote(dynamic(attrs, context))
      unquote(updates(attrs, context))
      # functions
      unquote(initial(attrs, context))
      unquote(validate(attrs))
      unquote(update())

      @spec __makina_info__() :: Makina.State.state_info()
      def __makina_info__() do
        %{attributes: unquote(attrs), module: unquote(state_module)}
      end

      # extras
      unquote(import_definitions(context))
      unquote(import_types(context))
    end
    |> then(&Makina.Module.create(state_module, &1, env))

    []

  end


  ##################################################################################################
  # Helpers
  ##################################################################################################

  @doc """

  This function returns an AST that contains the definition of the initial state of the model.

  ## Examples

     iex> attributes = [:attr1, :attr2, :attr3]
     iex> initial(attributes, Example) |> Macro.to_string()
     quote do
       @spec initial() :: symbolic_state()
       def initial() do
         [
           attr1: Example.State.Attribute.Attr1.init(),
           attr2: Example.State.Attribute.Attr2.init(),
           attr3: Example.State.Attribute.Attr3.init()
         ]
         |> Enum.into(%{})
       end
     end |> Macro.to_string()

  """
  @spec initial([atom()], module()) :: Macro.t()
  def initial(attributes, context) do
    inits =
      for attr <- attributes do
        attribute_module_name(attr, context)
        |> then(&quote(do: unquote(&1).init))
        |> then(&{attr, &1})
      end

    quote do
      @spec initial() :: symbolic_state()
      def initial(), do: unquote(inits) |> Enum.into(%{})
    end
  end

  @doc """

  This function returns an AST which contains a function to validate the state of the model.

  ## Examples

     iex> attributes = [:attr1, :attr2, :attr3]
     iex> validate(attributes)

  """
  @spec validate([atom()]) :: Macro.t()
  def validate(attributes) do
    quote do
      @spec validate(t()) :: {:ok, t()} | {:error, String.t()}
      def validate(state) when is_map(state) do
        attrs = unquote(attributes)
        missing = Enum.filter(attrs, &(&1 not in Map.keys(state)))
        extra = Enum.filter(Map.keys(state), &(&1 not in attrs))

        if missing == [] and extra == [] do
          {:ok, state}
        else
          missing =
            if missing != [] do
              Makina.Helpers.atoms_to_string(missing)
              |> then(&"missing attributes: #{&1}")
            else
              ""
            end

          extra =
            if extra != [] do
              Makina.Helpers.atoms_to_string(extra)
              |> then(&"extra attributes: #{&1}")
            else
              ""
            end

          sep = if missing != "" and extra != "", do: ", ", else: ""

          {:error, missing <> sep <> extra}
        end
      end

      def validate(_state), do: {:error, "state should be a map"}
    end
  end

  @doc """

  This function returns an AST which contains a function to update the state model.

  ## Examples

      iex> update()

  """
  @spec update() :: Macro.t()
  def update() do
    quote do
      @spec update(symbolic_state(), symbolic_updates()) ::
              {:ok, symbolic_state()} | {:error, String.t()}
      @spec update(dynamic_state(), dynamic_updates()) ::
              {:ok, dynamic_state()} | {:error, String.t()}
      def update(state, updates) do
        with {:ok, state} <- validate(state),
             true <- Keyword.keyword?(updates) do
          Enum.reduce(updates, state, fn
            {k, v}, acc when is_function(v) -> Map.put(acc, k, v.(acc))
            {k, v}, acc -> Map.put(acc, k, v)
          end)
          |> validate()
        else
          false -> {:error, "updates are not in a keyword list"}
          error -> error
        end
      end
    end
  end

  @doc """

  This function returns an AST which contains the definition of the `symbolic_state()` type of a model.

  ## Examples

      iex> attributes = [:attr1, :attr2, :attr3]
      iex> symbolic(attributes, Example)

  """
  @spec symbolic([atom()], module()) :: Macro.t()
  def symbolic(attributes, context) do
    type =
      for attr <- attributes do
        attribute_module_name(attr, context)
        |> then(&quote(do: unquote(&1).symbolic_attribute()))
        |> then(&{attr, &1})
      end

    quote do
      @type symbolic_state() :: %{unquote_splicing(type)}
    end
  end

  @doc """

  This function returns an AST which contains the definition of the `dynamic_state()` type of a model.

  ## Examples

      iex> attributes = [:attr1, :attr2, :attr3]
      iex> dynamic(attributes, Example)

  """
  @spec dynamic([atom()], module()) :: Macro.t()
  def dynamic(attributes, context) do
    type =
      for attr <- attributes do
        attribute_module_name(attr, context)
        |> then(&quote(do: unquote(&1).dynamic_attribute()))
        |> then(&{attr, &1})
      end

    quote do
      @type dynamic_state() :: %{unquote_splicing(type)}
    end
  end

  @doc """

  This function returns an AST which contains the definition of the `updates()` type of a model.

  ## Examples

      iex> attributes = [:attr1, :attr2, :attr3]
      iex> symbolic(attributes, Example)

  """
  @spec updates([atom()], module()) :: Macro.t()
  def updates([], _) do
    quote do
      @type symbolic_updates() :: []
      @type dynamic_updates() :: []
    end
  end

  def updates(attributes, context) do
    sym_type =
      for attr <- attributes do
        module = attribute_module_name(attr, context)
        attr_type = quote do: unquote(module).symbolic_attribute()
        fun_type = quote(do: (symbolic_state() -> unquote(attr_type)))
        [{attr, attr_type}, {attr, fun_type}]
      end
      |> List.flatten()
      |> union_type()

    dyn_type =
      for attr <- attributes do
        module = attribute_module_name(attr, context)
        attr_type = quote do: unquote(module).dynamic_attribute()
        fun_type = quote(do: (dynamic_state() -> unquote(attr_type)))
        [{attr, attr_type}, {attr, fun_type}]
      end
      |> List.flatten()
      |> union_type()

    quote do
      @type symbolic_update() :: unquote_splicing(sym_type)
      @type symbolic_updates() :: [symbolic_update()]
      @type dynamic_update() :: unquote_splicing(dyn_type)
      @type dynamic_updates() :: [dynamic_update()]
    end
  end

  ##################################################################################################
  # Options Helpers
  ##################################################################################################

  @doc """

  This function extracts the attributes defined in the extending model.

  ## Examples

      iex> defmodule ExtendsExample do
      ...>   use Makina.State
      ...>   state x: 0 :: integer(), y: true
      ...> end
      iex> extends(Example, A)

  """
  @spec extends(module() | nil, module()) :: {:ok, [{atom(), Macro.t()}]} | {:error, String.t()}
  def extends(nil, _module), do: {:ok, []}

  def extends(extends, module) do
    with {:ok, extends} <- validate_extends(extends, module),
         {:state_info, %{attributes: attrs}} <- state_info(extends) do
      for name <- attrs do
        {:attribute_info, attr_info} = attribute_info(extends, name)
        %{type: type, module: attr_module} = attr_info
        expr = quote do: unquote(attr_module).init() :: unquote(type)
        {name, expr}
      end
      |> then(&{:ok, &1})
    end
  end

  ##################################################################################################
  # AST helpers
  ##################################################################################################

  @doc """

  This function retrieves the type from a typed expression `expr :: type`. Returns `any()` if no
  type annotation is provided.

  ## Examples

      iex> expr = quote do: 1 :: integer()
      iex> get_type(expr)
      quote do: integer()

  """
  @spec get_type(Macro.t()) :: atom() | String.t()
  def get_type({:"::", _, [_, type]}), do: type
  def get_type(_), do: quote(do: any())

  @doc """

  This function removes the type from a typed expression `expr :: type` and returns just the
  expression `expr`.

  ## Examples

      iex> expr = quote do: 1 :: integer()
      iex> remove_types(expr)
      quote do: 1

  """
  @spec remove_types(Macro.t()) :: Macro.t()
  def remove_types({:"::", _, [ast, _]}), do: ast
  def remove_types(ast), do: ast

  ##################################################################################################
  # Information functions
  ##################################################################################################

  @type state_info() :: %{attributes: [atom()], module: module()}

  @doc """

  This function extracts the state information from a model.

  ## Examples

      iex> defmodule Example do
      ...>   use Makina.State
      ...>   state attribute: 0 :: integer()
      ...> end
      iex> state_info(Example)
      {:state_info, %{attributes: [:attribute], module: MakinaDoctestTest.Example.State}}

      iex> state_info(Enum)
      {:error, "module `Enum` does not contain a state"}
      iex> state_info(A)
      {:error, "could not load `A`"}

  """
  @spec state_info(module()) :: {:state_info, state_info()} | {:error, String.t()}
  def state_info(module) when is_atom(module) do
    with {:module, ^module} <- ensure_compiled(module),
         state_module = state_module_name(module),
         {:state_module, {:module, ^state_module}} <-
           {:state_module, ensure_compiled(state_module)},
         true <- function_exported?(state_module, :__makina_info__, 0) do
      {:state_info, state_module.__makina_info__()}
    else
      {:state_module, _} -> {:error, "module `#{inspect(module)}` does not contain a state"}
      {:error, message} when is_bitstring(message) -> {:error, message}
      false -> {:error, "could not load information about the state module of model `#{module}`"}
      unknown -> {:error, "unknown error: `#{inspect(unknown)}`"}
    end
  end

  def state_info(module), do: {:error, "`#{inspect(module)}` is not a valid module name"}

  @type attribute_info() :: %{
          name: atom(),
          dynamic: Macro.t(),
          symbolic: Macro.t(),
          type: Macro.t(),
          module: module()
        }

  @doc """

  This function extracts the attribute information from a model.

  ## Examples

      iex> defmodule Example do
      ...>   use Makina.State
      ...>   state attribute: 0 :: integer()
      ...> end
      iex> {:attribute_info, info} = attribute_info(Example, :attribute)
      iex> Map.keys(info)
      [:module, :name, :type, :dynamic, :symbolic]

      iex> attribute_info(Enum, :attribute)
      {:error, "module `Enum` does not contain a state"}
      iex> attribute_info(A, :attribute)
      {:error, "could not load `A`"}

  """
  @spec attribute_info(module(), atom()) ::
          {:attribute_info, attribute_info()} | {:error, String.t()}
  def attribute_info(module, attribute) when is_atom(module) and is_atom(attribute) do
    with {:state_info, state} <- state_info(module),
         {:contained?, true} <- {:contained?, attribute in state.attributes},
         attribute_module = attribute_module_name(attribute, module),
         {:module, ^attribute_module} <- ensure_compiled(attribute_module),
         true <- {:__makina_info__, 0} in attribute_module.__info__(:functions) do
      {:attribute_info, attribute_module.__makina_info__()}
    else
      {:contained?, false} ->
        {:error, "state in `#{inspect(module)}` does not contain an attribute `#{attribute}`"}

      {:error, message} when is_bitstring(message) ->
        {:error, message}

      unknown ->
        {:error, "unknown error: `#{inspect(unknown)}`"}
    end
  end

  def attribute_info(module, atom) when is_atom(module),
    do: {:error, "`#{inspect(atom)}` is not a valid command name"}

  def attribute_info(module, _atom),
    do: {:error, "`#{inspect(module)}` is not a valid module name"}

  @doc """

  This function extracts the information about all the attributes in a model.

  ## Examples

      iex> defmodule Example do
      ...>   use Makina.State
      ...>   state attr1: 0 :: integer, attr2: true
      ...> end
      iex> {:attribute_info, info} = attribute_info(Example)
      iex> 2 = length(info)

      iex> attribute_info(Enum)
      {:error, "module `Enum` does not contain a state"}
      iex> attribute_info(A)
      {:error, "could not load `A`"}

  """
  @spec attribute_info(module) :: {:attribute_info, [attribute_info()]} | {:error, String.t()}
  def attribute_info(module) do
    case state_info(module) do
      {:state_info, %{attributes: attributes}} ->
        Enum.map(attributes, &attribute_info(module, &1))
        |> Enum.split_with(&match?({:error, _}, &1))
        |> then(fn
          {[], attributes} -> {:attribute_info, attributes |> Keyword.values()}
          {errors, _} -> concat_error_messages(errors)
        end)

      error ->
        error
    end
  end
end
