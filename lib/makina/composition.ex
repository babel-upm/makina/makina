defmodule Makina.Composition do
  import Makina.Helpers
  import Makina.State, only: [attribute_info: 1]
  import Makina.Invariants, only: [invariant_info: 1]
  import Makina.Command, only: [command_info: 1]

  alias Makina.Error

  @moduledoc false

  @typedoc """

  This type represents the command options processed by this module:
  - `:extends` contains the list of models to compose.

  """
  @type option() :: [extends: module()]

  @typep command_info() :: Makina.Command.command_info()

  ##################################################################################################
  # Macros
  ##################################################################################################

  @doc """

  This macro generates a Makina model that composes a list of models.

  ## Examples

      iex> defmodule F do
      ...>   use Makina
      ...>   state attr1: 0 :: integer()
      ...>   command f() do
      ...>     call 1
      ...>    end
      ...> end
      iex> defmodule G do
      ...>   use Makina
      ...>   state attr2: 0 :: integer()
      ...>   command g() do
      ...>     call :ok
      ...>    end
      ...> end
      iex> defmodule FG do
      ...>   use Makina.Composition, extends: [F, G]
      ...> end

      iex> defmodule FG do
      ...>   use Makina.Composition, extends: F
      ...> end
      ** (Makina.Error) composition receives a list of models

      iex> defmodule FG do
      ...>   use Makina.Composition, extends: [Enum]
      ...> end
      ** (Makina.Error) module `Enum` does not contain a state

  """
  @spec __using__(extends: Macro.t()) :: Macro.t()
  defmacro __using__(extends: modules) do
    modules =
      if is_list(modules) do
        Enum.map(modules, &Macro.expand(&1, __CALLER__))
      else
        Macro.expand(modules, __CALLER__)
      end

    with {:ok, modules} <- validate_composition(modules),
         {:ok, state} <- compose_states(modules),
         {:ok, invariants} <- compose_invariants(modules),
         {:ok, commands} <- compose_commands(modules) do
      quote do
        use Makina
        unquote(state)
        unquote(invariants)
        unquote_splicing(commands)
      end
    else
      {:error, error} -> {:error, error}
    end
    |> then(fn
      {:error, error} -> Error.throw_error(error, __CALLER__)
      expr -> expr
    end)
  end

  defmacro __using__(_opts) do
    Makina.Error.throw_error("syntax error in `use #{inspect(__MODULE__)}`")
  end

  ##################################################################################################
  # Helpers
  ##################################################################################################

  @doc """

  This function returns the the composition of the states in the modules passed in its arguments.

  TODO: should generate warnings when attribute types are not equivalent

  ## Examples

      iex> defmodule A do
      ...>   use Makina.State
      ...>   state attr1: 0
      ...> end
      iex> defmodule B do
      ...>   use Makina.State
      ...>   state attr2: 1
      ...> end
      iex> {:ok, state} = compose_states([A, B])
      iex> result = state |> Macro.to_string()
      iex> code = quote do
      ...>   state attr1: MakinaDoctestTest.A.State.Attribute.Attr1.init() :: any(),
      ...>         attr2: MakinaDoctestTest.B.State.Attribute.Attr2.init() :: any()
      ...> end |> Macro.to_string()
      iex> assert result =~ code

      iex> {:error, "could not load `A`" <> _} = compose_states([A, B])

  """
  @spec compose_states([module()]) :: {:ok, Macro.t()} | {:error, String.t()}
  def compose_states(modules) do
    {errors, attributes} =
      Enum.map(modules, &attribute_info/1)
      |> Enum.split_with(&match?({:error, _error}, &1))

    case errors do
      [] ->
        attributes
        |> Enum.map(fn {_k, v} -> v end)
        |> Enum.concat()
        |> Enum.group_by(fn attr -> attr.name end)
        |> Enum.map(fn {_name, attrs} -> hd(attrs) end)
        |> Enum.map(fn attr ->
          {attr.name, quote(do: unquote(attr.module).init :: unquote(attr.type))}
        end)
        |> then(fn expr -> {:ok, quote(do: state(unquote(expr)))} end)

      errors when is_list(errors) ->
        concat_error_messages(errors)
    end
  end

  @doc """

  This function returns the the composition of the invariants in the modules passed in its
  arguments.

  ## Examples

      iex> defmodule A do
      ...>   use Makina.Invariants
      ...>   invariants invariant_1: true
      ...> end
      iex> defmodule B do
      ...>   use Makina.Invariants
      ...>   invariants invariant_2: true
      ...> end
      iex> {:ok, invariants} = compose_invariants([A, B])
      iex> result = invariants |> Macro.to_string()
      iex> code = quote do
      ...>   invariants invariant_1: MakinaDoctestTest.A.Invariant.Invariant1.check(state),
      ...>              invariant_2: MakinaDoctestTest.B.Invariant.Invariant2.check(state)
      ...> end |> Macro.to_string()
      iex> assert result =~ code

  """
  @spec compose_invariants([module()]) :: {:ok, Macro.t()} | {:error, String.t()}
  def compose_invariants(modules) do
    state = Macro.var(:state, nil)

    {errors, invariants} =
      Enum.map(modules, &invariant_info/1)
      |> Enum.split_with(&match?({:error, _error}, &1))

    with [] <- errors,
         invariants =
           invariants
           |> Enum.map(fn {_k, v} -> v end)
           |> Enum.concat()
           |> Enum.group_by(fn inv -> inv.name end)
           |> Enum.map(fn {name, invariants} ->
             {name, Enum.uniq_by(invariants, & &1.defined_in)}
           end),
         {:freqs, []} <-
           {:freqs, Enum.filter(invariants, fn {_name, invs} -> length(invs) > 1 end)} do
      invariants
      |> Enum.map(fn {name, [%{module: module}]} ->
        {name, quote(do: unquote(module).check(unquote(state)))}
      end)
      |> then(&{:ok, quote(do: invariants(unquote(&1)))})
    else
      errors when is_list(errors) ->
        concat_error_messages(errors)

      {:freqs, freqs} ->
        {:error, "multiple declarations of invariants: #{atoms_to_string(Keyword.keys(freqs))}"}
    end
  end

  @doc """

  This function returns the composition of the commands in the models passed as arguments.

  ## Examples

      iex> defmodule A do
      ...>   use Makina.Command
      ...>   command f(x), defaults: true do
      ...>     call :ok
      ...>   end
      ...> end
      iex> defmodule B do
      ...>    use Makina.Command
      ...>    command f(y), defaults: true do
      ...>      call :ok
      ...>    end
      ...> end
      iex> compose_commands([A, B])
      {:error, "different arguments in composition of command `f`"}

      iex> defmodule A do
      ...>   use Makina.Command
      ...>   command f(), defaults: true do
      ...>      call :ok
      ...>    end
      ...> end
      iex> defmodule B do
      ...>    use Makina.Command
      ...>    command g(), defaults: true do
      ...>      call :ok
      ...>    end
      ...> end
      iex> {:ok, commands} = compose_commands([A, B])
      iex> commands |> Macro.to_string()

  """
  @spec compose_commands([module()]) :: {:ok, [Macro.t()]} | {:error, String.t()}
  def compose_commands(modules) do
    {errors, commands} =
      Enum.map(modules, &command_info/1)
      |> Enum.split_with(&match?({:error, _error}, &1))

    with [] <- errors,
         {[], commands} <-
           commands
           |> Enum.map(fn {_k, v} -> v end)
           |> Enum.concat()
           |> Enum.group_by(fn cmd -> cmd.name end)
           |> Enum.map(fn {name, info} -> validate_command_composition(name, info) end)
           |> Enum.split_with(&match?({:error, _error}, &1)) do
      commands =
        for {:ok, {name, infos}} <- commands, base = hd(infos) do
          arguments =
            base.arguments
            |> Enum.map(fn {name, type} ->
              quote do: unquote(Macro.var(name, nil)) :: unquote(type)
            end)

          result = base.result

          quote do
            command unquote(name)(unquote_splicing(arguments)) :: unquote(result) do
              unquote(compose_pre(infos))
              unquote(compose_args(infos))
              unquote(compose_call(infos))
              unquote(compose_valid_args(infos))
              unquote(compose_valid(infos))
              unquote(compose_next(infos))
              unquote(compose_post(infos))
            end
          end
        end

      {:ok, commands}
    else
      errors when is_list(errors) -> concat_error_messages(errors)
      {errors, _} -> concat_error_messages(errors)
    end
  end

  @doc """

  This functions returns the parallel composition of the precondition. The precondition is
  disjunctive.

  ## Examples

      iex> defmodule A do
      ...>   use Makina.Command
      ...>   command f(), defaults: true do
      ...>      call :ok
      ...>    end
      ...> end
      iex> defmodule B do
      ...>    use Makina.Command
      ...>    command f(), defaults: true do
      ...>      call :ok
      ...>    end
      ...> end
      iex> {:command_info, a_info} = Makina.Command.command_info(A, :f)
      iex> {:command_info, b_info} = Makina.Command.command_info(B, :f)
      iex> result = compose_pre([a_info, b_info]) |> Macro.to_string()
      iex> code = quote do
      ...>   pre  ((pre = MakinaDoctestTest.B.Command.F.pre(state)) || is_nil(pre)) || (((pre = MakinaDoctestTest.A.Command.F.pre(state)) || is_nil(pre)) || false)
      ...> end |> Macro.to_string()
      iex> assert result =~ code

  """
  @spec compose_pre([command_info()]) :: Macro.t()
  def compose_pre(infos) do
    state = Macro.var(:state, nil)

    for %{callbacks: callbacks, module: module} <- infos, :pre in callbacks, reduce: false do
      acc ->
        quote generated: true,
              do: (pre = unquote(module).pre(unquote(state))) || is_nil(pre) || unquote(acc)
    end
    |> then(&quote do: pre(unquote(&1)))
  end

  @doc """

  This function returns a callback which contains the composition of the parameter generator. This
  generator is a random choice between the "enabled" composed models. An enabled model is one that
  contains a definition of this callback and a true precondition.

  ## Examples

      iex> defmodule A do
      ...>   use Makina.Command
      ...>   command f(), defaults: true do
      ...>      call :ok
      ...>    end
      ...> end
      iex> defmodule B do
      ...>    use Makina.Command
      ...>    command f(), defaults: true do
      ...>      call :ok
      ...>    end
      ...> end
      iex> {:command_info, a_info} = Makina.Command.command_info(A, :f)
      iex> {:command_info, b_info} = Makina.Command.command_info(B, :f)
      iex> result = compose_args([a_info, b_info]) |> Macro.to_string()
      iex> code = quote do
      ...>   args do
      ...>     enabled = [MakinaDoctestTest.A.Command.F, MakinaDoctestTest.B.Command.F]
      ...>               |> Enum.filter(fn module -> (pre = module.pre(state)) || is_nil(pre) end)
      ...>     let(module <- oneof(enabled)) do
      ...>       module.args(state)
      ...>     end
      ...>   end
      ...> end |> Macro.to_string()
      iex> assert result =~ code

  """
  @spec compose_args([command_info()]) :: Macro.t()
  def compose_args(infos) do
    state = Macro.var(:state, nil)

    modules =
      for %{callbacks: callbacks, module: module} <- infos,
          :pre in callbacks and :args in callbacks,
          do: module

    quote do
      args do
        enabled =
          unquote(modules)
          |> Enum.filter(fn module -> (pre = module.pre(unquote(state))) || is_nil(pre) end)

        let(module <- oneof(enabled), do: module.args(unquote(state)))
      end
    end
  end

  @doc """

  This function composes the call to the implementation. The parallalel composition performs a
  random choice between the submodels that contain an implementation for the call callback.

  ## Examples

      iex> defmodule A do
      ...>   use Makina.Command
      ...>   command f(), defaults: true do
      ...>      call :ok
      ...>    end
      ...> end
      iex> defmodule B do
      ...>    use Makina.Command
      ...>    command f(), defaults: true do
      ...>      call :ok
      ...>    end
      ...> end
      iex> {:command_info, a_info} = Makina.Command.command_info(A, :f)
      iex> {:command_info, b_info} = Makina.Command.command_info(B, :f)
      iex> result = compose_call([a_info, b_info]) |> Macro.to_string()
      iex> code = quote do
      ...>   call do
      ...>     [MakinaDoctestTest.A.Command.F, MakinaDoctestTest.B.Command.F]
      ...>     |> Enum.random()
      ...>     |> then(fn module -> module.call(arguments) end)
      ...>   end
      ...> end |> Macro.to_string()
      iex> assert result =~ code

  """
  @spec compose_call([command_info()]) :: Macro.t()
  def compose_call(infos) do
    arguments = Macro.var(:arguments, nil)
    modules = for %{callbacks: callbacks, module: module} <- infos, :call in callbacks, do: module

    quote do
      call do
        unquote(modules)
        |> Enum.random()
        |> then(fn module -> module.call(unquote(arguments)) end)
      end
    end
  end

  @doc """

  This functions returns the parallel composition of the valid_args callback. This callback is
  conjunctive.

  ## Examples

      iex> defmodule A do
      ...>   use Makina.Command
      ...>   command f(), defaults: true do
      ...>      call :ok
      ...>    end
      ...> end
      iex> defmodule B do
      ...>    use Makina.Command
      ...>    command f(), defaults: true do
      ...>      call :ok
      ...>    end
      ...> end
      iex> {:command_info, a_info} = Makina.Command.command_info(A, :f)
      iex> {:command_info, b_info} = Makina.Command.command_info(B, :f)
      iex> result = compose_valid_args([a_info, b_info]) |> Macro.to_string()
      iex> code = quote do
      ...> valid_args ((valid_args = MakinaDoctestTest.B.Command.F.valid_args(state, arguments)) || is_nil(valid_args)) &&
      ...>              (((valid_args = MakinaDoctestTest.A.Command.F.valid_args(state, arguments)) || is_nil(valid_args)) && true)
      ...> end |> Macro.to_string()
      iex> assert result =~ code

  """
  @spec compose_valid_args([command_info()]) :: Macro.t()
  def compose_valid_args(infos) do
    state = Macro.var(:state, nil)
    arguments = Macro.var(:arguments, nil)
    args = [state, arguments]

    for %{callbacks: callbacks, module: module} <- infos, :valid_args in callbacks, reduce: true do
      acc ->
        quote generated: true do
          ((valid_args = unquote(module).valid_args(unquote_splicing(args))) || is_nil(valid_args)) &&
            unquote(acc)
        end
    end
    |> then(&quote do: valid_args(unquote(&1)))
  end

  @doc """

  This functions returns the parallel composition of the valid callback. This callback is
  conjunctive.

  ## Examples

      iex> defmodule A do
      ...>   use Makina.Command
      ...>   command f(), defaults: true do
      ...>      call :ok
      ...>    end
      ...> end
      iex> defmodule B do
      ...>    use Makina.Command
      ...>    command f(), defaults: true do
      ...>      call :ok
      ...>    end
      ...> end
      iex> {:command_info, a_info} = Makina.Command.command_info(A, :f)
      iex> {:command_info, b_info} = Makina.Command.command_info(B, :f)
      iex> result = compose_valid([a_info, b_info]) |> Macro.to_string()
      iex> code = quote do
      ...> valid ((valid = MakinaDoctestTest.B.Command.F.valid(state, arguments, result)) || is_nil(valid)) &&
      ...>         (((valid = MakinaDoctestTest.A.Command.F.valid(state, arguments, result)) || is_nil(valid)) && true)
      ...> end |> Macro.to_string()
      iex> assert result =~ code

  """
  @spec compose_valid([command_info()]) :: Macro.t()
  def compose_valid(infos) do
    state = Macro.var(:state, nil)
    arguments = Macro.var(:arguments, nil)
    result = Macro.var(:result, nil)
    args = [state, arguments, result]

    for %{callbacks: callbacks, module: module} <- infos, :valid in callbacks, reduce: true do
      acc ->
        quote generated: true,
              do:
                ((valid = unquote(module).valid(unquote_splicing(args))) || is_nil(valid)) &&
                  unquote(acc)
    end
    |> then(&quote do: valid(unquote(&1)))
  end

  @doc """

  This function returns the parallel composition of the next callback. This callback is the
  concatenation of the updates in the submodules that form the composition.

  ## Examples

      iex> defmodule A do
      ...>   use Makina.Command
      ...>   command f(), defaults: true do
      ...>      call :ok
      ...>    end
      ...> end
      iex> defmodule B do
      ...>    use Makina.Command
      ...>    command f(), defaults: true do
      ...>      call :ok
      ...>    end
      ...> end
      iex> {:command_info, a_info} = Makina.Command.command_info(A, :f)
      iex> {:command_info, b_info} = Makina.Command.command_info(B, :f)
      iex> result = compose_next([a_info, b_info]) |> Macro.to_string()
      iex> code = quote do
      ...>   next do
      ...>     for module <- [MakinaDoctestTest.A.Command.F, MakinaDoctestTest.B.Command.F] do
      ...>       module.next(state, arguments, result, module.valid(state, arguments, result))
      ...>       |> then(&if is_nil(&1), do: [], else: &1)
      ...>     end
      ...>     |> Enum.concat()
      ...>   end
      ...> end |> Macro.to_string()
      iex> assert result =~ code

  """
  @spec compose_next([command_info()]) :: Macro.t()
  def compose_next(infos) do
    state = Macro.var(:state, nil)
    arguments = Macro.var(:arguments, nil)
    result = Macro.var(:result, nil)

    modules = for %{callbacks: callbacks, module: module} <- infos, :next in callbacks, do: module

    quote do
      next do
        for module <- unquote(modules) do
          module.next(
            unquote(state),
            unquote(arguments),
            unquote(result),
            module.valid(unquote(state), unquote(arguments), unquote(result))
          )
          |> then(&if is_nil(&1), do: [], else: &1)
        end
        |> Enum.concat()
      end
    end
  end

  @doc """

  This functions returns the parallel composition of the postcondition. This callback is
  conjunctive.

  ## Examples

      iex> defmodule A do
      ...>   use Makina.Command
      ...>   command f(), defaults: true do
      ...>      call :ok
      ...>    end
      ...> end
      iex> defmodule B do
      ...>    use Makina.Command
      ...>    command f(), defaults: true do
      ...>      call :ok
      ...>    end
      ...> end
      iex> {:command_info, a_info} = Makina.Command.command_info(A, :f)
      iex> {:command_info, b_info} = Makina.Command.command_info(B, :f)
      iex> result = compose_post([a_info, b_info]) |> Macro.to_string()
      iex> code = quote do
      ...>   post ((post = MakinaDoctestTest.B.Command.F.post(state, arguments, result, valid)) || is_nil(post))
      ...>         && (((post = MakinaDoctestTest.A.Command.F.post(state, arguments, result, valid)) || is_nil(post)) && true)
      ...> end |> Macro.to_string()
      iex> assert result =~ code

  """
  @spec compose_post([command_info()]) :: Macro.t()
  def compose_post(infos) do
    state = Macro.var(:state, nil)
    arguments = Macro.var(:arguments, nil)
    result = Macro.var(:result, nil)
    valid = Macro.var(:valid, nil)
    args = [state, arguments, result, valid]

    for %{callbacks: callbacks, module: module} <- infos, :post in callbacks, reduce: true do
      acc ->
        quote generated: true do
          ((post = unquote(module).post(unquote_splicing(args))) || is_nil(post)) && unquote(acc)
        end
    end
    |> then(&quote do: post(unquote(&1)))
  end

  @doc """

  This function validates the parameters of the composition operator.

  ## Examples

      iex> validate_composition([A, Enum])
      {:error, "could not load `A`"}

      iex> validate_composition(["Enum"])
      {:error, "composition receives a list of modules"}

      iex> validate_composition([Enum, Kernel])
      {:ok, [Enum, Kernel]}

  """
  @spec validate_composition([module()]) :: {:ok, [module()]}
  def validate_composition(modules) do
    with true <- is_list(modules),
         [] <- Enum.filter(modules, &(not is_atom(&1))) do
      errors =
        for module <- modules, ensured = ensure_compiled(module), match?({:error, _}, ensured) do
          ensured
        end

      if errors == [], do: {:ok, modules}, else: concat_error_messages(errors)
    else
      false -> {:error, "composition receives a list of models"}
      values when is_list(values) -> {:error, "composition receives a list of modules"}
    end
  end

  @doc """

  This function checks that the given commands can be composed.

  ## Examples

      iex> defmodule A do
      ...>   use Makina.Command
      ...>   command f(x), defaults: true do
      ...>     call :ok
      ...>   end
      ...> end
      iex> defmodule B do
      ...>    use Makina.Command
      ...>    command f(), defaults: true do
      ...>      call :ok
      ...>    end
      ...> end
      iex> {:command_info, a_info} = Makina.Command.command_info(A, :f)
      iex> {:command_info, b_info} = Makina.Command.command_info(B, :f)
      iex> {:ok, _commands} = validate_command_composition(:f, [a_info, a_info])
      iex> validate_command_composition(:f, [a_info, b_info])
      {:error, "multiple arities in the composition of command `f`"}
      iex> defmodule C do
      ...>    use Makina.Command
      ...>    command f(y), defaults: true do
      ...>      call :ok
      ...>    end
      ...> end
      iex> {:command_info, c_info} = Makina.Command.command_info(C, :f)
      iex> validate_command_composition(:f, [a_info, c_info])
      {:error, "different arguments in composition of command `f`"}

  """
  @spec validate_command_composition(atom(), [command_info()]) ::
          {:ok, {atom(), [command_info()]}} | {:error, String.t()}
  def validate_command_composition(name, infos) do
    with {:arity, [_]} <-
           {:arity, Enum.group_by(infos, fn i -> length(i.arguments) end) |> Enum.to_list()},
         {:args, [_]} <- {:args, Enum.group_by(infos, fn i -> i.arguments end) |> Enum.to_list()} do
      {:ok, {name, infos}}
    else
      {:arity, _arities} -> {:error, "multiple arities in the composition of command `#{name}`"}
      {:args, _args} -> {:error, "different arguments in composition of command `#{name}`"}
    end
  end
end
