defmodule Makina.Behaviour do
  import Makina.Helpers

  @moduledoc false

  ##################################################################################################
  # Macros
  ##################################################################################################

  @doc """

  This macro configures the module to make it compatible with `PropCheck` and `eqc_ex` state
  machines.

  This macro registers the following module attributes:

  | attribute     | accumulate | initial value | description                   |
  |---------------|------------|---------------|-------------------------------|
  | `:attributes` | `true`     | `[]`          | names of the state attributes |
  | `:invariants` | `true`     | `[]`          | names of the model invariants |
  | `:commands`   | `true`     | `[]`          | names of the model commands   |

  ## Examples

      iex> defmodule Example do
      ...>   use Makina.Behaviour
      ...> end

  """
  @spec __using__(any()) :: Macro.t()
  defmacro __using__(options) do
    Module.register_attribute(__CALLER__.module, :attributes, accumulate: true)
    Module.register_attribute(__CALLER__.module, :invariants, accumulate: true)
    Module.register_attribute(__CALLER__.module, :commands, accumulate: true)
    Module.register_attribute(__CALLER__.module, :behaviour_options, accumulate: true)
    checker = get_checker()
    debug = Keyword.get(options, :debug, Application.get_env(:makina, :debug))

    quote do
      unquote(debug)
      use unquote(checker)
      import unquote(__MODULE__), only: :macros
      @before_compile unquote(__MODULE__)
    end
  end

  @doc """

  This macro generates the model behaviour that makes Makina models compatible with `PropCheck` and
  `eqc_ex` state machines.

  This macro reads the module attributes: `:commands`.

  ## Examples

      iex> defmodule Example do
      ...>   use Makina.Behaviour
      ...>   @attributes :attr1
      ...>   @invariants :inv1
      ...>   @commands :f
      ...> end

  """
  @spec __before_compile__(Macro.Env.t()) :: Macro.t()
  defmacro __before_compile__(_env) do
    cmds = Module.get_attribute(__CALLER__.module, :commands)
    context = __CALLER__.module

    # If debug is enabled inserts the debug code in the module.
    debug =
      Module.get_attribute(context, :behaviour_options, [])
      |> Keyword.get(:debug, Application.get_env(:makina, :debug))

    docs =
      quote do
        @moduledoc "Contains the behaviour of the model."
      end

    behaviour_module = :"#{context}.Behaviour"

    [
      debug,
      initial_state(context),
      call_command(),
      precondition(context),
      arguments(context),
      command(context),
      commands(cmds),
      weight(context),
      weights(),
      call_args(),
      validate_args(context),
      next_state(context),
      postcondition(context),
      call_features(context),
      types(cmds, context),
      docs
    ]
    |> then(&Makina.Module.create(behaviour_module, &1, __CALLER__))

    quote do
      defdelegate initial_state(), to: unquote(behaviour_module)
      defdelegate precondition(state, call), to: unquote(behaviour_module)
      defdelegate arguments(state, call), to: unquote(behaviour_module)
      defdelegate command(state), to: unquote(behaviour_module)
      defdelegate next_state(state, result, call), to: unquote(behaviour_module)
      defdelegate call_features(state, call, result), to: unquote(behaviour_module)
      defdelegate postcondition(state, call, result), to: unquote(behaviour_module)
      defdelegate weight(state, call), to: unquote(behaviour_module)
    end
  end

  ##################################################################################################
  # Behaviour functions
  ##################################################################################################

  @doc """

  This function generates the function `initial_state/0` of the model.

  ## Examples

      iex> initial_state(Example) |> Macro.to_string()
      quote do
        def initial_state() do
          Example.State.initial()
        end
      end |> Macro.to_string()

  """
  @spec initial_state(module()) :: Macro.t()
  def initial_state(context) do
    quote do
      def initial_state(), do: unquote(:"#{context}.State").initial()
    end
  end

  @doc """

  This function generates the function `precondition/2` of the model which is used to filter the
  command generation.

  ## Examples

      iex> result = precondition(Example) |> Macro.to_string()
      iex> code = quote do
      ...>   @doc "\\nThis function returns true if a call can be generated from the given state.\\n\\n"
      ...>   def precondition(state, call) do
      ...>     model = Example
      ...>     command = call_command(call)
      ...>     command_module = Makina.Helpers.command_module_name(command, Example)
      ...>     pre = command_module.pre(state)
      ...>     is_nil(pre) || pre
      ...>   end
      ...> end |> Macro.to_string()
      iex> assert result =~ code

  """

  @doc_template ~S"""

  This function returns true if a call can be generated from the given state.

  """
  @spec precondition(module()) :: Macro.t()
  def precondition(context) do
    quote do
      # @spec precondition(symbolic_state(), command() | symbolic_call()) :: boolean()
      # @spec precondition(dynamic_state(), command() | dynamic_call()) :: boolean()
      # TODO: this is not correct according to proper's documentation, but proper uses this types.
      @spec precondition(
              symbolic_state() | dynamic_state(),
              command() | symbolic_call() | dynamic_call()
            ) :: boolean()
      @doc unquote(EEx.eval_string(@doc_template))
      def precondition(state, call) do
        model = unquote(context)
        command = call_command(call)
        command_module = Makina.Helpers.command_module_name(command, unquote(context))
        pre = command_module.pre(state)
        is_nil(pre) || pre
      end
    end
  end

  @doc """

  This function generates the function `arguments/2` of the model which is is used to generate the
  arguments of a command.

  ## Examples

      iex> result = arguments(Example) |> Macro.to_string()
      iex> code = quote do
      ...>   @spec arguments(symbolic_state(), command()) :: Makina.Types.generator(symbolic_arguments())
      ...>   @doc "\\nThis function generates the arguments for a given call.\\n\\n"
      ...>   def arguments(state, command) do
      ...>     model = Example
      ...>     command_module = Makina.Helpers.command_module_name(command, Example)
      ...>     command_module.args(state)
      ...>   end
      ...> end |> Macro.to_string()
      iex> assert result =~ code

  """

  @doc_template ~S"""

  This function generates the arguments for a given call.

  """
  @spec arguments(atom()) :: Macro.t()
  def arguments(context) do
    quote do
      @spec arguments(symbolic_state(), command()) :: Makina.Types.generator(symbolic_arguments())
      @doc unquote(EEx.eval_string(@doc_template))
      def arguments(state, command) do
        model = unquote(context)
        command_module = Makina.Helpers.command_module_name(command, unquote(context))
        command_module.args(state)
      end
    end
  end

  @doc """

  This function generates the function `commands/0` which returns the names of the commands of the
  model.

  ## Examples

      iex> result = commands([:f, :g]) |> Macro.to_string()
      iex> code = quote do
      ...>   @spec commands() :: [command()]
      ...>   defp commands() do
      ...>     [:f, :g]
      ...>   end
      ...> end |> Macro.to_string()
      iex> assert result =~ code

  """
  @spec commands([atom()]) :: Macro.t()
  def commands(commands) do
    quote do
      @spec commands() :: [command()]
      defp commands(), do: unquote(Enum.map(commands, &:"#{&1}"))
    end
  end

  @doc """

  This function generates the function `weight/2` which returns the weight of a given command of the
  model.

  ## Examples

      iex> result = weight(Example) |> Macro.to_string()
      iex> code = quote do
      ...>   @spec weight(symbolic_state(), command()) :: integer()
      ...>   @doc "\\nThis function returns the weight of a command defined in `Example`.\\n\\n"
      ...>   def weight(state, command) do
      ...>     model = Example
      ...>     command_module = Makina.Helpers.command_module_name(command, Example)
      ...>     command_module.weight(state)
      ...>   end
      ...> end |> Macro.to_string()
      iex> assert result =~ code

  """

  @doc_template ~S"""

  This function returns the weight of a command defined in `<%= name %>`.

  """
  @spec weight(atom()) :: Macro.t()
  def weight(context) do
    quote do
      @spec weight(symbolic_state(), command()) :: integer()
      @doc unquote(EEx.eval_string(@doc_template, name: inspect(context)))
      def weight(state, command) do
        model = unquote(context)
        command_module = Makina.Helpers.command_module_name(command, unquote(context))
        command_module.weight(state)
      end
    end
  end

  @doc """

  This function generates the function `weights/1` which returns the weight of each model command in
  a given state.

  ## Examples

      iex> result = weights() |> Macro.to_string()
      iex> code = quote do
      ...>   @spec weights(symbolic_state()) :: [{integer(), command()}]
      ...>   def weights(state) do
      ...>     for command <- commands() do
      ...>       {weight(state, command), command}
      ...>     end
      ...>   end
      ...> end |> Macro.to_string()
      iex> assert result =~ code

  """
  @spec weights() :: Macro.t()
  def weights() do
    quote do
      @spec weights(symbolic_state()) :: [{integer(), command()}]
      def weights(state) do
        for command <- commands(), do: {weight(state, command), command}
      end
    end
  end

  @doc """

  This function generates the function `command/1` of the model which is used to generate commands.

  ## Examples

      iex> command(Example) |> Macro.to_string()

  """

  @doc_template """

  This function generates a call to a command specified in `<%= name %>`.

  """
  @spec command(atom()) :: Macro.t()
  def command(context) do
    quote do
      @spec command(symbolic_state()) :: Makina.Types.generator(symbolic_call())
      @doc unquote(EEx.eval_string(@doc_template, name: context))
      def command(state) do
        valid_commands = Enum.filter(commands(), fn cmd -> precondition(state, cmd) end)

        weighted_commands =
          Enum.map(valid_commands, fn valid_cmd -> {weight(state, valid_cmd), valid_cmd} end)

        if weighted_commands != [] do
          let [cmd <- frequency(weighted_commands), args <- arguments(state, ^cmd)] do
            # NOTE: this is necessary due to poor support for maps in proper
            args =
              if is_map(args) do
                list = Map.to_list(args)
                let(result <- list, do: Map.new(result))
              else
                args
              end
              |> Enum.into(%{})

            module =
              __MODULE__
              |> Module.split()
              |> Enum.reverse()
              |> tl()
              |> Enum.reverse()
              |> Module.concat()
              |> then(&Makina.Helpers.command_module_name(cmd, &1))

            {:call, module, :call, [args]}
          end
        else
          # All commands have false preconditions; we provide a good error message
          # Possibly throw_error should also log the message to a logging "file",
          # for now we do it manually
          error_message =
            "all commands in model `#{inspect(unquote(context))}` have false preconditions"

          IO.puts("*** Error: " <> error_message)
          Makina.Error.throw_error(error_message)
        end
      end
    end
  end

  @doc """

  This function generates a private function to validate the generated args.

  ## Examples

      iex> result = validate_args(Example) |> Macro.to_string()
      iex> code = quote do
      ...>   defp validate_args(call) do
      ...>   model = Example
      ...>   command = call_command(call)
      ...>   command_module = Makina.Helpers.command_module_name(command, Example)
      ...>   args = call_args(call)
      ...>   command_module.validate_args(args)
      ...>   end
      ...> end |> Macro.to_string()
      iex> assert result =~ code

  """
  @spec validate_args(module()) :: Macro.t()
  def validate_args(context) do
    quote do
      @spec validate_args(symbolic_call()) :: {:ok, symbolic_arguments()}
      @spec validate_args(dynamic_call()) :: {:ok, dynamic_arguments()}
      defp validate_args(call) do
        model = unquote(context)
        command = call_command(call)
        command_module = Makina.Helpers.command_module_name(command, unquote(context))
        args = call_args(call)
        command_module.validate_args(args)
      end
    end
  end

  @doc """

  This function generates the transition function `next_state` of the model which returns the
  updates for a given command call.

  ## Examples

      iex> next_state(Example) |> Macro.to_string()

  """

  @doc_template """

  Computes the next state from the given call.

  """
  @spec next_state(atom()) :: Macro.t()
  def next_state(context) do
    quote do
      @spec next_state(symbolic_state(), symbolic_result(), symbolic_call()) :: symbolic_state()
      @spec next_state(dynamic_state(), dynamic_result() | symbolic_result(), dynamic_call()) ::
              dynamic_state()
      @doc unquote(EEx.eval_string(@doc_template))
      def next_state(state, result, call) do
        model = unquote(context)
        command = call_command(call)
        command_module = Makina.Helpers.command_module_name(command, unquote(context))

        with {:ok, args} <- validate_args(call),
             valid <- command_module.valid(state, args, result),
             valid = is_nil(valid) || valid,
             updates <- command_module.next(state, args, result, valid),
             updates = updates || [],
             {:ok, next} <- unquote(:"#{context}.State").update(state, updates),
             {:ok, validate} <- unquote(:"#{context}.State").validate(next) do
          next
        else
          {:error, message} ->
            env = command_module.__makina_info__().env

            """
            #{message}
            During state update in of command `#{command}` in model `#{inspect(model)}`.
            Please check the definition of `next` callback in `#{inspect(model)}.#{command}`.
            """
            |> Makina.Error.throw_error(env)
        end
      end
    end
  end

  @doc """

  This function generates the `postcondition` predicate of the model.

  ## Examples

      iex> postcondition(Example) |> Macro.to_string()

  """

  @doc_template ~S"""

  This function checks the postconditions defined in `<%= name %>` after the execution of a command.

  """

  @spec postcondition(atom()) :: Macro.t()
  def postcondition(context) do
    quote do
      @spec postcondition(dynamic_state(), dynamic_call(), dynamic_result()) :: boolean()
      @doc unquote(EEx.eval_string(@doc_template, name: context))
      def postcondition(state, call, result) do
        model = unquote(context)
        command = call_command(call)
        command_module = Makina.Helpers.command_module_name(command, unquote(context))

        with {:ok, args} <- validate_args(call),
             # TODO: THIS next_state MAY INTRODUCE SYMBOLIC VALUES!!!
             next_state <- next_state(state, result, call),
             {:ok, next_state} <- :"#{model}.State".validate(next_state),
             {:ok, next_state} <- :"#{model}.Invariants".check(next_state),
             valid <- command_module.valid(state, args, result),
             valid = is_nil(valid) || valid do
          post = command_module.post(state, args, result, valid)
          is_nil(post) || post
        else
          {:error, message} ->
            env = command_module.__makina_info__().env
            Makina.Error.throw_error(message, env)
        end
      end
    end
  end

  @doc """

  This function generates a private function `call_features` which calls the `features` of a
  model. This is an EQC exclusive function.

  ## Examples

      iex> result = call_features(Example) |> Macro.to_string()
      iex> code = quote do
      ...>   @spec call_features(dynamic_state(), dynamic_call(), dynamic_result()) :: [any()]
      ...>   @doc "\\nThis function returns a list of the features tested in the command.\\n\\n"
      ...>   def call_features(state, call, result) do
      ...>     model = Example
      ...>     command = call_command(call)
      ...>     command_module = Makina.Helpers.command_module_name(command, Example)
      ...>     args = call_args(call)
      ...>     command_module.features(state, args, result) || []
      ...>   end
      ...> end |> Macro.to_string()
      iex> assert result =~ code

  """

  @doc_template ~S"""

  This function returns a list of the features tested in the command.

  """
  @spec call_features(atom()) :: Macro.t()
  def call_features(context) do
    quote do
      @spec call_features(dynamic_state(), dynamic_call(), dynamic_result()) :: [any()]
      @doc unquote(EEx.eval_string(@doc_template))
      def call_features(state, call, result) do
        model = unquote(context)
        command = call_command(call)
        command_module = Makina.Helpers.command_module_name(command, unquote(context))
        args = call_args(call)
        command_module.features(state, args, result) || []
      end
    end
  end

  @doc """

  This function generates the helper function `call_command/1` which extracts the command name from
  a symbolic call.

  ## Examples

      iex> result = call_command() |> Macro.to_string()
      iex> code = quote do
      ...>   @spec call_command(command() | symbolic_call() | dynamic_call()) :: command()
      ...>   defp call_command({:call, m, _command, _args}) do
      ...>     Makina.Helpers.module_to_command_name(m)
      ...>   end
      ...>
      ...>   defp call_command({:call, m, _command, _args, _meta}) do
      ...>     Makina.Helpers.module_to_command_name(m)
      ...>   end
      ...>
      ...>   defp call_command(command) do
      ...>     command
      ...>   end
      ...> end |> Macro.to_string()
      iex> assert result =~ code

  """
  @spec call_command() :: Macro.t()
  def call_command() do
    quote do
      @spec call_command(command() | symbolic_call() | dynamic_call()) :: command()
      defp call_command({:call, m, _command, _args}) do
        Makina.Helpers.module_to_command_name(m)
      end

      defp call_command({:call, m, _command, _args, _meta}) do
        Makina.Helpers.module_to_command_name(m)
      end

      defp call_command(command), do: command
    end
  end

  @doc """

  This function generates a function `call_args/1` which extracts the arguments from a symbolic
  call.

  ## Examples

      iex> result = call_args() |> Macro.to_string()
      iex> code = quote do
      ...>   @spec call_args(symbolic_call()) :: symbolic_arguments()
      ...>   @spec call_args(dynamic_call()) :: dynamic_arguments()
      ...>   defp call_args({:call, _m, _command, [args]}) do
      ...>     args
      ...>   end
      ...>
      ...>   defp call_args({:call, _m, _command, [args], _meta}) do
      ...>     args
      ...>   end
      ...> end |> Macro.to_string()
      iex> assert result =~ code

  """
  @spec call_args() :: Macro.t()
  def call_args() do
    quote do
      @spec call_args(symbolic_call()) :: symbolic_arguments()
      @spec call_args(dynamic_call()) :: dynamic_arguments()
      defp call_args({:call, _m, _command, [args]}), do: args
      defp call_args({:call, _m, _command, [args], _meta}), do: args
    end
  end

  @doc """

  This function generates the types needed by the behaviour.

  ## Examples

      iex> result = types([:f, :g], Example) |> Macro.to_string()
      iex> code = quote do
      ...>   @type command() :: Example.Commands.command()
      ...>   @type symbolic_state() :: Example.State.symbolic_state()
      ...>   @type dynamic_state() :: Example.State.dynamic_state()
      ...>   @type symbolic_call() ::
      ...>           {:call, module(), :call, [symbolic_arguments()]}
      ...>           | {:call, module(), :call, [symbolic_arguments()], any()}
      ...>   @type dynamic_call() ::
      ...>           {:call, module(), :call, [dynamic_arguments()]}
      ...>           | {:call, module(), :call, [dynamic_arguments()], any()}
      ...>   @type symbolic_arguments :: Example.Command.G.symbolic_arguments() | Example.Command.F.symbolic_arguments()
      ...>   @type dynamic_arguments :: Example.Command.G.dynamic_arguments() | Example.Command.F.dynamic_arguments()
      ...>   @type symbolic_result :: Makina.Types.symbolic_var()
      ...>   @type dynamic_result :: Example.Command.G.dynamic_result() | Example.Command.F.dynamic_result()
      ...> end |> Macro.to_string()
      iex> assert result =~ code

  """
  @spec types([atom()], module()) :: Macro.t()
  def types(commands, context) do
    state_module = state_module_name(context)
    commands_module = commands_module_name(context)

    commands =
      commands
      |> Enum.map(fn c -> command_module_name(c, context) end)

    results =
      commands
      |> Enum.map(fn m -> quote do: unquote(m).dynamic_result() end)

    symbolic_arguments =
      commands
      |> Enum.map(fn m -> quote do: unquote(m).symbolic_arguments() end)
      |> then(&if &1 != [], do: union_type(&1), else: quote(do: [%{}]))

    dynamic_arguments =
      commands
      |> Enum.map(fn m -> quote do: unquote(m).dynamic_arguments() end)
      |> then(&if &1 != [], do: union_type(&1), else: quote(do: [%{}]))

    result_t = if results != [], do: union_type(results), else: quote(do: [any()])

    quote do
      @type command() :: unquote(commands_module).command()

      @type symbolic_state() :: unquote(state_module).symbolic_state()
      @type dynamic_state() :: unquote(state_module).dynamic_state()

      @type symbolic_call() ::
              {:call, module(), :call, [symbolic_arguments()]}
              | {:call, module(), :call, [symbolic_arguments()], any()}
      @type dynamic_call() ::
              {:call, module(), :call, [dynamic_arguments()]}
              | {:call, module(), :call, [dynamic_arguments()], any()}

      @type symbolic_arguments :: unquote_splicing(symbolic_arguments)
      @type dynamic_arguments :: unquote_splicing(dynamic_arguments)

      @type symbolic_result :: Makina.Types.symbolic_var()
      @type dynamic_result :: unquote_splicing(result_t)
    end
  end

  ##################################################################################################
  # AST Helpers
  ##################################################################################################

  @doc """

  This function generates a case pattern.

  ## Examples

      iex> result = generate_case(quote(do: x),  [f: quote do: f(x)], true) |> Macro.to_string()
      iex> code = quote do
      ...>   case x do
      ...>     :f -> f(x)
      ...>   end
      ...> end |> Macro.to_string()
      iex> assert result =~ code

  """
  @spec generate_case(Macro.t(), [{Macro.t(), Macro.t()}], any()) :: Macro.t()
  def generate_case(_, [], default), do: default

  def generate_case(case_arg, patterns, _default) do
    patterns =
      Enum.map(
        patterns,
        fn {l, r} ->
          quote do
            unquote(l) -> unquote(r)
          end
        end
      )

    quote do
      case unquote(case_arg) do
        unquote(patterns |> Enum.concat())
      end
    end
  end
end
