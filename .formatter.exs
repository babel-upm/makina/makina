locals_without_parens = [
  state: 1,
  state: 2,
  command: 1,
  command: 2,
  command: 3,
  invariants: 1,
  invariants: 2,
  pre: 1,
  args: 1,
  valid_args: 1,
  call: 1,
  next: 1,
  post: 1,
  weight: 1,
  features: 1,
  valid: 1
]

[
  inputs: [
    "{mix,.iex,.formatter,.credo}.exs",
    "{config,lib,test}/**/*.{ex,exs}"
  ],
  line_length: 100,
  locals_without_parens: locals_without_parens,
  export: [locals_without_parens: locals_without_parens]
]
