# Makina Changelog

## Legend

- `[+]` Added for new features
- `[-]` Removed for now removed features
- `[C]` Changed for changes in existing functionality
- `[F]` Fixed for any bug fixes
- `[O]` Obsolete for soon-to-be removed features
- `[T]` Technical change that doesn't affect the API (eg. refactoring, tooling, etc.)

## Next release

## 0.3.0

- `[+]` New Makina constructions:
  - Command
    - *abstract* commands.
    - *valid* (says if the generated call is successful).
- `[C]` Some changes in the original composition rules and callbacks:
  - Command
    - callbacks can now return `nil`.
  - Composition
    - extension always refines the base callbacks.
    - improved parallel composition rules.
- `[F]` Generated code now doesn't produce Dialyzer warnings.
- `[T]` Improved internal documentation.
- `[-]` Tracer removed, it is no longer needed.
- `[T]` Move tests to this repository.

## 0.2.2

- `[T]` Documentation improved.

## 0.2.1

- `[+]` Initial public release of Makina. Supported constructions:
  - State
    - *attribute names*
    - *initial values*
    - *types*
    - *invariants*
  - Command
    - *argument names*
    - *types*
    - *pre* (precondition)
    - *arg* (argument generators)
    - *valid args* (precondition used during shrinking)
    - *call* (the call to system under test)
    - *next* (calculates the next state of the model)
    - *post* (postcondition that compares the system under test result with the model)
  - Composition
    - Extension (*inheritance*)
    - Parallel composition
