defmodule HidingAndRenamingTest do
  use ExUnit.Case
  use TestHelpers

  import Makina.Command, only: [commands_info: 1]

  test "hiding and renaming" do
    assert_raise Makina.Error,
                 "`:where` error, missing `f` in `:\"#{inspect(__MODULE__)}.A hiding [f]\"`",
                 fn ->
                   defmodule A do
                     use Makina
                     command f(), do: call(:f)
                     command g(), do: call(:g)
                   end

                   defmodule B do
                     use Makina, extends: A, hiding: :f, where: [f: :g]
                   end
                 end
  end

  test "renaming and hiding" do
    defmodule A do
      use Makina
      command f(), do: call(:f)
      command g(), do: call(:g)
    end

    defmodule B do
      use Makina, extends: A, where: [f: :h], hiding: :f
    end

    assert {:commands_info, %{commands: cmds}} = commands_info(A)
    assert :f in cmds
    assert :g in cmds
    assert {:commands_info, %{commands: cmds}} = commands_info(B)
    assert :g in cmds
    assert :f not in cmds
  end

  test "multiple modules can use the same aliasing and hiding" do
    defmodule A do
      use Makina
      command f(), do: call(:ok)
    end

    fun = fn module ->
      receive do
        :start -> :ok
      end

      defmodule module do
        use Makina, extends: A, where: [f: :g], hiding: :f
      end
    end

    # starts compilation of C and D
    pid1 = spawn(fn -> fun.(C) end)
    pid2 = spawn(fn -> fun.(D) end)

    Process.link(pid1)
    Process.link(pid2)

    send(pid1, :start)
    send(pid2, :start)
  end
end
