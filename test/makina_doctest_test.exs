defmodule MakinaDoctestTest do
  use ExUnit.Case
  use TestHelpers
  import ExUnit.CaptureIO

  doctest Makina.Behaviour, import: true
  doctest Makina.Command, import: true
  doctest Makina.Command.Base, import: true
  doctest Makina.Command.Callback, import: true
  doctest Makina.Command.Defaults, import: true
  doctest Makina.Command.Extends, import: true
  doctest Makina.Command.ImplementedBy, import: true
  doctest Makina.Composition, import: true
  doctest Makina.Error, import: true
  doctest Makina.Exports, import: true
  doctest Makina.Helpers, import: true
  doctest Makina.Invariants, import: true
  doctest Makina.Import, import: true
  doctest Makina.State, import: true
  doctest Makina.State.Attribute, import: true
end
