defmodule CompositionTest do
  use ExUnit.Case
  use TestHelpers

  import Makina.Invariants, only: [invariants_info: 1]
  import Makina.State, only: [state_info: 1]
  import Makina.Command, only: [commands_info: 1, command_info: 1]

  describe "error messages" do
    test "model missing in composition" do
      assert_raise Makina.Error, "could not load `B`", fn ->
        defmodule A do
          use Makina
        end

        defmodule Wrong do
          use Makina, extends: [A, B]
        end
      end
    end

    test "self-extending model" do
      assert_raise Makina.Error, "could not load `CompositionTest.B`", fn ->
        defmodule A do
          use Makina
        end

        defmodule B do
          use Makina, extends: [A, B]
        end
      end
    end

    test "invariants cannot overlap" do
      assert_raise Makina.Error, "multiple declarations of invariants: `:inv`", fn ->
        defmodule A do
          use Makina
          invariants inv: true
        end

        defmodule B do
          use Makina
          invariants inv: true
        end

        defmodule C do
          use Makina, extends: [A, B]
        end
      end
    end

    test "overlapping commands must have the same arguments" do
      assert_raise Makina.Error, "different arguments in composition of command `f`", fn ->
        defmodule A do
          use Makina
          command f(arg1), do: call(A_f)
        end

        defmodule B do
          use Makina
          command f(arg2), do: call(B_g)
        end

        defmodule C do
          use Makina, extends: [A, B]
        end
      end
    end
  end

  test "composing empty machines" do
    defmodule A do
      use Makina
    end

    defmodule B do
      use Makina
    end

    defmodule C do
      use Makina, extends: [A, B]
    end

    assert {:state_info, %{attributes: [], module: _}} = state_info(C)
    assert {:invariants_info, %{invariants: [], module: _}} = invariants_info(C)
    assert {:commands_info, %{commands: []}} = commands_info(C)
  end

  describe "boolean values" do
    test "composing 2 non-overlapping machines" do
      defmodule A do
        use Makina
        state attrA: :attrA
        invariants invA: attrA == :attrA

        command f(arg_f) do
          pre attrA == :attrA
          args arg_f: :arg_f
          valid_args arg_f == :arg_f
          call A_f
          valid arg_f == :arg_f
          next attrA: :attrA
          post result == A_f && arg_f == :arg_f
        end
      end

      defmodule B do
        use Makina
        state attrB: :attrB
        invariants invB: attrB == :attrB

        command g(arg_g) do
          pre attrB == :attrB
          args arg_g: :arg_g
          valid_args arg_g == :arg_g
          call B_g
          valid arg_g == :arg_g
          next attrB: :attrB
          post result == B_g && arg_g == :arg_g
        end
      end

      defmodule C do
        use Makina, extends: [A, B]
      end

      assert {:state_info, %{attributes: attrs, module: state_module}} = state_info(C)
      assert :attrA in attrs
      assert :attrB in attrs
      assert %{attrA: :attrA, attrB: :attrB} = state_module.initial()

      assert {:invariants_info, %{invariants: invs, module: invariants_module}} = invariants_info(C)
      assert :invA in invs
      assert :invB in invs

      assert {:ok, %{attrA: :attrA, attrB: :attrB}} =
               invariants_module.check(state_module.initial())

      assert {:commands_info, %{commands: cmds}} = commands_info(C)
      assert :f in cmds
      assert :g in cmds

      assert {:command_info, cmds} = command_info(C)

      assert %{module: command_module, name: :f, callbacks: callbacks, arguments: _, result: _} =
               Enum.find(cmds, fn c -> c.name == :f end)

      assert :pre in callbacks
      assert :args in callbacks
      assert :weight in callbacks
      assert :valid_args in callbacks
      assert :valid in callbacks
      assert :call in callbacks
      assert :features in callbacks
      assert :next in callbacks
      assert :post in callbacks

      args = %{arg_f: :arg_f}
      result = A_f
      valid = true
      state = %{attrA: :attrA, attrB: :attrB}
      assert true == command_module.pre(state)
      assert [arg_f: :arg_f] = command_module.args(state)
      assert 1 = command_module.weight(state)
      assert true == command_module.valid_args(state, args)
      assert true == command_module.valid(state, args, result)
      assert A_f = command_module.call(args)
      assert [attrA: :attrA] = command_module.next(state, args, result, valid)
      assert true == command_module.post(state, args, result, valid)

      assert %{module: command_module, name: :g, callbacks: callbacks, arguments: _, result: _} =
               Enum.find(cmds, fn c -> c.name == :g end)

      assert :pre in callbacks
      assert :args in callbacks
      assert :weight in callbacks
      assert :valid_args in callbacks
      assert :valid in callbacks
      assert :call in callbacks
      assert :features in callbacks
      assert :next in callbacks
      assert :post in callbacks

      args = %{arg_g: :arg_g}
      result = B_g
      valid = true
      state = %{attrA: :attrA, attrB: :attrB}
      assert true == command_module.pre(state)
      assert [arg_g: :arg_g] = command_module.args(state)
      assert 1 = command_module.weight(state)
      assert true == command_module.valid_args(state, args)
      assert true == command_module.valid(state, args, result)
      assert B_g = command_module.call(args)
      assert [attrB: :attrB] = command_module.next(state, args, result, valid)
      assert true == command_module.post(state, args, result, valid)
    end

    test "composing 2 machines with overlapping state" do
      defmodule A do
        use Makina
        state attrA: :attrA, attr: :attrA
        invariants invA: attrA == :attrA

        command f(arg_f) do
          pre attrA == :attrA
          args arg_f: :arg_f
          valid_args arg_f == :arg_f
          call A_f
          valid arg_f == :arg_f
          next attrA: :attrA
          post result == A_f && arg_f == :arg_f
        end
      end

      defmodule B do
        use Makina
        state attrB: :attrB, attr: :attrB
        invariants invB: attrB == :attrB

        command g(arg_g) do
          pre attrB == :attrB
          args arg_g: :arg_g
          valid_args arg_g == :arg_g
          call B_g
          valid arg_g == :arg_g
          next attrB: :attrB
          post result == B_g && arg_g == :arg_g
        end
      end

      defmodule C do
        use Makina, extends: [A, B]
      end

      assert {:state_info, %{attributes: attrs, module: state_module}} = state_info(C)
      assert :attrA in attrs
      assert :attrB in attrs
      assert :attr in attrs
      assert %{attrA: :attrA, attrB: :attrB, attr: :attrA} = state_module.initial()

      assert {:invariants_info, %{invariants: invs, module: invariants_module}} = invariants_info(C)
      assert :invA in invs
      assert :invB in invs

      assert {:ok, %{attrA: :attrA, attrB: :attrB}} =
               invariants_module.check(state_module.initial())

      assert {:commands_info, %{commands: cmds}} = commands_info(C)
      assert :f in cmds
      assert :g in cmds

      assert {:command_info, cmds} = command_info(C)

      assert %{module: command_module, name: :f, callbacks: callbacks, arguments: _, result: _} =
               Enum.find(cmds, fn c -> c.name == :f end)

      assert :pre in callbacks
      assert :args in callbacks
      assert :weight in callbacks
      assert :valid_args in callbacks
      assert :valid in callbacks
      assert :call in callbacks
      assert :features in callbacks
      assert :next in callbacks
      assert :post in callbacks

      args = %{arg_f: :arg_f}
      result = A_f
      valid = true
      state = %{attrA: :attrA, attrB: :attrB, attr: :attrA}
      assert true == command_module.pre(state)
      # TODO: to check args we need a mock implementation of let
      assert [arg_f: :arg_f] = command_module.args(state)
      assert 1 = command_module.weight(state)
      assert true == command_module.valid_args(state, args)
      assert true == command_module.valid(state, args, result)
      assert A_f = command_module.call(args)
      assert [attrA: :attrA] = command_module.next(state, args, result, valid)
      assert true == command_module.post(state, args, result, valid)

      assert %{module: command_module, name: :g, callbacks: callbacks, arguments: _, result: _} =
               Enum.find(cmds, fn c -> c.name == :g end)

      assert :pre in callbacks
      assert :args in callbacks
      assert :weight in callbacks
      assert :valid_args in callbacks
      assert :valid in callbacks
      assert :call in callbacks
      assert :features in callbacks
      assert :next in callbacks
      assert :post in callbacks

      args = %{arg_g: :arg_g}
      result = B_g
      valid = true
      state = %{attrA: :attrA, attrB: :attrB, attr: :attrA}
      assert true == command_module.pre(state)
      assert [arg_g: :arg_g] = command_module.args(state)
      assert 1 = command_module.weight(state)
      assert true == command_module.valid_args(state, args)
      assert true == command_module.valid(state, args, result)
      assert B_g = command_module.call(args)
      assert [attrB: :attrB] = command_module.next(state, args, result, valid)
      assert true == command_module.post(state, args, result, valid)
    end

    test "composing 2 machines with overlapping state and commands" do
      defmodule A do
        use Makina
        state attrA: :attrA, attr: :attrA
        invariants invA: attrA == :attrA

        command f(arg_f) do
          pre attrA == :attrA
          args arg_f: :arg_f
          valid_args arg_f == :arg_f
          call A_f
          valid arg_f == :arg_f
          next attrA: :attrA
          post result == A_f && arg_f == :arg_f
        end

        command h(arg) do
          pre attrA == :attrA
          args arg: :arg_A
          valid_args arg == :arg_A || arg == :arg
          call A_h
          valid arg == :arg_A || arg == :arg
          next attrA: :attrA
          post result == A_h || valid
        end
      end

      defmodule B do
        use Makina
        state attrB: :attrB, attr: :attrB
        invariants invB: attrB == :attrB

        command g(arg_g) do
          pre attrB == :attrB
          args arg_g: :arg_g
          valid_args arg_g == :arg_g
          call B_g
          valid arg_g == :arg_g
          next attrB: :attrB
          post result == B_g && arg_g == :arg_g
        end

        command h(arg) do
          pre attrB == :attrB
          args arg: :arg_B
          valid_args arg == :arg_B || arg == :arg
          call B_h
          valid arg == :arg_B || arg == :arg
          next attrB: :attrB
          post (result == B_h && arg == :arg_B) || valid
        end
      end

      defmodule C do
        use Makina, extends: [A, B]
      end

      assert {:state_info, %{attributes: attrs, module: state_module}} = state_info(C)
      assert :attrA in attrs
      assert :attrB in attrs
      assert :attr in attrs
      assert %{attrA: :attrA, attrB: :attrB, attr: :attrA} = state_module.initial()

      assert {:invariants_info, %{invariants: invs, module: invariants_module}} = invariants_info(C)
      assert :invA in invs
      assert :invB in invs

      assert {:ok, %{attrA: :attrA, attrB: :attrB}} =
               invariants_module.check(state_module.initial())

      assert {:commands_info, %{commands: cmds}} = commands_info(C)
      assert :f in cmds
      assert :g in cmds

      assert {:command_info, cmds} = command_info(C)

      assert %{module: command_module, name: :f, callbacks: callbacks, arguments: _, result: _} =
               Enum.find(cmds, fn c -> c.name == :f end)

      assert :pre in callbacks
      assert :args in callbacks
      assert :weight in callbacks
      assert :valid_args in callbacks
      assert :valid in callbacks
      assert :call in callbacks
      assert :features in callbacks
      assert :next in callbacks
      assert :post in callbacks

      args = %{arg_f: :arg_f}
      result = A_f
      valid = true
      state = %{attrA: :attrA, attrB: :attrB, attr: :attrA}
      assert true == command_module.pre(state)
      assert [arg_f: :arg_f] = command_module.args(state)
      assert 1 = command_module.weight(state)
      assert true == command_module.valid_args(state, args)
      assert true == command_module.valid(state, args, result)
      assert A_f = command_module.call(args)
      assert [attrA: :attrA] = command_module.next(state, args, result, valid)
      assert true == command_module.post(state, args, result, valid)

      assert %{module: command_module, name: :g, callbacks: callbacks, arguments: _, result: _} =
               Enum.find(cmds, fn c -> c.name == :g end)

      assert :pre in callbacks
      assert :args in callbacks
      assert :weight in callbacks
      assert :valid_args in callbacks
      assert :valid in callbacks
      assert :call in callbacks
      assert :features in callbacks
      assert :next in callbacks
      assert :post in callbacks

      args = %{arg_g: :arg_g}
      result = B_g
      valid = true
      state = %{attrA: :attrA, attrB: :attrB, attr: :attrA}
      assert true == command_module.pre(state)
      assert [arg_g: :arg_g] = command_module.args(state)
      assert 1 = command_module.weight(state)
      assert true == command_module.valid_args(state, args)
      assert true == command_module.valid(state, args, result)
      assert B_g = command_module.call(args)
      assert [attrB: :attrB] = command_module.next(state, args, result, valid)
      assert true == command_module.post(state, args, result, valid)

      assert %{module: command_module, name: :h, callbacks: callbacks, arguments: _, result: _} =
               Enum.find(cmds, fn c -> c.name == :h end)

      assert :pre in callbacks
      assert :args in callbacks
      assert :weight in callbacks
      assert :valid_args in callbacks
      assert :valid in callbacks
      assert :call in callbacks
      assert :features in callbacks
      assert :next in callbacks
      assert :post in callbacks

      # pre is the disjunction between the composing models
      assert true == command_module.pre(%{attrA: :attrA, attrB: :attrB, attr: :attr})
      assert true == command_module.pre(%{attrA: :attr, attrB: :attrB, attr: :attr})
      assert true == command_module.pre(%{attrA: :attrA, attrB: :attr, attr: :attr})
      assert false == command_module.pre(%{attrA: :attr, attrB: :attr, attr: :attr})
      # args can be any args of the enabled commands
      assert [arg: arg] = command_module.args(%{attrA: :attrA, attrB: :attrB, attr: :attr})
      assert arg == :arg_A || arg == :arg_B
      assert [arg: :arg_B] = command_module.args(%{attrA: :attrB, attrB: :attrB, attr: :attr})
      assert [arg: :arg_A] = command_module.args(%{attrA: :attrA, attrB: :attrA, attr: :attr})
      # valid_args is the conjunction of the composing models
      assert false == command_module.valid_args(state, %{arg: :arg_A})
      assert false == command_module.valid_args(state, %{arg: :arg_B})
      assert true == command_module.valid_args(state, %{arg: :arg})
      # valid is the conjunction of the composing models
      assert false == command_module.valid(state, %{arg: :arg_A}, result)
      assert false == command_module.valid(state, %{arg: :arg_B}, result)
      assert true == command_module.valid(state, %{arg: :arg}, result)
      # call makes a random choice
      assert result = command_module.call(%{arg: :arg})
      assert result == A_h || result == B_h
      # next performs the updates in all composing models
      assert [attrA: :attrA, attrB: :attrB] = command_module.next(state, %{arg: :arg}, :ok, true)
      # post is the conjunction of the composing models
      assert false == command_module.post(state, %{arg: :arg}, A_h, false)
      assert false == command_module.post(state, %{arg: :arg}, B_h, false)
      assert true == command_module.post(state, %{arg: :arg}, :ok, true)
    end
  end

  describe "nil-values" do
    test "composing 2 non-overlapping machines" do
      defmodule A do
        use Makina
        state attrA: :attrA
        invariants invA: if(attrA != :attrA, do: false)

        command f(arg_f) do
          pre if attrA != :attrA, do: false
          args arg_f: :arg_f
          valid_args if arg_f != :arg_f, do: false
          call A_f
          valid if arg_f != :arg_f, do: false
          next if arg_f == :arg_f, do: [attrA: :attrA]
          post if result != A_f, do: false
        end
      end

      defmodule B do
        use Makina
        state attrB: :attrB
        invariants invB: if(attrB != :attrB, do: false)

        command g(arg_g) do
          pre if attrB != :attrB, do: false
          args arg_g: :arg_g
          valid_args if arg_g != :arg_g, do: false
          call B_g
          valid if arg_g != :arg_g, do: false
          next if arg_g == :arg_g, do: [attrB: :attrB]
          post if result != B_g, do: false
        end
      end

      defmodule C do
        use Makina, extends: [A, B]
      end

      assert {:state_info, %{attributes: attrs, module: state_module}} = state_info(C)
      assert :attrA in attrs
      assert :attrB in attrs
      assert %{attrA: :attrA, attrB: :attrB} = state_module.initial()

      assert {:invariants_info, %{invariants: invs, module: invariants_module}} = invariants_info(C)
      assert :invA in invs
      assert :invB in invs

      assert {:ok, %{attrA: :attrA, attrB: :attrB}} =
               invariants_module.check(state_module.initial())

      assert {:commands_info, %{commands: cmds}} = commands_info(C)
      assert :f in cmds
      assert :g in cmds

      assert {:command_info, cmds} = command_info(C)

      assert %{module: command_module, name: :f, callbacks: callbacks, arguments: _, result: _} =
               Enum.find(cmds, fn c -> c.name == :f end)

      assert :pre in callbacks
      assert :args in callbacks
      assert :weight in callbacks
      assert :valid_args in callbacks
      assert :valid in callbacks
      assert :call in callbacks
      assert :features in callbacks
      assert :next in callbacks
      assert :post in callbacks

      args = %{arg_f: :arg_f}
      result = A_f
      valid = true
      state = %{attrA: :attrA, attrB: :attrB}
      assert true == command_module.pre(state)
      assert [arg_f: :arg_f] = command_module.args(state)
      assert 1 = command_module.weight(state)
      assert true == command_module.valid_args(state, args)
      assert true == command_module.valid(state, args, result)
      assert A_f = command_module.call(args)
      assert [attrA: :attrA] = command_module.next(state, args, result, valid)
      assert true == command_module.post(state, args, result, valid)

      assert %{module: command_module, name: :g, callbacks: callbacks, arguments: _, result: _} =
               Enum.find(cmds, fn c -> c.name == :g end)

      assert :pre in callbacks
      assert :args in callbacks
      assert :weight in callbacks
      assert :valid_args in callbacks
      assert :valid in callbacks
      assert :call in callbacks
      assert :features in callbacks
      assert :next in callbacks
      assert :post in callbacks

      args = %{arg_g: :arg_g}
      result = B_g
      valid = true
      state = %{attrA: :attrA, attrB: :attrB}
      assert true == command_module.pre(state)
      assert [arg_g: :arg_g] = command_module.args(state)
      assert 1 = command_module.weight(state)
      assert true == command_module.valid_args(state, args)
      assert true == command_module.valid(state, args, result)
      assert B_g = command_module.call(args)
      assert [attrB: :attrB] = command_module.next(state, args, result, valid)
      assert true == command_module.post(state, args, result, valid)
    end

    test "composing 2 machines with overlapping state" do
      defmodule A do
        use Makina
        state attrA: :attrA, attr: :attrA
        invariants invA: if(attrA != :attrA, do: false)

        command f(arg_f) do
          pre if attrA != :attrA, do: false
          args arg_f: :arg_f
          valid_args if arg_f != :arg_f, do: false
          call A_f
          valid if arg_f != :arg_f, do: false
          next if arg_f == :arg_f, do: [attrA: :attrA]
          post if result != A_f, do: false
        end

        command h(arg) do
          pre if attrA != :attrA, do: false
          args arg: :arg_A
          valid_args if arg != :arg_A && arg != :arg, do: false
          call A_h
          valid if arg != :arg_A && arg == :arg, do: false
          next attrA: :attrA
          post if result != A_h && valid, do: false
        end
      end

      defmodule B do
        use Makina
        state attrB: :attrB, attr: :attrB
        invariants invB: if(attrB != :attrB, do: false)

        command g(arg_g) do
          pre if attrB != :attrB, do: false
          args arg_g: :arg_g
          valid_args if arg_g != :arg_g, do: false
          call B_g
          valid if arg_g != :arg_g, do: false
          next if arg_g == :arg_g, do: [attrB: :attrB]
          post if result != B_g, do: false
        end

        command h(arg) do
          pre if attrB != :attrB, do: false
          args arg: :arg_B
          valid_args if !(arg == :arg_B || arg == :arg), do: false
          call B_h
          valid if !(arg == :arg_B || arg == :arg), do: false
          next attrB: :attrB
          post if !((result == B_h && arg == :arg_B) || valid), do: false
        end
      end

      defmodule C do
        use Makina, extends: [A, B]
      end

      assert {:state_info, %{attributes: attrs, module: state_module}} = state_info(C)
      assert :attrA in attrs
      assert :attrB in attrs
      assert :attr in attrs
      assert %{attrA: :attrA, attrB: :attrB, attr: :attrA} = state_module.initial()

      assert {:invariants_info, %{invariants: invs, module: invariants_module}} = invariants_info(C)
      assert :invA in invs
      assert :invB in invs

      assert {:ok, %{attrA: :attrA, attrB: :attrB}} =
               invariants_module.check(state_module.initial())

      assert {:commands_info, %{commands: cmds}} = commands_info(C)
      assert :f in cmds
      assert :g in cmds

      assert {:command_info, cmds} = command_info(C)

      assert %{module: command_module, name: :f, callbacks: callbacks, arguments: _, result: _} =
               Enum.find(cmds, fn c -> c.name == :f end)

      assert :pre in callbacks
      assert :args in callbacks
      assert :weight in callbacks
      assert :valid_args in callbacks
      assert :valid in callbacks
      assert :call in callbacks
      assert :features in callbacks
      assert :next in callbacks
      assert :post in callbacks

      args = %{arg_f: :arg_f}
      result = A_f
      valid = true
      state = %{attrA: :attrA, attrB: :attrB, attr: :attrA}
      assert true == command_module.pre(state)
      assert [arg_f: :arg_f] = command_module.args(state)
      assert 1 = command_module.weight(state)
      assert true == command_module.valid_args(state, args)
      assert true == command_module.valid(state, args, result)
      assert A_f = command_module.call(args)
      assert [attrA: :attrA] = command_module.next(state, args, result, valid)
      assert true == command_module.post(state, args, result, valid)

      assert %{module: command_module, name: :g, callbacks: callbacks, arguments: _, result: _} =
               Enum.find(cmds, fn c -> c.name == :g end)

      assert :pre in callbacks
      assert :args in callbacks
      assert :weight in callbacks
      assert :valid_args in callbacks
      assert :valid in callbacks
      assert :call in callbacks
      assert :features in callbacks
      assert :next in callbacks
      assert :post in callbacks

      args = %{arg_g: :arg_g}
      result = B_g
      valid = true
      state = %{attrA: :attrA, attrB: :attrB, attr: :attrA}
      assert true == command_module.pre(state)
      assert [arg_g: :arg_g] = command_module.args(state)
      assert 1 = command_module.weight(state)
      assert true == command_module.valid_args(state, args)
      assert true == command_module.valid(state, args, result)
      assert B_g = command_module.call(args)
      assert [attrB: :attrB] = command_module.next(state, args, result, valid)
      assert true == command_module.post(state, args, result, valid)
    end

    test "composing 2 machines with overlapping state and commands" do
      defmodule A do
        use Makina
        state attrA: :attrA, attr: :attrA
        invariants invA: if(attrA != :attrA, do: false)

        command f(arg_f) do
          pre if attrA != :attrA, do: false
          args arg_f: :arg_f
          valid_args if arg_f != :arg_f, do: false
          call A_f
          valid if arg_f != :arg_f, do: false
          next if arg_f == :arg_f, do: [attrA: :attrA]
          post if result != A_f, do: false
        end

        command h(arg) do
          pre if attrA != :attrA, do: false
          args arg: :arg_A
          valid_args if !(arg == :arg_A || arg == :arg), do: false
          call A_h
          valid if !(arg == :arg_A || arg == :arg), do: false
          next attrA: :attrA
          post if !(result == A_h || valid), do: false
        end
      end

      defmodule B do
        use Makina
        state attrB: :attrB, attr: :attrB
        invariants invB: if(attrB != :attrB, do: false)

        command g(arg_g) do
          pre if attrB != :attrB, do: false
          args arg_g: :arg_g
          valid_args if arg_g != :arg_g, do: false
          call B_g
          valid if arg_g != :arg_g, do: false
          next if arg_g == :arg_g, do: [attrB: :attrB]
          post if result != B_g, do: false
        end

        command h(arg) do
          pre if attrB != :attrB, do: false
          args arg: :arg_B
          valid_args if !(arg == :arg_B || arg == :arg), do: false
          call B_h
          valid if !(arg == :arg_B || arg == :arg), do: false
          next attrB: :attrB
          post if !((result == B_h && arg == :arg_B) || valid), do: false
        end
      end

      defmodule C do
        use Makina, extends: [A, B]
      end

      assert {:state_info, %{attributes: attrs, module: state_module}} = state_info(C)
      assert :attrA in attrs
      assert :attrB in attrs
      assert :attr in attrs
      assert %{attrA: :attrA, attrB: :attrB, attr: :attrA} = state_module.initial()

      assert {:invariants_info, %{invariants: invs, module: invariants_module}} = invariants_info(C)
      assert :invA in invs
      assert :invB in invs

      assert {:ok, %{attrA: :attrA, attrB: :attrB}} =
               invariants_module.check(state_module.initial())

      assert {:commands_info, %{commands: cmds}} = commands_info(C)
      assert :f in cmds
      assert :g in cmds

      assert {:command_info, cmds} = command_info(C)

      assert %{module: command_module, name: :f, callbacks: callbacks, arguments: _, result: _} =
               Enum.find(cmds, fn c -> c.name == :f end)

      assert :pre in callbacks
      assert :args in callbacks
      assert :weight in callbacks
      assert :valid_args in callbacks
      assert :valid in callbacks
      assert :call in callbacks
      assert :features in callbacks
      assert :next in callbacks
      assert :post in callbacks

      args = %{arg_f: :arg_f}
      result = A_f
      valid = true
      state = %{attrA: :attrA, attrB: :attrB, attr: :attr}
      assert true == command_module.pre(state)
      assert [arg_f: :arg_f] = command_module.args(state)
      assert 1 = command_module.weight(state)
      assert true == command_module.valid_args(state, args)
      assert true == command_module.valid(state, args, result)
      assert A_f = command_module.call(args)
      assert [attrA: :attrA] = command_module.next(state, args, result, valid)
      assert true == command_module.post(state, args, result, valid)

      assert %{module: command_module, name: :g, callbacks: callbacks, arguments: _, result: _} =
               Enum.find(cmds, fn c -> c.name == :g end)

      assert :pre in callbacks
      assert :args in callbacks
      assert :weight in callbacks
      assert :valid_args in callbacks
      assert :valid in callbacks
      assert :call in callbacks
      assert :features in callbacks
      assert :next in callbacks
      assert :post in callbacks

      args = %{arg_g: :arg_g}
      result = B_g
      valid = true
      state = %{attrA: :attrA, attrB: :attrB, attr: :attrA}
      assert true == command_module.pre(state)
      assert [arg_g: :arg_g] = command_module.args(state)
      assert 1 = command_module.weight(state)
      assert true == command_module.valid_args(state, args)
      assert true == command_module.valid(state, args, result)
      assert B_g = command_module.call(args)
      assert [attrB: :attrB] = command_module.next(state, args, result, valid)
      assert true == command_module.post(state, args, result, valid)

      assert %{module: command_module, name: :h, callbacks: callbacks, arguments: _, result: _} =
               Enum.find(cmds, fn c -> c.name == :h end)

      assert :pre in callbacks
      assert :args in callbacks
      assert :weight in callbacks
      assert :valid_args in callbacks
      assert :valid in callbacks
      assert :call in callbacks
      assert :features in callbacks
      assert :next in callbacks
      assert :post in callbacks

      # pre is the disjunction between the composing models
      assert true == command_module.pre(%{attrA: :attrA, attrB: :attrB, attr: :attr})
      assert true == command_module.pre(%{attrA: :attr, attrB: :attrB, attr: :attr})
      assert true == command_module.pre(%{attrA: :attrA, attrB: :attr, attr: :attr})
      assert false == command_module.pre(%{attrA: :attr, attrB: :attr, attr: :attr})
      # args can be any args of the enabled commands
      assert [arg: arg] = command_module.args(%{attrA: :attrA, attrB: :attrB, attr: :attr})
      assert arg == :arg_A || arg == :arg_B
      assert [arg: :arg_B] = command_module.args(%{attrA: :attrB, attrB: :attrB, attr: :attr})
      assert [arg: :arg_A] = command_module.args(%{attrA: :attrA, attrB: :attrA, attr: :attr})
      # valid_args is the conjunction of the composing models
      assert false == command_module.valid_args(state, %{arg: :arg_A})
      assert false == command_module.valid_args(state, %{arg: :arg_B})
      assert true == command_module.valid_args(state, %{arg: :arg})
      # valid is the conjunction of the composing models
      assert false == command_module.valid(state, %{arg: :arg_A}, result)
      assert false == command_module.valid(state, %{arg: :arg_B}, result)
      assert true == command_module.valid(state, %{arg: :arg}, result)
      # call makes a random choice
      assert result = command_module.call(%{arg: :arg})
      assert result == A_h || result == B_h
      # next performs the updates in all composing models
      assert [attrA: :attrA, attrB: :attrB] = command_module.next(state, %{arg: :arg}, :ok, true)
      # post is the conjunction of the composing models
      assert false == command_module.post(state, %{arg: :arg}, A_h, false)
      assert false == command_module.post(state, %{arg: :arg}, B_h, false)
      assert true == command_module.post(state, %{arg: :arg}, :ok, true)
    end
  end

  test "multiple modules can use the same composition" do
    defmodule A do
      use Makina
    end

    defmodule B do
      use Makina
    end

    fun = fn module ->
      receive do
        :start -> :ok
      end

      defmodule module do
        use Makina, extends: [A, B]
      end
    end

    # starts compilation of C and D
    pid1 = spawn(fn -> fun.(C) end)
    pid2 = spawn(fn -> fun.(D) end)

    Process.link(pid1)
    Process.link(pid2)

    send(pid1, :start)
    send(pid2, :start)
  end
end
