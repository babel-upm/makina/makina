defmodule SymbolicTest do
  use ExUnit.Case
  use TestHelpers

  import Makina.Command, only: [command_info: 1]

  test "next with symbolic expressions that do not refer to result should compile" do
    defmodule A do
      use Makina

      state attr: 0

      command f() do
        call :ok
        next attr: symbolic(Kernel.elem(result, 1))
      end
    end

    assert {:command_info, [f_info]} = command_info(A)

    assert %{module: command_module, name: :f, callbacks: _, arguments: _, result: _} =
             f_info

    assert [attr: {:call, Kernel, :elem, [{:var, 1}, 1]}] =
             command_module.next(A.initial_state(), %{}, {:var, 1}, true)

    assert [attr: "error"] = command_module.next(A.initial_state(), %{}, {:error, "error"}, true)
  end
end
