defmodule ExtendsTest do
  use ExUnit.Case
  use TestHelpers

  import Makina.Invariants, only: [invariants_info: 1]
  import Makina.State, only: [state_info: 1]
  import Makina.Command, only: [commands_info: 1, command_info: 1]

  describe "error messages" do
    test "missing extending module" do
      assert_raise Makina.Error, "could not load `A`", fn ->
        defmodule Wrong do
          use Makina, extends: A
        end
      end
    end

    test "self-extending module" do
      assert_raise Makina.Error, "`ExtendsTest.Wrong` cannot extend itself", fn ->
        defmodule Wrong do
          use Makina, extends: Wrong
        end
      end
    end

    test "mutual extension" do
      assert_raise Makina.Error, "could not load `B`", fn ->
        defmodule A do
          use Makina, extends: B
        end

        defmodule B do
          use Makina, extends: A
        end
      end
    end

    test "cycle" do
      assert_raise Makina.Error, "could not load `C`", fn ->
        defmodule A do
          use Makina, extends: C
        end

        defmodule B do
          use Makina, extends: A
        end

        defmodule C do
          use Makina, extends: B
        end
      end
    end

    test "invariants cannot be overriden" do
      assert_raise Makina.Error, "multiple declarations of invariants: `:prop1`", fn ->
        defmodule A do
          use Makina
          invariants prop1: true
        end

        defmodule B do
          use Makina, extends: A
          invariants prop1: false
        end
      end
    end
  end

  describe "boolean values" do
    test "empty base; empty extending model" do
      defmodule A do
        use Makina
      end

      defmodule B do
        use Makina, extends: A
      end
    end

    test "non-empty base model; empty extending model" do
      defmodule A do
        use Makina
        state attr1: 0
        invariants prop1: true

        command f(arg1) do
          pre false
          args arg1: A_f
          valid_args false
          call A_f
          next attr1: 1
          post false
          valid false
          features [A_f]
          weight 10
        end
      end

      defmodule B do
        use Makina, extends: A
      end

      assert {:state_info, %{attributes: [:attr1], module: state_module}} = state_info(B)
      assert (state = %{attr1: 0}) = state_module.initial()

      assert {:invariants_info, %{invariants: [:prop1], module: invariants_module}} =
               invariants_info(B)

      assert {:ok, ^state} = invariants_module.check(state)

      assert {:commands_info, %{commands: [:f]}} = commands_info(B)
      assert {:command_info, [f_info]} = command_info(B)

      assert %{module: command_module, name: :f, callbacks: callbacks, arguments: _, result: _} =
               f_info

      assert :pre in callbacks
      assert :args in callbacks
      assert :weight in callbacks
      assert :valid_args in callbacks
      assert :valid in callbacks
      assert :call in callbacks
      assert :features in callbacks
      assert :next in callbacks
      assert :post in callbacks

      args = %{arg1: 0}
      result = :ok
      valid = true
      assert false == command_module.pre(state)
      assert [arg1: A_f] = command_module.args(state)
      assert 10 = command_module.weight(state)
      assert false == command_module.valid_args(state, args)
      assert false == command_module.valid(state, args, result)
      assert A_f = command_module.call(args)
      assert [A_f] = command_module.features(state, args, result)
      assert [attr1: 1] = command_module.next(state, args, result, valid)
      assert false == command_module.post(state, args, result, valid)
    end

    test "non-empty base model; non-empty extending model; non-overlapping commands, state or invariants." do
      defmodule A do
        use Makina
        state attr1: 0
        invariants prop1: true

        command f(arg1) do
          pre attr1 == 1
          args arg1: 1
          valid_args arg1 == 1
          call A_f
          next attr1: 1
          post arg1 == 1
          valid arg1 == 1
          features [A_f]
          weight 2
        end
      end

      defmodule B do
        use Makina, extends: A
        state attr2: 0
        invariants prop2: false

        command g(arg2) do
          pre attr2 == 1
          args arg2: 1
          valid_args arg2 == 1
          call B_g
          next attr2: 1
          post arg2 == 1
          valid arg2 == 1
          features [B_g]
          weight 2
        end
      end

      assert {:state_info, %{attributes: attrs, module: state_module}} = state_info(B)
      assert :attr1 in attrs
      assert :attr2 in attrs
      assert (state = %{attr1: 0, attr2: 0}) = state_module.initial()

      assert {:invariants_info, %{invariants: props, module: invariants_module}} =
               invariants_info(B)

      assert :prop1 in props
      assert :prop2 in props

      assert {:error, "failed to check the invariants: `:prop2`"} = invariants_module.check(state)

      assert {:commands_info, %{commands: cmds}} = commands_info(B)
      assert :f in cmds
      assert :g in cmds
      assert {:command_info, [f_info = %{name: :f}, g_info = %{name: :g}]} = command_info(B)

      assert %{module: command_module, name: :f, callbacks: callbacks, arguments: _, result: _} =
               f_info

      assert :pre in callbacks
      assert :args in callbacks
      assert :weight in callbacks
      assert :valid_args in callbacks
      assert :valid in callbacks
      assert :call in callbacks
      assert :features in callbacks
      assert :next in callbacks
      assert :post in callbacks

      args = %{arg1: 0}
      result = :ok
      valid = true
      assert false == command_module.pre(state)
      assert [arg1: 1] = command_module.args(state)
      assert 2 = command_module.weight(state)
      assert false == command_module.valid_args(state, args)
      assert false == command_module.valid(state, args, result)
      assert A_f = command_module.call(args)
      assert [A_f] = command_module.features(state, args, result)
      assert [attr1: 1] = command_module.next(state, args, result, valid)
      assert false == command_module.post(state, args, result, valid)

      assert %{module: command_module, name: :g, callbacks: callbacks, arguments: _, result: _} =
               g_info

      assert :pre in callbacks
      assert :args in callbacks
      assert :weight in callbacks
      assert :valid_args in callbacks
      assert :valid in callbacks
      assert :call in callbacks
      assert :features in callbacks
      assert :next in callbacks
      assert :post in callbacks

      args = %{arg2: 0}
      result = :ok
      valid = true
      assert false == command_module.pre(state)
      assert [arg2: 1] = command_module.args(state)
      assert 2 = command_module.weight(state)
      assert false == command_module.valid_args(state, args)
      assert false == command_module.valid(state, args, result)
      assert B_g = command_module.call(args)
      assert [B_g] = command_module.features(state, args, result)
      assert [attr2: 1] = command_module.next(state, args, result, valid)
      assert false == command_module.post(state, args, result, valid)
    end

    test "non-empty base model; non-empty extending model; overlapping commands." do
      defmodule A do
        use Makina
        state attr1: 0
        invariants prop1: true

        command f(arg1) do
          pre attr1 == 1
          args arg1: 1
          valid_args arg1 == 1
          call A_f
          next attr1: 1
          post arg1 == 1
          valid arg1 == 1
          features [A_f]
          weight 1
        end
      end

      defmodule B do
        use Makina, extends: A
        state attr2: 1
        invariants prop2: false

        # TODO: THIS GENERATES A WARNING AND IT SHOULD NOT: variable "state" is unused (if the variable is not meant to be used, prefix it with an underscore)
        command f(arg2) do
          pre attr2 == 1
          args arg2: 1
          valid_args arg2 == 1
          call B_f
          next attr2: 1
          post arg2 == 1
          valid arg2 == 1
          features [B_f]
          weight 2
        end
      end

      assert {:state_info, %{attributes: attrs, module: state_module}} = state_info(B)
      assert :attr1 in attrs
      assert :attr2 in attrs
      assert (state = %{attr1: 0, attr2: 1}) = state_module.initial()

      assert {:invariants_info, %{invariants: props, module: invariants_module}} =
               invariants_info(B)

      assert :prop1 in props
      assert :prop2 in props

      assert {:error, "failed to check the invariants: `:prop2`"} = invariants_module.check(state)

      assert {:commands_info, %{commands: cmds}} = commands_info(B)
      assert :f in cmds
      assert {:command_info, [f_info = %{name: :f}]} = command_info(B)

      assert %{module: command_module, name: :f, callbacks: callbacks, arguments: _, result: _} =
               f_info

      assert :pre in callbacks
      assert :args in callbacks
      assert :weight in callbacks
      assert :valid_args in callbacks
      assert :valid in callbacks
      assert :call in callbacks
      assert :features in callbacks
      assert :next in callbacks
      assert :post in callbacks

      args = %{arg1: 0, arg2: 1}
      result = :ok
      valid = true
      # both pres are true
      assert true == command_module.pre(%{attr1: 1, attr2: 1})
      # the base pre is false
      assert false == command_module.pre(%{attr1: 0, attr2: 1})
      # the extending pre is false
      assert false == command_module.pre(%{attr1: 0, attr2: 1})
      # both pres are false
      assert false == command_module.pre(%{attr1: 0, attr2: 0})

      # TODO: check precedence
      assert [arg1: 1, arg2: 1] = command_module.args(state)

      # weight is the one in the extending model
      assert 2 = command_module.weight(state)

      # both valid args are true
      assert true == command_module.valid_args(%{attr1: 1, attr2: 1}, %{arg1: 1, arg2: 1})
      # base valid_args is false
      assert false == command_module.valid_args(%{attr1: 1, attr2: 1}, %{arg1: 0, arg2: 1})
      # extending valid_args is false
      assert false == command_module.valid_args(%{attr1: 1, attr2: 1}, %{arg1: 1, arg2: 0})
      # both valid_args are false
      assert false == command_module.valid_args(%{attr1: 1, attr2: 1}, %{arg1: 0, arg2: 0})

      # call is the one in the extending model
      assert B_f = command_module.call(args)

      # both valid are true
      assert true == command_module.valid(%{attr1: 1, attr2: 1}, %{arg1: 1, arg2: 1}, result)
      # base valid is false
      assert false == command_module.valid(%{attr1: 1, attr2: 1}, %{arg1: 0, arg2: 1}, result)
      # extending valid is false
      assert false == command_module.valid(%{attr1: 1, attr2: 1}, %{arg1: 1, arg2: 0}, result)
      # both valid are false
      assert false == command_module.valid(%{attr1: 1, attr2: 1}, %{arg1: 0, arg2: 0}, result)

      # features are overriden
      assert [B_f] = command_module.features(state, args, result)

      # next applies the updates both models in order
      assert [attr1: 1, attr2: 1] = command_module.next(state, args, result, valid)

      # both post are true
      assert true == command_module.post(%{attr1: 1, attr2: 1}, %{arg1: 1, arg2: 1}, :ok, true)
      # base post is false
      assert false == command_module.post(%{attr1: 1, attr2: 1}, %{arg1: 0, arg2: 1}, :ok, true)
      # extending post is false
      assert false == command_module.post(%{attr1: 1, attr2: 1}, %{arg1: 1, arg2: 0}, :ok, true)
      # both post are false
      assert false == command_module.post(%{attr1: 1, attr2: 1}, %{arg1: 0, arg2: 0}, :ok, true)
    end

    test "non-empty base model; non-empty extending model; overlapping commands and state." do
      defmodule A do
        use Makina
        state attr1: 0
        invariants prop1: true

        command f(arg1) do
          pre attr1 == 1
          args arg1: 1
          valid_args arg1 == 1
          call A_f
          next attr1: 1
          post arg1 == 1
          valid arg1 == 1
          features [A_f]
          weight 1
        end
      end

      defmodule B do
        use Makina, extends: A
        state attr1: 1
        invariants prop2: false

        # TODO: This generates a warning when it should not. It seems this warning is only triggered in the tests.
        command f(arg2) do
          pre attr1 == 1
          args arg2: 1
          valid_args arg2 == 1
          call B_f
          next attr2: 1
          post arg2 == 1
          valid arg2 == 1
          features [B_f]
          weight 2
        end
      end

      assert {:state_info, %{attributes: attrs, module: state_module}} = state_info(B)
      assert :attr1 in attrs
      assert (state = %{attr1: 1}) = state_module.initial()

      assert {:invariants_info, %{invariants: props, module: invariants_module}} =
               invariants_info(B)

      assert :prop1 in props
      assert :prop2 in props

      assert {:error, "failed to check the invariants: `:prop2`"} = invariants_module.check(state)

      assert {:commands_info, %{commands: cmds}} = commands_info(B)
      assert :f in cmds
      assert {:command_info, [f_info = %{name: :f}]} = command_info(B)

      assert %{module: command_module, name: :f, callbacks: callbacks, arguments: _, result: _} =
               f_info

      assert :pre in callbacks
      assert :args in callbacks
      assert :weight in callbacks
      assert :valid_args in callbacks
      assert :valid in callbacks
      assert :call in callbacks
      assert :features in callbacks
      assert :next in callbacks
      assert :post in callbacks

      args = %{arg1: 0, arg2: 1}
      result = :ok
      valid = true
      # both pres are true
      assert true == command_module.pre(%{attr1: 1, attr2: 1})
      # the base pre is false
      assert false == command_module.pre(%{attr1: 0, attr2: 1})
      # the extending pre is false
      assert false == command_module.pre(%{attr1: 0, attr2: 1})
      # both pres are false
      assert false == command_module.pre(%{attr1: 0, attr2: 0})

      # TODO: check precedence
      assert [arg1: 1, arg2: 1] = command_module.args(state)

      # weight is the one in the extending model
      assert 2 = command_module.weight(state)

      # both valid args are true
      assert true == command_module.valid_args(%{attr1: 1, attr2: 1}, %{arg1: 1, arg2: 1})
      # base valid_args is false
      assert false == command_module.valid_args(%{attr1: 1, attr2: 1}, %{arg1: 0, arg2: 1})
      # extending valid_args is false
      assert false == command_module.valid_args(%{attr1: 1, attr2: 1}, %{arg1: 1, arg2: 0})
      # both valid_args are false
      assert false == command_module.valid_args(%{attr1: 1, attr2: 1}, %{arg1: 0, arg2: 0})

      # call is the one in the extending model
      assert B_f = command_module.call(args)

      # both valid are true
      assert true == command_module.valid(%{attr1: 1, attr2: 1}, %{arg1: 1, arg2: 1}, result)
      # base valid is false
      assert false == command_module.valid(%{attr1: 1, attr2: 1}, %{arg1: 0, arg2: 1}, result)
      # extending valid is false
      assert false == command_module.valid(%{attr1: 1, attr2: 1}, %{arg1: 1, arg2: 0}, result)
      # both valid are false
      assert false == command_module.valid(%{attr1: 1, attr2: 1}, %{arg1: 0, arg2: 0}, result)

      # features are overriden
      assert [B_f] = command_module.features(state, args, result)

      # next applies the updates both models in order
      assert [attr1: 1, attr2: 1] = command_module.next(state, args, result, valid)

      # both post are true
      assert true == command_module.post(%{attr1: 1, attr2: 1}, %{arg1: 1, arg2: 1}, :ok, true)
      # base post is false
      assert false == command_module.post(%{attr1: 1, attr2: 1}, %{arg1: 0, arg2: 1}, :ok, true)
      # extending post is false
      assert false == command_module.post(%{attr1: 1, attr2: 1}, %{arg1: 1, arg2: 0}, :ok, true)
      # both post are false
      assert false == command_module.post(%{attr1: 1, attr2: 1}, %{arg1: 0, arg2: 0}, :ok, true)
    end
  end

  # TODO: check extends when callbacks return nil
  describe "nil values" do
    test "non-empty base model; non-empty extending model; non-overlapping commands, state or invariants." do
      defmodule A do
        use Makina
        state attr1: 0
        invariants prop1: true

        command f(arg1) do
          pre if attr1 != 1, do: false
          args arg1: 1
          valid_args if arg1 != 1, do: false
          call A_f
          next if valid, do: [attr1: 1]
          post if arg1 != 1, do: false
          valid if arg1 != 1, do: false
          features [A_f]
          weight 2
        end
      end

      defmodule B do
        use Makina, extends: A
        state attr2: 0
        invariants prop2: false

        # TODO: This generates a warning when it should not. It seems this warning is only triggered in the tests.
        command g(arg2) do
          pre if attr2 != 1, do: false
          args arg2: 1
          valid_args if arg2 != 1, do: false
          call B_g
          next if valid, do: [attr2: 1]
          post if arg2 != 1, do: false
          valid if arg2 != 1, do: false
          features [B_g]
          weight 2
        end
      end

      assert {:state_info, %{attributes: attrs, module: state_module}} = state_info(B)
      assert :attr1 in attrs
      assert :attr2 in attrs
      assert (state = %{attr1: 0, attr2: 0}) = state_module.initial()

      assert {:invariants_info, %{invariants: props, module: invariants_module}} =
               invariants_info(B)

      assert :prop1 in props
      assert :prop2 in props

      assert {:error, "failed to check the invariants: `:prop2`"} = invariants_module.check(state)

      assert {:commands_info, %{commands: cmds}} = commands_info(B)
      assert :f in cmds
      assert :g in cmds
      assert {:command_info, [f_info = %{name: :f}, g_info = %{name: :g}]} = command_info(B)

      assert %{module: command_module, name: :f, callbacks: callbacks, arguments: _, result: _} =
               f_info

      assert :pre in callbacks
      assert :args in callbacks
      assert :weight in callbacks
      assert :valid_args in callbacks
      assert :valid in callbacks
      assert :call in callbacks
      assert :features in callbacks
      assert :next in callbacks
      assert :post in callbacks

      args = %{arg1: 0}
      result = :ok
      valid = true
      assert false == command_module.pre(state)
      assert [arg1: 1] = command_module.args(state)
      assert 2 = command_module.weight(state)
      assert false == command_module.valid_args(state, args)
      assert false == command_module.valid(state, args, result)
      assert A_f = command_module.call(args)
      assert [A_f] = command_module.features(state, args, result)
      assert [attr1: 1] = command_module.next(state, args, result, valid)
      assert false == command_module.post(state, args, result, valid)

      assert %{module: command_module, name: :g, callbacks: callbacks, arguments: _, result: _} =
               g_info

      assert :pre in callbacks
      assert :args in callbacks
      assert :weight in callbacks
      assert :valid_args in callbacks
      assert :valid in callbacks
      assert :call in callbacks
      assert :features in callbacks
      assert :next in callbacks
      assert :post in callbacks

      args = %{arg2: 0}
      result = :ok
      valid = true
      assert false == command_module.pre(state)
      assert [arg2: 1] = command_module.args(state)
      assert 2 = command_module.weight(state)
      assert false == command_module.valid_args(state, args)
      assert false == command_module.valid(state, args, result)
      assert B_g = command_module.call(args)
      assert [B_g] = command_module.features(state, args, result)
      assert [attr2: 1] = command_module.next(state, args, result, valid)
      assert false == command_module.post(state, args, result, valid)
    end

    test "non-empty base model; non-empty extending model; overlapping commands." do
      defmodule A do
        use Makina
        state attr1: 0
        invariants prop1: true

        command f(arg1) do
          pre if attr1 != 1, do: false
          args arg1: 1
          valid_args if arg1 != 1, do: false
          call A_f
          next if valid, do: [attr1: 1]
          post if arg1 != 1, do: false
          valid if arg1 != 1, do: false
          features [A_f]
          weight 2
        end
      end

      defmodule B do
        use Makina, extends: A
        state attr2: 1
        invariants prop2: false

        command f(arg2) do
          pre if attr2 != 1, do: false
          args arg2: 1
          valid_args if arg2 != 1, do: false
          call B_f
          next if valid, do: [attr2: 1]
          post if arg2 != 1, do: false
          valid if arg2 != 1, do: false
          features [B_f]
          weight 2
        end
      end

      assert {:state_info, %{attributes: attrs, module: state_module}} = state_info(B)
      assert :attr1 in attrs
      assert :attr2 in attrs
      assert (state = %{attr1: 0, attr2: 1}) = state_module.initial()

      assert {:invariants_info, %{invariants: props, module: invariants_module}} =
               invariants_info(B)

      assert :prop1 in props
      assert :prop2 in props

      assert {:error, "failed to check the invariants: `:prop2`"} = invariants_module.check(state)

      assert {:commands_info, %{commands: cmds}} = commands_info(B)
      assert :f in cmds
      assert {:command_info, [f_info = %{name: :f}]} = command_info(B)

      assert %{module: command_module, name: :f, callbacks: callbacks, arguments: _, result: _} =
               f_info

      assert :pre in callbacks
      assert :args in callbacks
      assert :weight in callbacks
      assert :valid_args in callbacks
      assert :valid in callbacks
      assert :call in callbacks
      assert :features in callbacks
      assert :next in callbacks
      assert :post in callbacks

      args = %{arg1: 0, arg2: 1}
      result = :ok
      valid = true
      # both pres are true
      assert true == command_module.pre(%{attr1: 1, attr2: 1})
      # the base pre is false
      assert false == command_module.pre(%{attr1: 0, attr2: 1})
      # the extending pre is false
      assert false == command_module.pre(%{attr1: 0, attr2: 1})
      # both pres are false
      assert false == command_module.pre(%{attr1: 0, attr2: 0})

      # TODO: to check args we need a mock implementation of let
      assert [arg1: 1, arg2: 1] = command_module.args(state)

      # weight is the one in the extending model
      assert 2 = command_module.weight(state)

      # both valid args are true
      assert true == command_module.valid_args(%{attr1: 1, attr2: 1}, %{arg1: 1, arg2: 1})
      # base valid_args is false
      assert false == command_module.valid_args(%{attr1: 1, attr2: 1}, %{arg1: 0, arg2: 1})
      # extending valid_args is false
      assert false == command_module.valid_args(%{attr1: 1, attr2: 1}, %{arg1: 1, arg2: 0})
      # both valid_args are false
      assert false == command_module.valid_args(%{attr1: 1, attr2: 1}, %{arg1: 0, arg2: 0})

      # call is the one in the extending model
      assert B_f = command_module.call(args)

      # both valid are true
      assert true == command_module.valid(%{attr1: 1, attr2: 1}, %{arg1: 1, arg2: 1}, result)
      # base valid is false
      assert false == command_module.valid(%{attr1: 1, attr2: 1}, %{arg1: 0, arg2: 1}, result)
      # extending valid is false
      assert false == command_module.valid(%{attr1: 1, attr2: 1}, %{arg1: 1, arg2: 0}, result)
      # both valid are false
      assert false == command_module.valid(%{attr1: 1, attr2: 1}, %{arg1: 0, arg2: 0}, result)

      # features are overriden
      assert [B_f] = command_module.features(state, args, result)

      # next applies the updates both models in order
      assert [attr1: 1, attr2: 1] = command_module.next(state, args, result, valid)

      # both post are true
      assert true == command_module.post(%{attr1: 1, attr2: 1}, %{arg1: 1, arg2: 1}, :ok, true)
      # base post is false
      assert false == command_module.post(%{attr1: 1, attr2: 1}, %{arg1: 0, arg2: 1}, :ok, true)
      # extending post is false
      assert false == command_module.post(%{attr1: 1, attr2: 1}, %{arg1: 1, arg2: 0}, :ok, true)
      # both post are false
      assert false == command_module.post(%{attr1: 1, attr2: 1}, %{arg1: 0, arg2: 0}, :ok, true)
    end
  end
end
