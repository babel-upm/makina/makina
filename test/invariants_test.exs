defmodule InvariantsTest do
  use ExUnit.Case
  use TestHelpers

  test "invariants inherited from the same model should be permitted in parallel composition" do
    defmodule A do
      use Makina
      invariants inv: true
    end

    defmodule B do
      use Makina, extends: A
    end

    defmodule C do
      use Makina, extends: A
    end

    defmodule D do
      use Makina, extends: [A, B, C]
    end
  end


  test "invariants inherited from the same model should be permitted in parallel composition 2" do
    defmodule A do
      use Makina
      invariants inv: true
    end

    defmodule B do
      use Makina, extends: A
    end

    defmodule C do
      use Makina, extends: B
    end

    defmodule D do
      use Makina, extends: [A, B, C]
    end
  end
end
