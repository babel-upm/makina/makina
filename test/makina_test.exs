defmodule MakinaTest do
  use ExUnit.Case
  use TestHelpers

  import Makina.Invariants, only: [invariants_info: 1]
  import Makina.State, only: [state_info: 1]
  import Makina.Command, only: [commands_info: 1, command_info: 1]

  describe "error messages" do
    test "multiple attribute" do
      assert_raise Makina.Error, "multiple declarations of attributes: `:attr1`", fn ->
        defmodule Wrong do
          use Makina
          state attr1: 1, attr1: 1
        end
      end
    end

    test "multiple invariant" do
      assert_raise Makina.Error, "multiple declarations of invariants: `:prop1`", fn ->
        defmodule Wrong do
          use Makina
          invariants prop1: true, prop1: true
        end
      end
    end
  end

  test "empty Makina" do
    defmodule Empty do
      use Makina
    end

    assert {:invariants_info, %{invariants: []}} = invariants_info(Empty)
    assert {:state_info, %{attributes: [], module: Empty.State}} = state_info(Empty)
    assert {:commands_info, %{commands: []}} = commands_info(Empty)
  end

  test "state" do
    defmodule A do
      use Makina
      state attr1: true :: integer(), attr2: %{}
    end

    assert {:state_info, %{attributes: attrs, module: state_module}} = state_info(A)
    assert :attr1 in attrs
    assert :attr2 in attrs
    assert %{attr1: true, attr2: %{}} = state_module.initial()

    assert {:invariants_info, %{invariants: []}} = invariants_info(A)
    assert {:commands_info, %{commands: []}} = commands_info(A)
  end

  test "invariants" do
    defmodule A do
      use Makina
      # nil should be treated as true
      invariants prop1: true, prop2: false, prop3: nil, prop4: :something
    end

    assert {:invariants_info, %{invariants: invs, module: invariants_module}} = invariants_info(A)
    assert :prop1 in invs
    assert :prop2 in invs
    assert :prop3 in invs

    assert {:error, "failed to check the invariants: `:prop2`"} =
             invariants_module.check(%{})

    assert {:commands_info, %{commands: []}} = commands_info(A)
    assert {:state_info, %{attributes: [], module: A.State}} = state_info(A)
  end

  test "commands" do
    defmodule A do
      use Makina

      command f(arg1 :: integer, arg2) do
        call :ok
        args arg1: 0, arg2: 1
      end
    end

    assert {:invariants_info, %{invariants: []}} = invariants_info(A)
    assert {:commands_info, %{commands: cmds}} = commands_info(A)
    assert {:state_info, %{attributes: [], module: state_module}} = state_info(A)
    assert :f in cmds
    assert {:command_info, [f_info]} = command_info(A)

    assert %{module: command_module, name: :f, callbacks: callbacks, arguments: _, result: _} =
             f_info

    assert :pre in callbacks
    assert :args in callbacks
    assert :valid_args in callbacks
    assert :valid in callbacks
    assert :call in callbacks
    assert :next in callbacks
    assert :post in callbacks
    assert :features in callbacks
    assert :weight in callbacks

    state = state_module.initial()
    assert true == command_module.pre(state)
    args = command_module.args(%{}) |> Map.new()
    assert {:ok, args} = command_module.validate_args(args)
    assert true = command_module.valid_args(state, args)
    assert :ok = command_module.call(args)
    valid = command_module.valid(state, args, :ok)
    assert [] = command_module.next(state, args, {:var, 1}, valid)
    assert true = command_module.post(state, args, {:var, 1}, valid)
    assert 1 = command_module.weight(state)
    assert [] = command_module.features(state, args, {:var, 1})
  end

  test "complete makina" do
    defmodule A do
      use Makina
      state attr1: 0 :: integer(), attr2: [] :: [integer()]
      invariants prop1: attr1 == 0

      command f(arg1 :: integer()) :: :ok do
        args arg1: 1
        call :ok
        next attr1: arg1
      end
    end

    assert {:state_info, %{attributes: attrs, module: state_module}} = state_info(A)
    assert :attr1 in attrs
    assert %{attr1: 0, attr2: []} = state_module.initial()
    state = state_module.initial()

    assert {:invariants_info, %{invariants: invs, module: invariants_module}} = invariants_info(A)
    assert :prop1 in invs
    assert {:ok, ^state} = invariants_module.check(state)

    assert {:commands_info, %{commands: cmds}} = commands_info(A)
    assert :f in cmds
    assert {:command_info, [f_info]} = command_info(A)

    assert %{module: command_module, name: :f, callbacks: callbacks, arguments: _, result: _} =
             f_info

    assert :pre in callbacks
    assert :args in callbacks
    assert :valid_args in callbacks
    assert :valid in callbacks
    assert :call in callbacks
    assert :next in callbacks
    assert :post in callbacks
    assert :features in callbacks
    assert :weight in callbacks

    assert true == command_module.pre(state)
    args = command_module.args(state) |> Map.new()
    assert {:ok, args} = command_module.validate_args(args)
    assert true = command_module.valid_args(state, args)
    assert :ok = command_module.call(args)
    valid = command_module.valid(state, args, :ok)
    assert [attr1: 1] = command_module.next(state, args, {:var, 1}, valid)
    assert true = command_module.post(state, args, {:var, 1}, valid)
    assert 1 = command_module.weight(state)
    assert [] = command_module.features(state, args, {:var, 1})

    updates = command_module.next(state, args, {:var, 1}, valid)
    {:ok, state} = state_module.update(state, updates)
    assert {:error, "failed to check the invariants: `:prop1`"} = invariants_module.check(state)
  end
end
