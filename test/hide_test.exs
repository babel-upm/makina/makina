defmodule MakinaHidingTest do
  use ExUnit.Case
  use TestHelpers

  import Makina.Command, only: [commands_info: 1]

  describe "hiding errors" do
    test "missing model" do
      assert_raise Makina.Error, "`:extends` is missing, but is required for `:hiding`", fn ->
        defmodule A do
          use Makina, hiding: :f
        end
      end
    end

    test "error argument" do
      assert_raise Makina.Error, "`:hiding` receives a list of command names", fn ->
        defmodule A do
          use Makina
        end

        defmodule B do
          use Makina, extends: A, hiding: {:f}
        end
      end
    end

    test "missing commands" do
      assert_raise Makina.Error, "`:hiding` error, missing `f` in `#{inspect(__MODULE__)}.A`", fn ->
        defmodule A do
          use Makina
        end

        defmodule B do
          use Makina, extends: A, hiding: :f
        end
      end
    end
  end

  test "hide 1 command" do
    defmodule A do
      use Makina
      command f(), do: call(:ok)
    end

    defmodule B do
      use Makina, extends: A, hiding: :f
    end

    assert {:commands_info, %{commands: [:f]}} = commands_info(A)
    assert {:commands_info, %{commands: []}} = commands_info(B)
  end

  test "hide 1 commands but 1 remains" do
    defmodule A do
      use Makina
      command f(), do: call(:ok)
      command g(), do: call(:ok)
    end

    defmodule B do
      use Makina, extends: A, hiding: :f
    end

    assert {:commands_info, %{commands: cmds}} = commands_info(A)
    assert :f in cmds
    assert :g in cmds
    assert {:commands_info, %{commands: cmds}} = commands_info(B)
    assert :f not in cmds
    assert :g in cmds
  end

  test "hide 2 commands" do
    defmodule A do
      use Makina
      command f(), do: call(:ok)
      command g(), do: call(:ok)
    end

    defmodule B do
      use Makina, extends: A, hiding: [:f, :g]
    end

    assert {:commands_info, %{commands: cmds}} = commands_info(A)
    assert :f in cmds
    assert :g in cmds
    assert {:commands_info, %{commands: cmds}} = commands_info(B)
    assert :f not in cmds
    assert :g not in cmds
  end

  test "multiple modules can use the same hiding" do
    defmodule A do
      use Makina
      command f(), do: call(:ok)
    end


    fun = fn module ->
      receive do
        :start -> :ok
      end

      defmodule module do
        use Makina, extends: A, hiding: :f
      end
    end

    # starts compilation of C and D
    pid1 = spawn(fn -> fun.(C) end)
    pid2 = spawn(fn -> fun.(D) end)

    Process.link(pid1)
    Process.link(pid2)

    send(pid1, :start)
    send(pid2, :start)
  end
end
