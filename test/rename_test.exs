defmodule RenamingTest do
  use ExUnit.Case
  use TestHelpers

  import Makina.Command, only: [commands_info: 1]

  describe "aliasing errors" do
    test "missing model" do
      assert_raise Makina.Error, "`:extends` is missing, but is required for `:where`", fn ->
        defmodule A do
          use Makina, where: [f: :g]
        end
      end
    end

    test "missing command" do
      assert_raise Makina.Error, "`:where` error, missing `f` in `#{inspect(__MODULE__)}.A`", fn ->
        defmodule A do
          use Makina
        end

        defmodule B do
          use Makina, extends: A, where: [f: :g]
        end
      end
    end

    test "overriding command" do
      assert_raise Makina.Error,
                   "`:where` error, cannot rename `f` to `g`, `g` already exists in `#{inspect(__MODULE__)}.A`",
                   fn ->
                     defmodule A do
                       use Makina

                       command f() do
                         call :ok
                       end

                       command g() do
                         call :ok
                       end
                     end

                     defmodule B do
                       use Makina, extends: A, where: [f: :g]
                     end
                   end
    end
  end

  test "rename 1 command" do
    defmodule A do
      use Makina
      command f(), do: call(:ok)
    end

    defmodule B do
      use Makina, extends: A, where: [f: :g]
    end

    assert {:commands_info, %{commands: cmds}} = commands_info(A)
    assert :f in cmds
    assert {:commands_info, %{commands: cmds}} = commands_info(B)
    assert :g in cmds
    assert :f in cmds
  end

  test "rename 1 to 2 commands" do
    defmodule A do
      use Makina
      command f(), do: call(:ok)
    end

    defmodule B do
      use Makina, extends: A, where: [f: :g, f: :h]
    end

    assert {:commands_info, %{commands: cmds}} = commands_info(A)
    assert :f in cmds
    assert {:commands_info, %{commands: cmds}} = commands_info(B)
    assert :g in cmds
    assert :h in cmds
    assert :f in cmds
  end

  test "multiple modules can use the same alias" do
    defmodule A do
      use Makina
      command f(), do: call(:ok)
    end


    fun = fn module ->
      receive do
        :start -> :ok
      end

      defmodule module do
        use Makina, extends: A, where: [f: :g]
      end
    end

    # starts compilation of C and D
    pid1 = spawn(fn -> fun.(C) end)
    pid2 = spawn(fn -> fun.(D) end)

    Process.link(pid1)
    Process.link(pid2)

    send(pid1, :start)
    send(pid2, :start)
  end
end
