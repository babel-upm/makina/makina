defmodule Checker do
  defmacro __using__(_options) do
    quote do
      import unquote(__MODULE__)
      require unquote(__MODULE__)
    end
  end

  defmacro let(decls, do: body) do
    decls =
      Macro.prewalk(decls, fn
        {:<-, c, args} -> {:=, c, args}
        {:^, _, [arg]} -> arg
        x -> x
      end)

    body =
      case body do
        {:__block__, _, block} -> block
        _ -> [body]
      end

    decls =
      if is_list(decls) do
        decls
      else
        [decls]
      end

    result =
      quote do
        (unquote_splicing(decls ++ body))
      end

    result
  end

  def frequency(_), do: []

  def oneof(list) do
    Enum.random(list)
  end
end

Application.put_env(:makina, :checker, Checker)

defmodule TestHelpers do
  defmacro __using__(_) do
    quote do
      import unquote(__MODULE__)

      setup_all do
        [loaded_modules: all_loaded()]
      end

      setup context do
        on_exit(fn ->
          MapSet.difference(all_loaded(), context.loaded_modules) |> Enum.map(&clean/1)
        end)

        :ok
      end
    end
  end

  def all_loaded(), do: :code.all_loaded() |> Enum.map(fn {name, _path} -> name end) |> MapSet.new()

  def clean(module) do
    :code.purge(module)
    :code.delete(module)
    :ok
  end
end

ExUnit.start()
