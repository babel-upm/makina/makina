This type represents the union of the arguments of all the commands in `<%= inspect(name) %>`.
