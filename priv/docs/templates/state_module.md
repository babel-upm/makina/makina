This module contains the state definition of the model `<%= inspect(model) %>`.

## Attributes

<%= for attr <- attrs, do: "- #{attr}\n" %>

Detailed information about each attribute can be accessed inside the interpreter:

    iex> h <%= module %>.attribute
