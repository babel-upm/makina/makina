Represents the symbolic calls that can be generated from the commands specified in `<%=
inspect(name) %>`.
