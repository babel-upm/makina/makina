This function checks all invariants defined in `<%= inspect(name) %>`. If a invariant is not satisfied, throws an exception.
