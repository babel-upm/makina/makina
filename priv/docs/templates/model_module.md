Contains a Makina model called `<%= inspect(name) %>`.

<%= if userdocs, do: elem(userdocs, 1) %>

<%= if extends do %>
## TODO
<% end %>

<%= if composed do %>
## TODO
<% end %>

<%= if imports do %>
## TODO
<% end %>

## Commands

<%= for c <- cmds, do: "- `#{c}` stored at `#{Makina.Helpers.command_module_name(c, name) |> inspect()}`\n" %>

Detailed information about each command can be accessed inside the interpreter:

    iex> h <%= inspect(name) %>.Command.NAME

## State attributes

<%= for attr <- attrs, do: "- `#{attr}`\n" %>

Detailed information about the state can be accessed inside the interpreter:

    iex> h <%= inspect(name) %>.Command.State

## Invariants

<%= for inv <- invs, do: "- `#{inv}`\n" %>

Detailed information about the invariants can be accessed inside the interpreter:

    iex> h <%= inspect(name) %>.Invariants
