Defines the initial state of the model `<%= inspect(name) %>`.

<%= if attrs == [] do %>

Contains no attributes.

<% else %>

Contains the attributes:

<%  attrs = for attr <- attrs, do: " - `#{attr}`." %>
<%= Enum.join(attrs, "\n")%>

<% end %>
