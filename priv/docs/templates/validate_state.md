This function checks the correctness of a given state. This means that the given state contains the
attributes declared in the model `<%= inspect(name) %>`
