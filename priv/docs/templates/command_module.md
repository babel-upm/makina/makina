This module contains the functions necessary to generate and execute the command `<%= name %>`.

<%= if userdocs, do: elem(userdocs, 1) %>

## Defined callbacks

<%= for c <- callbacks, do: "- `#{c}`\n" %>

Detailed information about each callback can be accessed inside the interpreter:

    iex> h <%= inspect(module) %>.post
