This type represent the arguments for command `<%= name %>`.

<%= if args != [] do %>

Contains the named arguments:

<%  args = for {arg, _} <- args, do: " - `#{arg}`." %>
<%= Enum.join(args, "\n") %>

<% else %>

Contains no named arguments.

<% end %>
