defmodule Checker do
  defmacro __using__(_options) do
    quote do
      import unquote(__MODULE__)
      require unquote(__MODULE__)
    end
  end

  defmacro let(decls, do: body) do
    decls =
      Macro.prewalk(decls, fn
        {:<-, c, args} -> {:=, c, args}
        {:^, _, [arg]} -> arg
        x -> x
      end)

    body =
      case body do
        {:__block__, _, block} -> block
        _ -> [body]
      end

    decls =
      if is_list(decls) do
        decls
      else
        [decls]
      end

    result =
      quote do
        (unquote_splicing(decls ++ body))
      end

    result
  end

  def frequency(_), do: []

  def oneof(list) do
    Enum.random(list)
  end
end

Application.put_env(:makina, :checker, Checker)

import Makina.Invariants, only: [invariants_info: 1, invariant_info: 1]
import Makina.State, only: [state_info: 1]
import Makina.Command, only: [commands_info: 1, command_info: 1]
