defmodule Makina.MixProject do
  use Mix.Project

  @source_url "https://gitlab.com/babel-upm/makina/makina"
  @version "0.3.0"

  def project do
    [
      app: :makina,
      version: @version,
      elixir: "~> 1.13",
      start_permanent: Mix.env() == :prod,
      description: description(),
      package: package(),
      deps: deps(),
      name: "Makina",
      source_url: @source_url,
      home_url: @source_url,
      docs: [
        extra_section: "Introduction",
        main: "readme",
        source_url: @source_url,
        source_ref: "v#{@version}",
        extras: ["README.md", "CHANGELOG.md"]
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :eex]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:dialyxir, "~> 1.0", only: [:dev], runtime: false},
      {:ex_doc, "~> 0.24", only: :dev, runtime: false}
    ]
  end

  defp description() do
    "Makina is a DSL for writing PBT models for stateful systems."
  end

  defp package() do
    [
      files: ~w(lib priv .formatter.exs mix.exs README* LICENSE*),
      licenses: ["BSD-2-Clause"],
      links: %{"GitLab" => @source_url}
    ]
  end
end
